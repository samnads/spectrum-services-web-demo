<?php
return [
    'js' => isDemo() === true ? time() : '1.10',
    'css' => isDemo() === true ? time() : '1.3',
    'img' => isDemo() === true ? time() : '1.2',
]
    ?>