
function renderAddressList(address_list) {
    address_html = ``;
    let addresses_list_html = ``;
    if (address_list.length == 0) {
        $('#address-list').html(`<div class="alert alert-info" role="alert">
  No address found, please add atleast one address to start booking.
</div>`);
        $('#address-def-btn-container').hide();
        showAddressForm();
    }
    else {
        $.each(address_list, function (index, address) {
            address_html += `<div class="col-sm-12 manage-address-section">
                                        <div class="d-flex manage-address-main">
                                            <div class="manage-address-content flex-grow-1">
                                                    <input id="address-`+ index + `" value="` + address.address_id + `" name="address_id" class="" type="radio" ` + (address.default_address == 1 ? 'checked' : '') + `>
                                                <label for="address-`+ index + `">
                                                    <span></span>
                                                    <p class="fw-bold"><i class="fa fa-building-o" aria-hidden="true"></i>&nbsp;&nbsp;` + address.flat_no + `, ` + address.building + `</p>
                                                    <p><i class="fa fa-map-marker" aria-hidden="true"></i>&nbsp;&nbsp;` + address.street + `, ` + address.area_name + `</p>
                                                </label>
                                            </div>
                                            <div class="manage-address-edit v-center">
                                                <div class="manage-address-edit-icons">
                                                    <label class="delete-action" data-action="delete-address-popup" data-id="` + address.address_id + `" data-default_address="` + address.default_address + `">` + (address.default_address == 1 ? '<i class="fa fa-lock"></i>' : '<i class="fa fa-trash"></i>') +`</label>
                                                    <label data-action="new-address" data-id="` + address.address_id + `"><i
                                                            class="fa fa-pencil"></i></label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>`;
        });
        $('#address-list').html(address_html);
        $('#address-def-btn-container').show();
    }
    $('[data-action="new-address"]').click(function (e) {
        if ($(this).attr('data-id')) {
            // show for edit purpose only
            showAddressForm($(this).attr('data-id'), e);
        }
    });
    $('[data-action="delete-address-popup"]').click(function () {
        if ($(this).attr('data-default_address') == 1) {
            toast('Can\'t Delete', 'Default address can\'t deleted.', 'warning');
        }
        else {
            let address_id = this.getAttribute('data-id');
            Swal.fire({
                title: "Confirm delete?",
                text: "Are you sure want to delete this address?",
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: "#d33",
                cancelButtonColor: "lightgreen",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No",
                focusCancel: true
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        type: 'POST',
                        url: _base_url + "api/customer/delete_address",
                        data: {
                            address_id: address_id,
                        },
                        success: function (response) {
                            if (response.result.status == "success") {
                                addressListFetch();
                                toast('Deleted', response.result.message, 'success');
                            }
                            else {
                                toast('Error', response.result.message, 'error');
                            }
                        },
                        error: function (response) {
                        },
                    });
                }
            });
        }
    });
    _address_from = $('#new-address-popup-form');
}
addressListFetch();
default_address_form_validator = $('#default_address_form').validate({
    focusInvalid: false,
    ignore: [],
    rules: {
        "address_id": {
            required: true,
        }
    },
    messages: {
        "address_id": {
            required: "Select an address",
        }
    },
    errorPlacement: function (error, element) {
        error.insertAfter(element);
    },
    submitHandler: function (form) {
        let btn = $('button[type="submit"]', form);
        btn.html(loading_button_html).attr('disabled', true);;
        $.ajax({
            type: 'POST',
            url: _base_url + "api/customer/set_default_address",
            dataType: 'json',
            //data: JSON.stringify($('#login-popup-form').serializeObjectForApi()),
            data: $('#default_address_form').serialize(),
            //contentType: 'application/json;charset=UTF-8',
            success: function (response) {
                btn.html('Set Default Address').attr('disabled', false);
                if (response.result.status == "success") {
                    toast('Success', response.result.message, 'success');
                    addressListFetch();
                } else {
                    toast('Error', "Try again...", 'error');
                }
            },
            error: function (response) {
                btn.html('Set Default Address').attr('disabled', false);
            },
        });
    }
});
$(document).ready(function () {
    addressForm();
});