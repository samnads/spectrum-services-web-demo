/************************************************************************************
 *                                                                                  *
 *                          File Author : Samnad.S                                  *
 *                                                                                  *
 ************************************************************************************/
var available_time_req = null;
var crew_list_req = null;
/************************************************************************************************* */
$('[data-action="next-step"]').click(function () {
    booking_form = $('#booking-form');
    let current_step = Number($(this).attr('data-step'));
    if (isStepValid(current_step) === false) {
        // DONT'T GO TO NEXT STEP
        return;
    }
    let next_step = current_step + 1;
    // do before going to step
    if (next_step == 3) {
        if ($('input[name="id"]', booking_form).val() == '') {
            // not logged in
            $('.login-popup').show(500);
            toast('Login Required', 'Please login to continue !', 'info');
            return false;
        } else {
            // logged in
            if (_address_list.length == 0) {
                // there is no address added
                $('#new-address-popup-form input[name="next_action"]').val('set_booking_address');
                showAddressForm();
                toast('Address Required', 'Add address to continue', 'info');
                return false;
            }
        }
    }
    //
    $('.step-' + current_step).hide();
    $('.step-' + next_step).show();
    // do after going to step
    if (next_step == 3) {
        $('input[name="payment_method"]:checked', booking_form).trigger('change');
    }
    window.history.pushState({}, "", decodeURI(_current_url + $.query.set('step', next_step)));
    window.scrollTo(0, 0);
});
$('[data-action="prev-step"]').click(function () {
    booking_form = $('#booking-form');
    let current_step = Number($(this).attr('data-step'));
    let prev_step = current_step - 1;
    //
    $('.step-' + current_step).hide();
    $('.step-' + prev_step).show();
    /************************************* */
    $(document).prop('title', _meta_title + ' ' + prev_step + '/4');
    /************************************* */
    if (prev_step == 3) {
        //$('#frequency-popup').show(500);
    }
    window.history.pushState({}, "", decodeURI(_current_url + $.query.set('step', prev_step)));
    window.scrollTo(0, 0);
});
/************************************************************************************************* */
// steps validator
function isStepValid(step) {
    var required = [];
    if (step == 1) {
        required = [{
            name: 'date',
            status: $('input[name="date"]', booking_form).valid()
        },
        {
            name: 'time',
            status: $('input[name="time"]', booking_form).valid()
        },
        {
            name: 'hours',
            status: $('input[name="hours"]', booking_form).valid()
        },
        {
            name: 'professionals_count',
            status: $('input[name="professionals_count"]', booking_form).valid()
        },
        ];
    } else if (step == 2) {
        required = [{
            name: 'cleaning_materials',
            status: $('input[name="cleaning_materials"]', booking_form).valid()
        },
        {
            name: 'instructions',
            status: $('textarea[name="instructions"]', booking_form).valid()
        },
        ];
    } else if (step == 3) {
        required = [{
            name: 'payment_method',
            status: $('input[name="payment_method"]', booking_form).valid()
        },];
    }
    /************************************************ */
    // check for errors and show as toast
    var errors = required.filter(field => {
        return field.status === false
    });
    if (errors.length > 0) {
        var first_error_field = errors[0].name;
        var error_message = booking_form_validator.submitted[first_error_field];
        toast(null, error_message, 'info');
        scrollToElement($('[name="' + first_error_field + '"]', booking_form));
        return false;
    }
    /************************************************ */
    // step valid
    return true;
}
/************************************************************************************************* */
$('button[type="submit"]', booking_form).click(function () {
    isStepValid(3);
});
/************************************************************************************************* */
$('[data-action="frequency-change"]').click(function () {
    // selected frequency changed
    var frequency = _frequencies.find(frequency => {
        return frequency.code == $('input[name="frequency"]:checked').val()
    });
    if (frequency.code == "OD") {
        $('.no-of-weeks').hide();
    }
    else {
        $('.no-of-weeks').show();
    }
    $('.frequency-name').html(frequency.name);
    Cookies.set('frequency', $('input[name="frequency"]:checked').val()); // save frequency
    $('#frequency-popup').hide(500);
    calculate();
});
$('#booking-form input[name=date]').change(function () {
    selected_time = $('#booking-form input[name="time"]:checked').val();
    if (selected_time) {
        // save selected time
        Cookies.set('time', selected_time);
    }
    var summary_date = moment(this.value, 'DD/MM/YYYY').format('DD MMM YYYY');
    $('#booking-summary .date').html(summary_date);
    params = { date: this.value, hours: $('#booking-form input[name="hours"]:checked').val() };
    $('#booking-times-holder').html(loading_html);
    available_time_req = $.ajax({
        type: 'GET',
        url: _base_url + "api/customer/available_time",
        dataType: 'json',
        data: params,
        beforeSend: function () {
            if (available_time_req != null) {
                available_time_req.abort();
            }
        },
        success: function (response) {
            if (response.result.status == "success") {
                if (response.result.available_times.length == 0) {
                    $('#booking-times-holder').html(no_time_slot_available_html);
                    $('#hours-count-holder').html(``);
                }
                else {
                    times = '';
                    $.each(response.result.available_times, function (index, available_time) {
                        var rush_time = response.result.rush_times.find(obj => {
                            return obj.time == available_time
                        })
                        if (rush_time !== undefined) {
                            rush_time_html = `<p>AED ` + rush_time.extra_charge + ` Extra</p>`;
                            rush_time_data = `data-rush_slot_id="` + rush_time.id + `"`;
                        }
                        else {
                            rush_time_html = ``;
                            rush_time_data = ``;
                        }
                        times += `<li>
                                <input id="booking_time_`+ index + `" value="` + available_time + `" name="time" class="" type="radio" ` + rush_time_data + `>
                                <label for="booking_time_`+ index + `">` + moment(available_time, "HH:mmm").format("hh:mm A") + ` ` + rush_time_html + `</label>
                            </li>`;
                    });
                    $('#booking-times-holder').html(times);
                    timeTrigger(); // create event listener
                    /********************************************************* */
                    // keep same time
                    if (Cookies.get('time')) {
                        // try to keep selected time if available
                        var same_time_found = response.result.available_times.find(obj => {
                            return obj == Cookies.get('time')
                        });
                        if (same_time_found) {
                            $('#booking-form input[name="time"][value="' + same_time_found + '"]').prop("checked", true).trigger('click');
                        }
                        else {
                            $('#booking-form input[name="time"]:first').trigger('click');
                        }
                    }
                    else {
                        $('#booking-form input[name="time"]:first').trigger('click');
                    }
                    /********************************************************* */
                }
            }
            else {
            }
        },
        error: function (response) {
        },
    });
});
function timeTrigger() {
    $('#booking-form input[name="time"]').click(function () {
        Cookies.set('time', this.value);
        /*$('#booking-form input[name="rush_slot_id"]').val($(this).attr("data-rush_slot_id"));
        var service_time = moment(this.value, 'H:mm');
        var maxHoursAvailable = WORK_END_TIME.diff(service_time, 'minutes') / 60;
        var hours = ``;
        for (var i = 2; i <= maxHoursAvailable; i++) {
            hours += `<li><input id="booking_hours_` + i + `" value="` + i + `" name="hours" class="" type="radio">
                    <label for="booking_hours_` + i + `">` + i + `</label>
                </li>`;
        }
        $('#hours-count-holder').html(hours);*/
        hoursTrigger();
        /********************************************************* */
        // keep same hour
        if (Cookies.get('hours')) {
            // try to keep selected hours if available
            if ($('#booking-form input[name="hours"][value="' + Cookies.get('hours') + '"]').length > 0) {
                $('#booking-form input[name="hours"][value="' + Cookies.get('hours') + '"]').prop("checked", true).trigger('change');
            }
            else {
                $('#booking-form input[name="hours"]:first').trigger('change');
            }
        }
        else {
            $('#booking-form input[name="hours"]:first').trigger('change');
        }
        /********************************************************* */
        var hours = $('#booking-form input[name="hours"]:checked').val();
        var summary_time_from = moment(this.value, 'H:mm').format('hh:mm A');
        var summary_time_to = moment(this.value, 'H:mm').add(hours, 'hours').format('hh:mm A');
        $('#booking-summary .time').html(summary_time_from + ` to ` + summary_time_to);
        calculate();
    });
}
function renderHousekeepers() {
    $('#housekeepers.owl-carousel').trigger('destroy.owl.carousel'); //these 3 lines kill the owl, and returns the markup to the initial state
    $('#housekeepers.owl-carousel').find('.owl-stage-outer').children().unwrap();
    $('#housekeepers.owl-carousel').removeClass("owl-center owl-loaded owl-text-select-on");
    $("#housekeepers.owl-carousel").owlCarousel({
        nav: false,
        loop: false,
        dots: true,
        margin: 15,
        autoplay: false,
        autoplayTimeout: 2000,
        smartSpeed: 800,
        autoplayHoverPause: true,
        responsive: {
            0: {
                items: 2
            },
            600: {
                items: 4
            },
            1000: {
                items: 5
            },
            1100: {
                items: 5
            },
            1200: {
                items: 6,
                //margin: 50
            }
        }
    }); //re-initialise the owl
}
function hoursTrigger() {
    $('#booking-form input[name="hours"]').change(function () {
        Cookies.set('hours', this.value);
        $('#booking-summary .duration').html(this.value + ' Hours');
        var hours = $('#booking-form input[name="hours"]:checked').val();
        var summary_time_from = moment($('#booking-form input[name="time"]:checked').val(), 'H:mm').format('hh:mm A');
        var summary_time_to = moment($('#booking-form input[name="time"]:checked').val(), 'H:mm').add(hours, 'hours').format('hh:mm A');
        $('#booking-summary .time').html(summary_time_from + ` to ` + summary_time_to);
        params = {
            date: $('#booking-form input[name="date"]:checked').val(),
            time_from: $('#booking-form input[name="time"]:checked').val(),
            time_to: moment($('#booking-form input[name="time"]:checked').val(), 'H:mm').add(hours, 'hours').format('H:mm'),
            frequency: $('#booking-form input[name="frequency"]').val(),
            booking_id: null,
            zone_id: null
        };
        crew_list_req = $.ajax({
            type: 'GET',
            url: _base_url + "api/customer/crew_list",
            dataType: 'json',
            data: params,
            beforeSend: function () {
                $('#housekeepers').html(`<div class="item">
                                                <input id="which-maid1" value="0" name="which-maid" checked="checked" class=""
                                                    type="checkbox">
                                                <label for="which-maid1">
                                                    <div class="which-housekeeper-thumb">
                                                        <div class="which-housekeeper-thumb-image"><img src="`+ _base_url + `images/auto-maid.jpg"
                                                                alt="" /></div>
                                                        <div class="which-housekeeper-thumb-name v-center">Auto-assign</div>
                                                        <div class="which-housekeeper-thumb-auto-assign">We'll assign the best Housekeeper
                                                        </div>
                                                    </div>
                                                </label>
                                            </div>`);
                renderHousekeepers();
                if (crew_list_req != null) {
                    crew_list_req.abort();
                }
            },
            success: function (response) {
                if (response.result.status == "success") {
                    available_maid_list = `<div class="item">
                                                <input id="prefer_auto" value="0" name="professional_prefer_auto" checked="checked" class=""
                                                    type="radio">
                                                <label for="prefer_auto">
                                                    <div class="which-housekeeper-thumb">
                                                        <div class="which-housekeeper-thumb-image"><img src="`+ _base_url + `images/auto-maid.jpg"
                                                                alt="" /></div>
                                                        <div class="which-housekeeper-thumb-name v-center">Auto-assign</div>
                                                        <div class="which-housekeeper-thumb-auto-assign">We'll assign the best Housekeeper
                                                        </div>
                                                    </div>
                                                </label>
                                            </div>`;
                    $.each(response.result.available_maid_list, function (index, maid) {
                        available_maid_list += `<div class="item">
                                                    <input value='` + JSON.stringify(maid) + `' name="professional[]" type="hidden">
                                                    <input id="maid`+ maid.maid_id + `" value="` + maid.maid_id + `" name="professional_prefer[]" class="" type="checkbox" data-name="` + maid.name + `">
                                                    <label for="maid`+ maid.maid_id + `">
                                                        <div class="which-housekeeper-thumb">
                                                            <div class="which-housekeeper-thumb-image"><img src="`+ maid.maid_img_url + `" alt="" />
                                                            </div>
                                                            <div class="which-housekeeper-thumb-name v-center">`+ maid.name + `</div>
                                                            <div class="which-housekeeper-thumb-rating"><img src="`+ _base_url + `images/5star.png" alt="" />
                                                            </div>
                                                            <div class="which-housekeeper-thumb-btn" onclick="javascript:void(0);">See Details</div>
                                                        </div>
                                                    </label>
                                                </div>`;
                    });
                    $('#housekeepers').html(available_maid_list);
                    renderHousekeepers();
                    housekeepersTrigger();
                    $('#booking-form input[name="professional_prefer[]"]').trigger('change');
                }
                else {
                }
            },
            error: function (response) {
            },
        });
        calculate();
    });
}
timeTrigger();
function housekeepersTrigger() {
    $('#booking-form input[name="professional_prefer[]"]').off();
    $('#booking-form input[name="professional_prefer[]"]').change(function () {
        var professionals_count = $('#booking-form input[name="professionals_count"]:checked').val();
        var professional_prefer_sel_count = $('#booking-form input[name="professional_prefer[]"]:checked').length;
        if (professional_prefer_sel_count == 0) {
            // none selected
            $('#booking-form input[name="professional_prefer_auto"]').prop('checked', true);
            $('#booking-form input[name="professional_prefer[]"]').attr("disabled", false);
        }
        else if (professional_prefer_sel_count == professionals_count) {
            // maximum selected
            $('#booking-form input[name="professional_prefer_auto"]').prop('checked', false);
            $('#booking-form input[name="professional_prefer[]"]:not(:checked)').attr("disabled", true);
        }
        else {
            // maximum not selected
            $('#booking-form input[name="professional_prefer_auto"]').prop('checked', false);
            $('#booking-form input[name="professional_prefer[]"]').attr("disabled", false);
        }
        /********************************************** */
        var crew_names = ``;
        var length = $('#booking-form input[name="professional_prefer[]"]:checked').length;
        $('#booking-form input[name="professional_prefer[]"]:checked').each(function (index, obj) {
            crew_names += $(obj).data("name");
            if (length > 1 && index != (length - 1)) {
                crew_names += `<br>`;
            }
        });
        $('.crew_names').html(crew_names || '[ Auto Assign ]');
        /********************************************** */
    });
    $('#booking-form input[name="professional_prefer_auto"]').change(function () {
        $('#booking-form input[name="professional_prefer[]"]').prop('checked', false).attr("disabled", false);
        $('#booking-form input[name="professional_prefer[]"]').trigger('change');
        calculate();
    });
    /********************************************************* */
    // maid details popup
    $('#booking-form .which-housekeeper-thumb-btn').click(function () {
        let professional = JSON.parse($(this).closest('.item').find('input[name="professional[]"]').val());
        console.log(professional);
        var maid_img = document.getElementsByClassName("js-maid-img")[0];
        maid_img.src = professional.maid_img_url;
        document.getElementsByClassName("js-maid-name")[0].innerHTML = professional.name;
        $('#maid-details-popup').show(500);
    });
    /********************************************************* */
}
$('#maid-details-popup [data-action="close"]').click(function () {
    $('#maid-details-popup').hide(500);
});
$('#booking-form input[name=professionals_count]').change(function () {
    Cookies.set('professionals_count', this.value);
    $('#booking-summary .professionals_count').html(this.value);
    $('#booking-form input[name="professional_prefer[]"]').attr("disabled", false).prop('checked', false);
    $('#booking-form input[name="professional_prefer_auto"]').prop('checked', true);
    $('#booking-form input[name="professional_prefer[]"]').trigger('change');
    calculate();
});
$('#booking-form input[name="addons[]"]').change(function () {
    if (this.checked) {
        $('input[type="number"][data-addons_id="' + this.value + '"]').val(1);
    }
    else {
        $('input[type="number"][data-addons_id="' + this.value + '"]').val(0);
    }
    addonsToggled();
});
$('[data-action="addon-plus"]').click(function () {
    current_quantity = Number($('input[name="addons_quanity[]"][data-addons_id="' + $(this).attr("data-addons_id") + '"]').val());
    max_quantity = $('input[name="addons_quanity[]"][data-addons_id="' + $(this).attr("data-addons_id") + '"]').attr("max");
    if (current_quantity >= max_quantity) {
        $('input[name="addons_quanity[]"][data-addons_id="' + $(this).attr("data-addons_id") + '"]').val(max_quantity)
    }
    else {
        $('input[name="addons_quanity[]"][data-addons_id="' + $(this).attr("data-addons_id") + '"]').val(current_quantity + 1)
    }
    addonsToggled();
});
$('[data-action="addon-minus"]').click(function () {
    current_quantity = Number($('input[name="addons_quanity[]"][data-addons_id="' + $(this).attr("data-addons_id") + '"]').val());
    min_quantity = $('input[name="addons_quanity[]"][data-addons_id="' + $(this).attr("data-addons_id") + '"]').attr("min");
    updated_quantity = current_quantity - 1;
    if (updated_quantity == 0) {
        $('#addon_' + $(this).attr("data-addons_id")).prop('checked', false);
    }
    else if (updated_quantity > min_quantity) {
        $('input[name="addons_quanity[]"][data-addons_id="' + $(this).attr("data-addons_id") + '"]').val(updated_quantity)
    }
    else {
        $('input[name="addons_quanity[]"][data-addons_id="' + $(this).attr("data-addons_id") + '"]').val(min_quantity)
    }
    addonsToggled();
});
$('#booking-form input[name=payment_method]').change(function () {
    Cookies.set('payment_method', this.value);
    calculate();
});
/************************************************************************************************* */
var session = null; // apple pay session store
debug = true;
function logit(data) {
    if (debug == true) {
        console.log(data);
    }
};
function createBooking() {
    loader();
    booking_form = $('#booking-form');
    let payment_method = $('input[name="payment_method"]:checked', booking_form).val();
    var booking_btn = $('button[type="submit"]', booking_form);
    if (payment_method == 1) { // cash
        booking_btn.html(loading_button_html);
        booking_btn.attr('disabled', true);
        // continue
    }
    else if (payment_method == 2) { // card (payfort)
        booking_btn.html(loading_button_html);
        booking_btn.attr('disabled', true);
        // continue
    }
    else if (payment_method == 3) { // apple pay
        booking_btn.attr('disabled', true);
        if (_checkout_token_data) {
            // continue
        }
        else {
            loader();
            try {
                var runningAmount = _calculation_data.total_payable;
                var runningPP = 0;
                getShippingCosts('domestic_std', true);
                var runningTotal = function () {
                    return runningAmount + runningPP;
                }
                var shippingOption = "";
                var subTotalDescr = "Payment against Cleaning Service";
                function getShippingOptions(shippingCountry) {
                    logit('getShippingOptions: ' + shippingCountry);
                    if (shippingCountry.toUpperCase() == "AE") {
                        shippingOption = [{
                            label: 'Standard Shipping',
                            amount: getShippingCosts('domestic_std', true),
                            detail: '3-5 days',
                            identifier: 'domestic_std'
                        }, {
                            label: 'Expedited Shipping',
                            amount: getShippingCosts('domestic_exp', false),
                            detail: '1-3 days',
                            identifier: 'domestic_exp'
                        }];
                    } else {
                        shippingOption = [{
                            label: 'International Shipping',
                            amount: getShippingCosts('international', true),
                            detail: '5-10 days',
                            identifier: 'international'
                        }];
                    }
                }
                function getShippingCosts(shippingIdentifier, updateRunningPP) {
                    var shippingCost = 0;
                    switch (shippingIdentifier) {
                        case 'domestic_std':
                            shippingCost = 0;
                            break;
                        case 'domestic_exp':
                            shippingCost = 0;
                            break;
                        case 'international':
                            shippingCost = 0;
                            break;
                        default:
                            shippingCost = 0;
                    }
                    if (updateRunningPP == true) {
                        runningPP = shippingCost;
                    }
                    logit('getShippingCosts: ' + shippingIdentifier + " - " + shippingCost + "|" + runningPP);
                    return shippingCost;
                }
                var paymentRequest = {
                    currencyCode: 'AED',
                    countryCode: 'AE',
                    requiredShippingContactFields: ['postalAddress'],
                    lineItems: [{
                        label: subTotalDescr,
                        amount: runningAmount
                    }, {
                        label: 'P&P',
                        amount: runningPP
                    }],
                    total: {
                        label: 'Spectrum',
                        amount: runningTotal()
                    },
                    supportedNetworks: ['amex', 'masterCard', 'visa'],
                    merchantCapabilities: ['supports3DS', 'supportsCredit', 'supportsDebit']
                };
                session = new ApplePaySession(1, paymentRequest);
                // Merchant Validation
                session.onvalidatemerchant = function (event) {
                    logit(event);
                    var promise = performValidation(event.validationURL);
                    promise.then(function (merchantSession) {
                        session.completeMerchantValidation(merchantSession);
                    });
                }
                function performValidation(valURL) {
                    return new Promise(function (resolve, reject) {
                        var xhr = new XMLHttpRequest();
                        xhr.onload = function () {
                            var data = JSON.parse(this.responseText);
                            logit(data);
                            resolve(data);
                        };
                        xhr.onerror = reject;
                        xhr.open('GET', 'https://www.spectrumservices.ae/ApplePayJS/apple_pay_comm.php?u=' + valURL);
                        xhr.send();
                    });
                }
                session.onshippingcontactselected = function (event) {
                    logit('starting session.onshippingcontactselected');
                    logit(
                        'NB: At this stage, apple only reveals the Country, Locality and 4 characters of the PostCode to protect the privacy of what is only a *prospective* customer at this point. This is enough for you to determine shipping costs, but not the full address of the customer.');
                    logit(event);
                    getShippingOptions(event.shippingContact.countryCode);
                    var status = ApplePaySession.STATUS_SUCCESS;
                    var newShippingMethods = shippingOption;
                    var newTotal = {
                        type: 'final',
                        label: 'Spectrum',
                        amount: runningTotal()
                    };
                    var newLineItems = [{
                        type: 'final',
                        label: subTotalDescr,
                        amount: runningAmount
                    }, {
                        type: 'final',
                        label: 'P&P',
                        amount: runningPP
                    }];
                    session.completeShippingContactSelection(status, newShippingMethods, newTotal, newLineItems);
                }
                session.onshippingmethodselected = function (event) {
                    logit('starting session.onshippingmethodselected');
                    logit(event);
                    getShippingCosts(event.shippingMethod.identifier, true);
                    var status = ApplePaySession.STATUS_SUCCESS;
                    var newTotal = {
                        type: 'final',
                        label: 'Spectrum',
                        amount: runningTotal()
                    };
                    var newLineItems = [{
                        type: 'final',
                        label: subTotalDescr,
                        amount: runningAmount
                    }, {
                        type: 'final',
                        label: 'P&P',
                        amount: runningPP
                    }];
                    session.completeShippingMethodSelection(status, newTotal, newLineItems);
                }
                session.onpaymentmethodselected = function (event) {
                    logit('starting session.onpaymentmethodselected');
                    logit(event);
                    var newTotal = {
                        type: 'final',
                        label: 'Spectrum',
                        amount: runningTotal()
                    };
                    var newLineItems = [{
                        type: 'final',
                        label: subTotalDescr,
                        amount: runningAmount
                    }, {
                        type: 'final',
                        label: 'P&P',
                        amount: runningPP
                    }];
                    session.completePaymentMethodSelection(newTotal, newLineItems);
                }
                session.onpaymentauthorized = function (event) {
                    logit('starting session.onpaymentauthorized');
                    logit(
                        'NB: This is the first stage when you get the *full shipping address* of the customer, in the event.payment.shippingContact object');
                    logit(event.payment.token);
                    /**************************************** */
                    //var eventToken = JSON.stringify(event.payment.token.paymentData);
                    var eventToken = JSON.stringify(event.payment);
                    _checkout_token_data = eventToken;
                    /**************************************** */
                    //alert('Token Success : ');
                    createBooking();
                }
                function sendPaymentToken(paymentToken) {
                    return new Promise(function (resolve, reject) {
                        logit('starting function sendPaymentToken()');
                        logit(
                            "this is where you would pass the payment token to your third-party payment provider to use the token to charge the card. Only if your provider tells you the payment was successful should you return a resolve(true) here. Otherwise reject;");
                        logit(
                            "defaulting to resolve(true) here, just to show what a successfully completed transaction flow looks like");
                        if (debug == true)
                            resolve(true);
                        else
                            reject;
                    });
                }
                session.oncancel = function (event) {
                    loader(false);
                    $('button[type="submit"]', booking_form).attr('disabled', false);
                }
                session.begin();
            }
            catch (err) {
                toast("Apple Exception", err.message, 'error');
                loader(false);
                $('button[type="submit"]', booking_form).attr('disabled', false);
            }
            return false;
        }
    }
    create_booking_req = $.ajax({
        type: 'POST',
        url: _base_url + "api/customer/create_booking_new",
        dataType: 'json',
        data: {
            calculation_data: _calculation_data,
            checkout_token_data: _checkout_token_data
        },
        beforeSend: function () {
            if (create_booking_req != null) {
                create_booking_req.abort();
            }
        },
        success: function (response) {
            if (response.result.status != 'success') {
                loader(false);
                if (_calculation_data.input.PaymentMethod == 3) {
                    session.completePayment(ApplePaySession.STATUS_FAILURE);
                }
                toast("An error occured.", response.result.message, 'error');
            }
            else {
                if (_calculation_data.input.PaymentMethod == 1) {
                    loader(false);
                    booking_btn.html('Completed');
                    Swal.fire({
                        title: "Booking Received !",
                        html: "Booking received with Ref. No. <b>" + response.result.booking_details.booking_reference_id + '</b>',
                        icon: "success",
                        //iconHtml: '<img src="' + _base_url + 'svg/checkmark1.svg' + '"/>',
                        allowOutsideClick: false,
                        confirmButtonText: 'Show Details',
                        timer: 3000,
                    }).then((result) => {
                        if (result.isConfirmed || result.dismiss === swal.DismissReason.timer) {
                            window.location.href = _base_url + 'booking/success/' + response.result.booking_details.booking_reference_id;
                        }
                    });
                }
                else if (_calculation_data.input.PaymentMethod == 2) {
                    /******************* open url with post data from api ************** */
                    var form_fields_html = ``;
                    $.each(response.result.checkout_data.payfort_data, function (field, value) {
                        form_fields_html += `<input type="hidden" name="` + field + `" value="` + value + `" />`;
                    });
                    let form = $('<form action="' + response.result.checkout_url + '" method="post">' +
                        form_fields_html +
                        '</form>');
                    $('body').append(form);
                    form.submit();
                    //$('#payfort-form input[name="signature"]').val(response.result.checkout_data.payfort_data.signature);
                    //$('#payfort-form input[name="return_url"]').val(response.result.checkout_data.payfort_data.return_url);
                    //$('#payfort-form input[name="merchant_reference"]').val(response.result.checkout_data.payfort_data.merchant_reference);
                }
                else if (_calculation_data.input.PaymentMethod == 3) {
                    session.completePayment(ApplePaySession.STATUS_SUCCESS);
                    loader(false);
                    // apple pay success
                    Swal.fire({
                        title: "Booking Received !",
                        html: "Booking received with Ref. No. <b>" + response.result.booking_details.booking_reference_id + '</b>',
                        icon: "success",
                        //iconHtml: '<img src="' + _base_url + 'svg/checkmark1.svg' + '"/>',
                        allowOutsideClick: false,
                        confirmButtonText: 'Show Details',
                        timer: 3000,
                    }).then((result) => {
                        if (result.isConfirmed || result.dismiss === swal.DismissReason.timer) {
                            window.location.href = _base_url + 'booking/success/' + response.result.booking_details.booking_reference_id;
                        }
                    });
                }
            }
        },
        error: function (response) {
            ajaxError(null, response);
            //booking_btn.attr('disabled', false);
        },
    });
}
/************************************************************************************************* */
create_booking_req = null;
booking_form_validator = $('#booking-form').validate({
    focusInvalid: false,
    ignore: [],
    rules: {
        "date": {
            required: true,
        },
        "time": {
            required: true,
        },
        "hours": {
            required: true,
        },
        "professionals_count": {
            required: true,
        },
        "instructions": {
            required: false,
        },
        "payment_method": {
            required: true,
        },
    },
    messages: {
        "date": {
            required: "Select service date",
        },
        "time": {
            required: "Select service time",
        },
        "hours": {
            required: "Select service hours",
        },
        "professionals_count": {
            required: "Select no. of professionals",
        },
        "payment_method": {
            required: "Select payment method",
        },
    },
    errorPlacement: function (error, element) {
        if (element.attr("name") == "time") {
            error.insertAfter($('#booking-times-holder').parent());
        }
        else if (element.attr("name") == "hours") {
            error.insertAfter($('#hours-count-holder').parent());
        }
        else if (element.attr("name") == "professionals_count") {
            error.insertAfter($('#professionals-count-holder').parent());
        }
        else if (element.attr("name") == "instructions") {
            error.insertAfter($('#booking-form textarea[name="instructions"]').parent());
        }
        else if (element.attr("name") == "payment_method") {
            error.insertAfter($('#payment-method-holder').parent());
        }
        else {
            error.insertAfter(element);
        }
        formValidationError(null, error);
    },
    submitHandler: function (form) {
        booking_form = form;
        createBooking();
    }
});
$().ready(function () {
    let searchParams = new URLSearchParams(window.location.search);
    if (searchParams.has('step') && searchParams.get('step') >= 2 && searchParams.get('step') <= 3) {
        currentStep = searchParams.get('step');
        var step = 1;
        var gotoStep = 1;
        do {
            if (isStepValid(step) === true) {
                step++;
            }
            else {
                step = currentStep;
            }
            gotoStep = step;
        }
        while (step < currentStep);
        $('[data-action="normal-step-' + gotoStep + '"]').trigger('click');
    }
    $('[data-action="close-frequency-popup"]').hide();
    $('[data-action="frequency-select"]').hide();
    $('[data-action="frequency-change"]').show();
    $('#booking-form input[name="date"]:first').trigger('change');
    if (Cookies.get('professionals_count')) {
        // try to keep selected hours if available
        if ($('#booking-form input[name="professionals_count"][value="' + Cookies.get('professionals_count') + '"]').length > 0) {
            $('#booking-form input[name="professionals_count"][value="' + Cookies.get('professionals_count') + '"]').prop("checked", true).trigger('change');
        }
        else {
            $('#booking-form input[name="professionals_count"]:first').trigger('change');
        }
    }
    else {
        $('#booking-form input[name="professionals_count"]:first').trigger('change');
    }
});
var owl = $("#calendar");
owl.owlCarousel({
    nav: true,
    loop: false,
    dots: false,
    margin: 15,
    autoplay: false,
    autoplayTimeout: 2000,
    smartSpeed: 800,
    autoplayHoverPause: false,
    responsive: {
        0: {
            items: 5
        },
        600: {
            items: 5
        },
        1000: {
            items: 10
        },
        1100: {
            items: 10
        },
        1200: {
            items: 10,
            //margin: 50
        }
    }
});
var owl = $("#housekeepers");
owl.owlCarousel({
    nav: false,
    loop: false,
    dots: true,
    margin: 15,
    autoplay: false,
    autoplayTimeout: 2000,
    smartSpeed: 800,
    autoplayHoverPause: true,
    responsive: {
        0: {
            items: 2
        },
        600: {
            items: 4
        },
        1000: {
            items: 5
        },
        1100: {
            items: 5
        },
        1200: {
            items: 6,
            //margin: 50
        }
    }
});
var owl = $("#add-ons");
owl.owlCarousel({
    nav: false,
    loop: false,
    dots: true,
    margin: 15,
    autoplay: false,
    autoplayTimeout: 2000,
    smartSpeed: 800,
    autoplayHoverPause: true,
    responsive: {
        0: {
            items: 1
        },
        600: {
            items: 2
        },
        1000: {
            items: 3
        },
        1100: {
            items: 3
        },
        1200: {
            items: 3,
            //margin: 50
        }
    }
});
calculate_req = null;
function calculate() {
    booking_form = $('#booking-form');
    let booking_btn = $('button[type="submit"]', booking_form);
    booking_btn.prop("disabled", true);
    professional_prefer = [];
    $('input[name="professional_prefer[]"]:checked', booking_form).each(function (index, obj) {
        var professional = {
            professional_id: $(this).val(),
        }
        professional_prefer.push(professional);

    });
    calculate_req = $.ajax({
        type: 'POST',
        url: _base_url + "api/customer/calculate",
        dataType: 'json',
        data: {
            address_id: $('input[name="address_id"]', booking_form).val(),
            //frequency: $('input[name="frequency"]:checked', booking_form).val(),
            //frequency_discount: $('input[name="frequency_discount"]', booking_form).val(),
            date: $('input[name="date"]:checked', booking_form).val(),
            time: $('input[name="time"]:checked', booking_form).val(),
            //hours: $('input[name="hours"]:checked', booking_form).val(),
            //professionals_count: $('input[name="professionals_count"]:checked', booking_form).val(),
            professional_prefer: professional_prefer,
            instructions: $('textarea[name="instructions"]', booking_form).val(),
            //promocode_id: $('input[name="promocode_id"]', booking_form).val(),
            PaymentMethod: $('input[name="payment_method"]:checked', booking_form).val(),
            //No_weeks: $('input[name="No_weeks"]', booking_form).val(),
            service_type_id: $('input[name="service_type_id"]', booking_form).val(),
            offer_id: $('input[name="offer_id"]', booking_form).val(),
            rush_slot_id: $('input[name="rush_slot_id"]', booking_form).val(),
        },
        beforeSend: function () {
            if (calculate_req != null) {
                calculate_req.abort();
            }
        },
        success: function (response) {
            _calculation_data = response.result.calculation_data;
            $('input[name="frequency_discount"]', booking_form).val(_calculation_data.frequency_discount);
            $('calc-amount.tax_percentage').html(Number(_calculation_data.tax_percentage).toLocaleString('en-US', {
                minimumFractionDigits: 1,
                maximumFractionDigits: 1
            }));
            $('calc-amount.service_fee').html(Number(_calculation_data.service_fee).toLocaleString('en-US', {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
            }));
            $('calc-amount.cleaning_material_fee').html(Number(_calculation_data.cleaning_material_fee).toLocaleString('en-US', {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
            }));
            $('calc-amount.rush_slot_charge').html(Number(_calculation_data.rush_slot_charge).toLocaleString('en-US', {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
            }));
            $('calc-amount.addons_amount').html(Number(_calculation_data.addons_amount).toLocaleString('en-US', {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
            }));
            $('calc-amount.vat_amount').html(Number(_calculation_data.vat_amount).toLocaleString('en-US', {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
            }));
            $('calc-amount.discount_total').html(Number(_calculation_data.discount_total).toLocaleString('en-US', {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
            }));
            $('calc-amount.taxable_amount').html(Number(_calculation_data.taxable_amount).toLocaleString('en-US', {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
            }));
            $('calc-amount.taxed_amount').html(Number(_calculation_data.taxed_amount).toLocaleString('en-US', {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
            }));
            $('calc-amount.payment_type_charge').html(Number(_calculation_data.payment_type_charge).toLocaleString('en-US', {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
            }));
            $('calc-amount.total_payable').html(Number(_calculation_data.total_payable).toLocaleString('en-US', {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
            }));
            if (_calculation_data.rush_slot_charge == null) {
                $('#booking-summary .rush_slot_charge').hide();
            }
            else {
                $('#booking-summary .rush_slot_charge').show();
            }
            if (_calculation_data.cleaning_material_fee > 0) {
                $('#booking-summary .cleaning_material_fee').show();
            }
            else {
                $('#booking-summary .cleaning_material_fee').hide();
            }
            if (_calculation_data.payment_type_charge > 0) {
                $('.payment_type_charge').show();
            }
            else {
                $('.payment_type_charge').hide();
            }
            if (_calculation_data.addons_amount != null) {
                $('#booking-summary .addons_amount').show();
            }
            else {
                $('#booking-summary .addons_amount').hide();
            }
            if (_calculation_data.discount_total > 0) {
                $('#booking-summary .discount_total').show();
            }
            else {
                $('#booking-summary .discount_total').hide();
            }
            booking_btn.prop("disabled", false);
        },
        error: function (response) {
        },
    });
}