/************************************************************************************
 *                                                                                  *
 *                          File Author : Samnad.S                                  *
 *                                                                                  *
 ************************************************************************************/
var crew_list_req = null;
/************************************************************************************************* */
$('[data-action="next-step"]').click(function () {
    booking_form = $('#booking-form');
    let current_step = Number($(this).attr('data-step'));
    if (isStepValid(current_step) === false) {
        // DONT'T GO TO NEXT STEP
        return;
    }
    let next_step = current_step + 1;
    // do before going to step
    if (next_step == 3) {
        if ($('input[name="id"]', booking_form).val() == '') {
            // not logged in
            $('.login-popup').show(500);
            toast('Login Required', 'Please login to continue !', 'info');
            return false;
        } else {
            // logged in
            if (_address_list.length == 0) {
                // there is no address added
                $('#new-address-popup-form input[name="next_action"]').val('set_booking_address');
                showAddressForm();
                toast('Address Required', 'Add address to continue', 'info');
                return false;
            }
        }
    }
    //
    $('.step-' + current_step).hide();
    $('.step-' + next_step).show();
    // do after going to step
    if (next_step == 3) {
        $('input[name="payment_method"]:checked', booking_form).trigger('change');
    }
    window.history.pushState({}, "", decodeURI(_current_url + $.query.set('step', next_step)));
    window.scrollTo(0, 0);
});
$('[data-action="prev-step"]').click(function () {
    booking_form = $('#booking-form');
    let current_step = Number($(this).attr('data-step'));
    let prev_step = current_step - 1;
    //
    $('.step-' + current_step).hide();
    $('.step-' + prev_step).show();
    /************************************* */
    $(document).prop('title', _meta_title + ' ' + prev_step + '/4');
    /************************************* */
    if (prev_step == 3) {
        //$('#frequency-popup').show(500);
    }
    window.history.pushState({}, "", decodeURI(_current_url + $.query.set('step', prev_step)));
    window.scrollTo(0, 0);
});
/************************************************************************************************* */
// steps validator
function isStepValid(step) {
    var required = [];
    if (step == 1) {
        required = [{
            name: 'packages[]',
            status: $('input[name="packages[]"]', booking_form).valid()
        }
        ];
    } else if (step == 2) {
        required = [{
            name: 'date',
            status: $('input[name="date"]', booking_form).valid()
        },
        {
            name: 'time',
            status: $('input[name="time"]', booking_form).valid()
        },
        {
            name: 'instructions',
            status: $('textarea[name="instructions"]', booking_form).valid()
        },
        ];
    } else if (step == 3) {
        required = [{
            name: 'payment_method',
            status: $('input[name="payment_method"]', booking_form).valid()
        },];
    }
    /************************************************ */
    // check for errors and show as toast
    var errors = required.filter(field => {
        return field.status === false
    });
    if (errors.length > 0) {
        var first_error_field = errors[0].name;
        var error_message = booking_form_validator.submitted[first_error_field];
        toast(null, error_message, 'info');
        scrollToElement($('[name="' + first_error_field + '"]', booking_form));
        return false;
    }
    /************************************************ */
    // step valid
    return true;
}
function goToStep(step) {
    $('div[class*=step-]').hide();
    $('.step-' + step).show();
}
/************************************************************************************************* */
$('button[type="submit"]', booking_form).click(function () {
    isStepValid(3);
});
/************************************************************************************************* */
$('[data-action="package-details"]').click(function () {
    // show addons details popup
    let package = JSON.parse($(this).closest('.booking-packages').find("input[name='package[]']").val());
    packageDetailsShow(package);
});
$('#package-details-popup .js-close').click(function () {
    // hide addons details popup
    packageDetailsHide();
});
function packageDetailsShow(package) {
    console.log(package);
    $('#package-img').attr("src", package.imageurl);
    $('#package-details-popup .name').html(package.package_name);
    $('#package-details-popup .description').html(package.package_description);
    $('#package-details-popup .package_amount').html('AED <span>' + package.actual_amount + '</span>  ' + package.amount);
    $('#package-details-popup').show(500);
}
function packageDetailsHide() {
    $('#package-details-popup').hide(500);
}
/************************************************************************************************* */
function packageToggled() {
    _cart.packages = [];
    $('input[name="packages[]"]:checked', booking_form).each(function (index, obj) {
        let quantity = $('#package_quanity_' + $(this).val()).val();
        var package = {
            package_id: $(this).val(),
            quantity: quantity,
        }
        _cart.packages.push(package);
    });
    if (_cart.packages.length == 0) {
        toast(null, "Please select atleast one package.", 'info');
        goToStep(1);
    }
    calculate();
}
$('#booking-form input[name="packages[]"]').change(function () {
    if (this.checked) {
        $('input[type="number"][data-package_id="' + this.value + '"]').val(1);
        $('#package-summary-row-' + this.value).show();
    }
    else {
        $('input[type="number"][data-package_id="' + this.value + '"]').val(0);
        $('#package-summary-row-' + this.value).hide();
    }
    packageToggled();
});
$('[data-action="package-plus"]').click(function () {
    current_quantity = Number($('input[name="package_quanity[]"][data-package_id="' + $(this).attr("data-package_id") + '"]').val());
    max_quantity = $('input[name="package_quanity[]"][data-package_id="' + $(this).attr("data-package_id") + '"]').attr("max");
    if (current_quantity >= max_quantity) {
        $('input[name="package_quanity[]"][data-package_id="' + $(this).attr("data-package_id") + '"]').val(max_quantity);
    }
    else {
        $('input[name="package_quanity[]"][data-package_id="' + $(this).attr("data-package_id") + '"]').val(current_quantity + 1);
    }
    packageToggled();
});
$('[data-action="package-minus"]').click(function () {
    current_quantity = Number($('input[name="package_quanity[]"][data-package_id="' + $(this).attr("data-package_id") + '"]').val());
    min_quantity = $('input[name="package_quanity[]"][data-package_id="' + $(this).attr("data-package_id") + '"]').attr("min");
    updated_quantity = current_quantity - 1;
    if (updated_quantity == 0) {
        $('#package-' + $(this).attr("data-package_id")).prop('checked', false);
        $('#package-summary-row-' + $(this).attr("data-package_id")).hide();
    }
    else if (updated_quantity > min_quantity) {
        $('input[name="package_quanity[]"][data-package_id="' + $(this).attr("data-package_id") + '"]').val(updated_quantity);
    }
    else {
        $('input[name="package_quanity[]"][data-package_id="' + $(this).attr("data-package_id") + '"]').val(min_quantity);
    }
    packageToggled();
});
/************************************************************************************************* */
var session = null; // apple pay session store
debug = true;
function logit(data) {
    if (debug == true) {
        console.log(data);
    }
};
function createBooking() {
    loader();
    booking_form = $('#booking-form');
    let payment_method = $('input[name="payment_method"]:checked', booking_form).val();
    var booking_btn = $('button[type="submit"]', booking_form);
    if (payment_method == 1) { // cash
        booking_btn.html(loading_button_html);
        booking_btn.attr('disabled', true);
        // continue
    }
    else if (payment_method == 2) { // card (payfort)
        booking_btn.html(loading_button_html);
        booking_btn.attr('disabled', true);
        // continue
    }
    else if (payment_method == 3) { // apple pay
        booking_btn.attr('disabled', true);
        if (_checkout_token_data) {
            // continue
        }
        else {
            try {
                var runningAmount = _calculation_data.total_payable;
                var runningPP = 0;
                getShippingCosts('domestic_std', true);
                var runningTotal = function () {
                    return runningAmount + runningPP;
                }
                var shippingOption = "";
                var subTotalDescr = "Payment against Cleaning Service";
                function getShippingOptions(shippingCountry) {
                    logit('getShippingOptions: ' + shippingCountry);
                    if (shippingCountry.toUpperCase() == "AE") {
                        shippingOption = [{
                            label: 'Standard Shipping',
                            amount: getShippingCosts('domestic_std', true),
                            detail: '3-5 days',
                            identifier: 'domestic_std'
                        }, {
                            label: 'Expedited Shipping',
                            amount: getShippingCosts('domestic_exp', false),
                            detail: '1-3 days',
                            identifier: 'domestic_exp'
                        }];
                    } else {
                        shippingOption = [{
                            label: 'International Shipping',
                            amount: getShippingCosts('international', true),
                            detail: '5-10 days',
                            identifier: 'international'
                        }];
                    }
                }
                function getShippingCosts(shippingIdentifier, updateRunningPP) {
                    var shippingCost = 0;
                    switch (shippingIdentifier) {
                        case 'domestic_std':
                            shippingCost = 0;
                            break;
                        case 'domestic_exp':
                            shippingCost = 0;
                            break;
                        case 'international':
                            shippingCost = 0;
                            break;
                        default:
                            shippingCost = 0;
                    }
                    if (updateRunningPP == true) {
                        runningPP = shippingCost;
                    }
                    logit('getShippingCosts: ' + shippingIdentifier + " - " + shippingCost + "|" + runningPP);
                    return shippingCost;
                }
                var paymentRequest = {
                    currencyCode: 'AED',
                    countryCode: 'AE',
                    requiredShippingContactFields: ['postalAddress'],
                    lineItems: [{
                        label: subTotalDescr,
                        amount: runningAmount
                    }, {
                        label: 'P&P',
                        amount: runningPP
                    }],
                    total: {
                        label: 'Spectrum',
                        amount: runningTotal()
                    },
                    supportedNetworks: ['amex', 'masterCard', 'visa'],
                    merchantCapabilities: ['supports3DS', 'supportsCredit', 'supportsDebit']
                };
                session = new ApplePaySession(1, paymentRequest);
                // Merchant Validation
                session.onvalidatemerchant = function (event) {
                    logit(event);
                    var promise = performValidation(event.validationURL);
                    promise.then(function (merchantSession) {
                        session.completeMerchantValidation(merchantSession);
                    });
                }
                function performValidation(valURL) {
                    return new Promise(function (resolve, reject) {
                        var xhr = new XMLHttpRequest();
                        xhr.onload = function () {
                            var data = JSON.parse(this.responseText);
                            logit(data);
                            resolve(data);
                        };
                        xhr.onerror = reject;
                        xhr.open('GET', 'https://www.spectrumservices.ae/ApplePayJS/apple_pay_comm.php?u=' + valURL);
                        xhr.send();
                    });
                }
                session.onshippingcontactselected = function (event) {
                    logit('starting session.onshippingcontactselected');
                    logit(
                        'NB: At this stage, apple only reveals the Country, Locality and 4 characters of the PostCode to protect the privacy of what is only a *prospective* customer at this point. This is enough for you to determine shipping costs, but not the full address of the customer.');
                    logit(event);
                    getShippingOptions(event.shippingContact.countryCode);
                    var status = ApplePaySession.STATUS_SUCCESS;
                    var newShippingMethods = shippingOption;
                    var newTotal = {
                        type: 'final',
                        label: 'Spectrum',
                        amount: runningTotal()
                    };
                    var newLineItems = [{
                        type: 'final',
                        label: subTotalDescr,
                        amount: runningAmount
                    }, {
                        type: 'final',
                        label: 'P&P',
                        amount: runningPP
                    }];
                    session.completeShippingContactSelection(status, newShippingMethods, newTotal, newLineItems);
                }
                session.onshippingmethodselected = function (event) {
                    logit('starting session.onshippingmethodselected');
                    logit(event);
                    getShippingCosts(event.shippingMethod.identifier, true);
                    var status = ApplePaySession.STATUS_SUCCESS;
                    var newTotal = {
                        type: 'final',
                        label: 'Spectrum',
                        amount: runningTotal()
                    };
                    var newLineItems = [{
                        type: 'final',
                        label: subTotalDescr,
                        amount: runningAmount
                    }, {
                        type: 'final',
                        label: 'P&P',
                        amount: runningPP
                    }];
                    session.completeShippingMethodSelection(status, newTotal, newLineItems);
                }
                session.onpaymentmethodselected = function (event) {
                    logit('starting session.onpaymentmethodselected');
                    logit(event);
                    var newTotal = {
                        type: 'final',
                        label: 'Spectrum',
                        amount: runningTotal()
                    };
                    var newLineItems = [{
                        type: 'final',
                        label: subTotalDescr,
                        amount: runningAmount
                    }, {
                        type: 'final',
                        label: 'P&P',
                        amount: runningPP
                    }];
                    session.completePaymentMethodSelection(newTotal, newLineItems);
                }
                session.onpaymentauthorized = function (event) {
                    logit('starting session.onpaymentauthorized');
                    logit(
                        'NB: This is the first stage when you get the *full shipping address* of the customer, in the event.payment.shippingContact object');
                    logit(event.payment.token);
                    /**************************************** */
                    //var eventToken = JSON.stringify(event.payment.token.paymentData);
                    var eventToken = JSON.stringify(event.payment);
                    _checkout_token_data = eventToken;
                    /**************************************** */
                    //alert('Token Success : ');
                    createBooking();
                }
                function sendPaymentToken(paymentToken) {
                    return new Promise(function (resolve, reject) {
                        logit('starting function sendPaymentToken()');
                        logit(
                            "this is where you would pass the payment token to your third-party payment provider to use the token to charge the card. Only if your provider tells you the payment was successful should you return a resolve(true) here. Otherwise reject;");
                        logit(
                            "defaulting to resolve(true) here, just to show what a successfully completed transaction flow looks like");
                        if (debug == true)
                            resolve(true);
                        else
                            reject;
                    });
                }
                session.oncancel = function (event) {
                    loader(false);
                    $('button[type="submit"]', booking_form).attr('disabled', false);
                }
                session.begin();
            }
            catch (err) {
                toast("Apple Exception", err.message, 'error');
                loader(false);
                $('button[type="submit"]', booking_form).attr('disabled', false);
            }
            return false;
        }
    }
    booking_btn = $('button[type="submit"]', booking_form);
    booking_btn.html(loading_button_html);
    //booking_btn.attr('disabled', true);
    create_booking_req = $.ajax({
        type: 'POST',
        url: _base_url + "api/customer/create_booking_new",
        dataType: 'json',
        data: {
            calculation_data: _calculation_data,
            checkout_token_data: _checkout_token_data
        },
        beforeSend: function () {
            if (create_booking_req != null) {
                create_booking_req.abort();
            }
        },
        success: function (response) {
            if (response.result.status == "failed") {
                loader(false);
                toast("An error occured", response.result.message, 'error');
            }
            else {
                if (_calculation_data.input.PaymentMethod == 1) {
                    loader(false);
                    booking_btn.html('Completed');
                    Swal.fire({
                        title: "Booking Received !",
                        html: "Booking received with Ref. No. <b>" + response.result.booking_details.booking_reference_id + '</b>',
                        icon: "success",
                        //iconHtml: '<img src="' + _base_url + 'svg/checkmark1.svg' + '"/>',
                        allowOutsideClick: false,
                        confirmButtonText: 'Show Details',
                        timer: 3000,
                    }).then((result) => {
                        if (result.isConfirmed || result.dismiss === swal.DismissReason.timer) {
                            window.location.href = _base_url + 'booking/success/' + response.result.booking_details.booking_reference_id;
                        }
                    });
                }
                else if (_calculation_data.input.PaymentMethod == 2) {
                    /******************* open url with post data from api ************** */
                    var form_fields_html = ``;
                    $.each(response.result.checkout_data.payfort_data, function (field, value) {
                        form_fields_html += `<input type="hidden" name="` + field + `" value="` + value + `" />`;
                    });
                    let form = $('<form action="' + response.result.checkout_url + '" method="post">' +
                        form_fields_html +
                        '</form>');
                    $('body').append(form);
                    form.submit();
                    //$('#payfort-form input[name="signature"]').val(response.result.checkout_data.payfort_data.signature);
                    //$('#payfort-form input[name="return_url"]').val(response.result.checkout_data.payfort_data.return_url);
                    //$('#payfort-form input[name="merchant_reference"]').val(response.result.checkout_data.payfort_data.merchant_reference);
                }
                else if (_calculation_data.input.PaymentMethod == 3) {
                    session.completePayment(ApplePaySession.STATUS_SUCCESS);
                    loader(false);
                    // apple pay success
                    Swal.fire({
                        title: "Booking Received !",
                        html: "Booking received with Ref. No. <b>" + response.result.booking_details.booking_reference_id + '</b>',
                        icon: "success",
                        //iconHtml: '<img src="' + _base_url + 'svg/checkmark1.svg' + '"/>',
                        allowOutsideClick: false,
                        confirmButtonText: 'Show Details',
                        timer: 3000,
                    }).then((result) => {
                        if (result.isConfirmed || result.dismiss === swal.DismissReason.timer) {
                            window.location.href = _base_url + 'booking/success/' + response.result.booking_details.booking_reference_id;
                        }
                    });
                }
            }
        },
        error: function (response) {
            ajaxError(null, response);
            //booking_btn.attr('disabled', false);
        },
    });
}
/************************************************************************************************* */
create_booking_req = null;
booking_form_validator = $('#booking-form').validate({
    focusInvalid: false,
    ignore: [],
    rules: {
        "packages[]": {
            required: true,
            minlength: 1
        },
        "date": {
            required: true,
        },
        "time": {
            required: true,
        },
        "instructions": {
            required: false,
        },
        "payment_method": {
            required: true,
        },
    },
    messages: {
        "packages[]": {
            required: "Select atleast one package",
        },
        "date": {
            required: "Select service date",
        },
        "time": {
            required: "Select service time",
        },
        "instructions": {
            required: "Please add instructions",
        },
        "payment_method": {
            required: "Select payment method",
        },
    },
    errorPlacement: function (error, element) {
        if (element.attr("name") == "packages[]") {
        }
        else if (element.attr("name") == "time") {
            error.insertAfter($('#booking-times-holder').parent());
        }
        else if (element.attr("name") == "instructions") {
            error.insertAfter($('#booking-form textarea[name="instructions"]').parent());
        }
        else if (element.attr("name") == "payment_method") {
            error.insertAfter($('#payment-method-holder').parent());
        } else {
            error.insertAfter(element);
        }
    },
    submitHandler: function (form) {
        booking_form = form;
        createBooking();
    }
});
/************************************************************/
var available_time_req = null
$('#booking-form input[name=date]').change(function () {
    selected_time = $('#booking-form input[name="time"]:checked').val();
    if (selected_time) {
        // save selected time
        Cookies.set('time', selected_time);
    }
    var summary_date = moment(this.value, 'DD/MM/YYYY').format('DD MMM YYYY');
    $('#booking-summary .date').html(summary_date);
    params = { date: this.value };
    $('#booking-times-holder').html(loading_html);
    available_time_req = $.ajax({
        type: 'GET',
        url: _base_url + "api/customer/available_time",
        dataType: 'json',
        data: params,
        beforeSend: function () {
            if (available_time_req != null) {
                available_time_req.abort();
            }
        },
        success: function (response) {
            if (response.result.status == "success") {
                if (response.result.available_times.length == 0) {
                    $('#booking-times-holder').html(no_time_slot_available_html);
                    $('#hours-count-holder').html(``);
                }
                else {
                    times = '';
                    $.each(response.result.available_times, function (index, available_time) {
                        var rush_time = response.result.rush_times.find(obj => {
                            return obj.time == available_time
                        })
                        if (rush_time !== undefined) {
                            rush_time_html = `<p>AED ` + rush_time.extra_charge + ` Extra</p>`;
                            rush_time_data = `data-rush_slot_id="` + rush_time.id + `"`;
                        }
                        else {
                            rush_time_html = ``;
                            rush_time_data = ``;
                        }
                        times += `<li>
                                <input id="booking_time_`+ index + `" value="` + available_time + `" name="time" class="" type="radio" ` + rush_time_data + `>
                                <label for="booking_time_`+ index + `">` + moment(available_time, "HH:mmm").format("hh:mm A") + ` ` + rush_time_html + `</label>
                            </li>`;
                    });
                    $('#booking-times-holder').html(times);
                    timeTrigger(); // create event listener
                    /********************************************************* */
                    // keep same time
                    if (Cookies.get('time')) {
                        // try to keep selected time if available
                        var same_time_found = response.result.available_times.find(obj => {
                            return obj == Cookies.get('time')
                        });
                        if (same_time_found) {
                            $('#booking-form input[name="time"][value="' + same_time_found + '"]').prop("checked", true).trigger('change');
                        }
                        else {
                            $('#booking-form input[name="time"]:first').trigger('change');
                        }
                    }
                    else {
                        $('#booking-form input[name="time"]:first').trigger('change');
                    }
                    /********************************************************* */
                }
            }
            else {
            }
        },
        error: function (response) {
            ajaxError(null, response);
        },
    });
});
$('#booking-form input[name=payment_method]').change(function () {
    //Cookies.set('payment_method', this.value);
    let payment_method = this.value;
    let booking_btn = $('#booking-form button[type="submit"]');
    $('[id^=submit-btn-pay-mode-]').hide();
    booking_btn.attr('disabled', false);
    if (payment_method == 1) {
        _checkout_token_data = undefined;
        $('#submit-btn-pay-mode-1').show();
        booking_btn.html('Complete');
    }
    else if (payment_method == 2) {
        _checkout_token_data = undefined;
        $('#submit-btn-pay-mode-2').show();
        booking_btn.html('Pay Now');
    }
    else if (payment_method == 3) {
        _checkout_token_data = undefined;
        booking_btn.html('');
        $('#submit-btn-pay-mode-3').show();
    }
    calculate();
});
function timeTrigger() {
    $('#booking-form input[name="time"]').change(function () {
        Cookies.set('time', this.value);
        $('#booking-form input[name="rush_slot_id"]').val($(this).attr("data-rush_slot_id"));
        /********************************************************* */
        var summary_time_from = moment(this.value, 'H:mm').format('hh:mm A');
        $('#booking-summary .time').html(summary_time_from);
        calculate();
    });
}

var owl = $("#calendar");
owl.owlCarousel({
    nav: true,
    loop: false,
    dots: false,
    margin: 15,
    autoplay: false,
    autoplayTimeout: 2000,
    smartSpeed: 800,
    autoplayHoverPause: false,
    responsive: {
        0: {
            items: 5
        },
        600: {
            items: 5
        },
        1000: {
            items: 10
        },
        1100: {
            items: 10
        },
        1200: {
            items: 10,
            //margin: 50
        }
    }
});
calculate_req = null;
function calculate() {
    var booking_form = $('#booking-form');
    let booking_btn = $('button[type="submit"]', booking_form);
    booking_btn.prop("disabled", true);
    let coupon_apply_btn = $('#coupon-apply-form button[type="submit"]');
    professional_prefer = [];
    $('input[name="professional_prefer[]"]:checked', booking_form).each(function (index, obj) {
        var professional = {
            professional_id: $(this).val(),
        }
        professional_prefer.push(professional);

    });
    calculate_req = $.ajax({
        type: 'POST',
        url: _base_url + "api/customer/calculate",
        dataType: 'json',
        data: {
            address_id: $('input[name="address_id"]', booking_form).val(),
            frequency: $('input[name="frequency"]:checked', booking_form).val(),
            frequency_discount: $('input[name="frequency_discount"]', booking_form).val(),
            date: $('input[name="date"]:checked', booking_form).val(),
            time: $('input[name="time"]:checked', booking_form).val(),
            hours: $('input[name="hours"]:checked', booking_form).val(),
            professionals_count: $('input[name="professionals_count"]:checked', booking_form).val(),
            professional_prefer: professional_prefer,
            cleaning_materials: $('input[name="cleaning_materials"]:checked', booking_form).val(),
            instructions: $('textarea[name="instructions"]', booking_form).val(),
            promocode_id: $('input[name="promocode_id"]', booking_form).val(),
            PaymentMethod: $('input[name="payment_method"]:checked', booking_form).val(),
            No_weeks: $('input[name="No_weeks"]', booking_form).val(),
            service_type_id: $('input[name="service_type_id"]', booking_form).val(),
            offer_id: $('input[name="offer_id"]', booking_form).val(),
            rush_slot_id: $('input[name="rush_slot_id"]', booking_form).val(),
            addons: [],
            packages: _cart.packages,
            coupon_code: _cart.coupon_code
        },
        beforeSend: function () {
            if (calculate_req != null) {
                calculate_req.abort();
            }
        },
        success: function (response) {
            _calculation_data = response.result.calculation_data;
            /************************************************************************************************ */
            coupon_apply_btn.html('Apply').prop("disabled", false);
            if (_calculation_data.coupon_applied != null) {
                // some coupon data found
                if (_coupon_applied == null) {
                    // no history on frontend
                    if (_calculation_data.coupon_applied.status == "success") {
                        toast(_calculation_data.coupon_applied.title, _calculation_data.coupon_applied.message, 'success');
                    }
                    else {
                        toast(_calculation_data.coupon_applied.title, _calculation_data.coupon_applied.message, 'error');
                    }
                }
                else {
                    // history found
                    if (_coupon_applied.coupon_code == _calculation_data.coupon_applied.coupon_code) {
                        // same coupon code found with history; do nothing
                        // show on error only
                        if (_calculation_data.coupon_applied.status != "success") {
                            toast(_calculation_data.coupon_applied.title, _calculation_data.coupon_applied.message, 'error');
                        } else {
                            toast(_calculation_data.coupon_applied.title, _calculation_data.coupon_applied.message, 'success');
                        }
                    }
                    else {
                        if (_calculation_data.coupon_applied.status == "success") {
                            toast(_calculation_data.coupon_applied.title, _calculation_data.coupon_applied.message, 'success');
                        }
                        else {
                            toast(_calculation_data.coupon_applied.title, _calculation_data.coupon_applied.message, 'error');
                        }
                    }
                }
            }
            if (_calculation_data.coupon_applied != null && _calculation_data.coupon_applied.status == "success") {
                $('.promo-code-message').html(_calculation_data.coupon_applied.message);
            }
            else {
                $('.promo-code-message').html("No voucher applied.");
            }
            hideCouponPopup();
            _coupon_applied = _calculation_data.coupon_applied;
            /************************************************************************************************ */
            $('input[name="frequency_discount"]', booking_form).val(_calculation_data.frequency_discount);
            $('calc-amount.tax_percentage').html(Number(_calculation_data.tax_percentage).toLocaleString('en-US', {
                minimumFractionDigits: 1,
                maximumFractionDigits: 1
            }));
            $('calc-amount.service_fee').html(Number(_calculation_data.service_fee).toLocaleString('en-US', {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
            }));
            $('calc-amount.cleaning_material_fee').html(Number(_calculation_data.cleaning_material_fee).toLocaleString('en-US', {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
            }));
            $('calc-amount.rush_slot_charge').html(Number(_calculation_data.rush_slot_charge).toLocaleString('en-US', {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
            }));
            $('calc-amount.addons_amount').html(Number(_calculation_data.addons_amount).toLocaleString('en-US', {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
            }));
            $('calc-amount.vat_amount').html(Number(_calculation_data.vat_amount).toLocaleString('en-US', {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
            }));
            $('calc-amount.discount_total').html(Number(_calculation_data.discount_total).toLocaleString('en-US', {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
            }));
            $('calc-amount.taxable_amount').html(Number(_calculation_data.taxable_amount).toLocaleString('en-US', {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
            }));
            $('calc-amount.taxed_amount').html(Number(_calculation_data.taxed_amount).toLocaleString('en-US', {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
            }));
            $('calc-amount.payment_type_charge').html(Number(_calculation_data.payment_type_charge).toLocaleString('en-US', {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
            }));
            $('calc-amount.total_payable').html(Number(_calculation_data.total_payable).toLocaleString('en-US', {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
            }));
            if (_calculation_data.rush_slot_charge == null) {
                $('#booking-summary .rush_slot_charge').hide();
            }
            else {
                $('#booking-summary .rush_slot_charge').show();
            }
            if (_calculation_data.cleaning_material_fee > 0) {
                $('#booking-summary .cleaning_material_fee').show();
            }
            else {
                $('#booking-summary .cleaning_material_fee').hide();
            }
            if (_calculation_data.payment_type_charge > 0) {
                $('.payment_type_charge').show();
            }
            else {
                $('.payment_type_charge').hide();
            }
            if (_calculation_data.addons_amount != null) {
                $('#booking-summary .addons_amount').show();
            }
            else {
                $('#booking-summary .addons_amount').hide();
            }
            if (_calculation_data.discount_total > 0) {
                $('#booking-summary .discount_total').show();
            }
            else {
                $('#booking-summary .discount_total').hide();
            }
            booking_btn.prop("disabled", false);
        },
        error: function (response) {
            //ajaxError('Calculation Error !',response);
        },
    });
}
$(document).ready(function () {
    $('#booking-form input[name="date"]:first').trigger('change');
    var strategy = undefined;
    var match = document.location.search.match(/strategy=(.\S+)/);
    if (match) strategy = match[1];
    $(function () {
        $('.sticker').fixie({
            topMargin: 37,
            pinSlop: 0,
            strategy: strategy,
            //pinnedBodyClass: 'showGlobalChange'
        });
    });
    var owl = $("#package-category");
    owl.owlCarousel({
        items: 7,
        autoWidth: true,
        loop: false,
        nav: true,
        dots: false,
        margin: 10,
        autoplay: false,
        autoplayTimeout: 2000,
        smartSpeed: 800,
        autoplayHoverPause: false,
        responsive: {
            0: {
                items: 4
            },
            600: {
                items: 4
            },
            1000: {
                items: 5
            },
            1100: {
                items: 6
            },
            1200: {
                items: 6,
                //margin: 50
            }
        }
    });
});