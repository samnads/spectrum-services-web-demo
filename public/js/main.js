/************************************************************************************
 *                                                                                  *
 *                          File Author : Samnad.S                                  *
 *                                                                                  *
 ************************************************************************************/
var _calculation_data = null;// for storing last calculation data response from calculation api
var loading_html = `<div class="d-flex justify-content-center">
  <div class="spinner-border" role="status">
    <span class="visually-hidden">Loading...</span>
  </div>
</div>`;
var loading_button_html = `<span class="spinner-border spinner-border-sm" aria-hidden="true"></span>
  <span class="visually-hidden" role="status">Loading...</span>&nbsp;`;
let _cart = {};
var _coupon_applied = null;
let address_form_popup = $('#address-form-popup');
let _address_list = [];
let booking_form = $('#booking-form');
var _checkout_token_data = undefined;
var event_ = null;
var no_time_slot_available_html = `<div class="alert alert-warning" style="background-color:#ffe3e3;" role="alert">No time slot available.</div>`;
/*************************************************** */
let login_form = $('#login-popup-form');
function showLogin() {
    login_form_validator.resetForm();
    $('#login-popup-form')[0].reset();
    $('#login-popup').show(500);
}
function hideLogin() {
    $('#login-popup').hide(500);
}
$('[data-action="login-popup"').click(function () {
    showLogin();
});
/*************************************************** */
let login_otp_popup_form = $('#login-otp-popup-form');
function showOtp() {
    startOTPTimer();
    $('#login-popup-form')[0].reset();
    login_otp_form_validator.resetForm();
    $('input[name="otp-1"]').val('');
    $('input[name="otp-2"]').val('');
    $('input[name="otp-3"]').val('');
    $('input[name="otp-4"]').val('');
    let submit_btn = $('[type="submit"]', login_otp_popup_form);
    submit_btn.html('Continue').prop("disabled", false);
    $('#otp-popup').show(500);
    $('input[name="otp-1"]').focus();
}
function hideOtp() {
    $('#otp-popup').hide(500);
}
/*************************************************** */
$('[data-action="my-account"').click(function () {
    window.open(_base_url + "account", "_self");
});
var WORK_END_TIME = moment("20:00", 'H:mm');
$().ready(function () {
    $('input[name="frequency"][value=OD]').prop("checked", "true")
    $.fn.serializeObjectForApi = function () {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return { 'params': o };
    };
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')
        }
    });
    login_form_validator = $('#login-popup-form').validate({
        focusInvalid: false,
        ignore: [],
        rules: {
            "country_code": {
                required: true,
            },
            "mobilenumber": {
                required: true,
                maxlength: 9,
                minlength: 9
            }
        },
        messages: {
            "country_code": {
                required: "Select your country",
            },
            "mobilenumber": {
                required: "Enter mobile number",
                maxlength: "Enter a valid mobile number",
                minlength: "Enter a valid mobile number",
            }
        },
        errorPlacement: function (error, element) {
            error.insertAfter(element);
        },
        submitHandler: function (form) {
            let btn = $('button[type="submit"]', form);
            btn.html(loading_button_html).attr('disabled', true);;
            $.ajax({
                type: 'POST',
                url: _base_url + "api/customer/customer_login",
                dataType: 'json',
                data: $('#login-popup-form').serialize(),
                success: function (response) {
                    btn.html('Continue').attr('disabled', false);
                    if (response.result.status == "success") {
                        toast('OTP Send', 'Enter the OTP to continue !', 'info');
                        $('#login-otp-popup-form input[name="mobilenumber"]').val(response.result.UserDetails.mobile);
                        $('.customer-full-mobile').html(response.result.UserDetails.mobile);
                        hideLogin();
                        showOtp();
                    }
                    else {
                        toast(null, response.result.message, 'error');
                    }
                },
                error: function (response) {
                    btn.html('Continue').attr('disabled', false);
                },
            });
        }
    });
    login_otp_form_validator = $('#login-otp-popup-form').validate({
        focusInvalid: false,
        ignore: [],
        rules: {
            "country_code": {
                required: true,
            },
            "mobilenumber": {
                required: true,
            },
            "otp": {
                required: true,
            }
        },
        messages: {
            "country_code": {
                required: "Select your country",
            },
            "mobilenumber": {
                required: "Enter mobile number",
            },
            "otp": {
                required: "Enter the OTP",
            }
        },
        errorPlacement: function (error, element) {
            error.insertAfter(element);
        },
        submitHandler: function (form) {
            $('[type="submit"]', form).html(loading_button_html).attr('disabled', true);
            $.ajax({
                type: 'POST',
                url: _base_url + "api/customer/check_otp",
                data: {
                    country_code: '+971',
                    mobilenumber: $('#login-otp-popup-form input[name="mobilenumber"]').val(),
                    otp: $('#login-otp-popup-form input[name="otp"]').val(),
                    fcm: null
                },
                success: function (response) {
                    if (response.result.status == "success") {
                        $('.before-login').hide();
                        $('.after-login').show();
                        _id = response.result.UserDetails.id;
                        $('input[name="id"]').val(response.result.UserDetails.id);
                        $('input[name="token"]').val(response.result.UserDetails.token);
                        $('input[name="address_id"]').val(response.result.UserDetails.default_address_id);
                        if ($('#booking-form input[name="service_type_id"]').val() > 0) {
                            // it means someone is in the middle of the booking form (need to preserve the data) :(
                            calculate();
                            toast('Login Success', "Successfully logged in...", 'success');
                            $('#otp-popup').hide(500);
                            if (response.result.UserDetails.UserName == null || response.result.UserDetails.UserName == "") {
                                showNameEntry();
                            }
                        }
                        else {
                            // may be not a service based form page (dont care about data)
                            $('#otp-popup').hide(500);
                            if (response.result.UserDetails.UserName == null || response.result.UserDetails.UserName == "") {
                                showNameEntry();
                            }
                            else {
                                window.location.reload();
                            }
                        }
                        showDefaultAddressOnStep();
                    }
                    else {
                        $('[type="submit"]', form).html('Continue').attr('disabled', false);
                        toast('Incorrect OTP', response.result.message, 'warning');
                    }
                },
                error: function (response) {
                    $('[type="submit"]', form).html('Continue').attr('disabled', false);
                },
            });
        }
    });
    name_entry_form_validator = $('#name-entry-form').validate({
        focusInvalid: false,
        ignore: [],
        rules: {
            "name": {
                required: true,
            },
            "email": {
                required: true,
            },
        },
        messages: {
            "name": {
                required: "Enter your name",
            },
            "email": {
                required: "Enter your email",
            },
        },
        errorPlacement: function (error, element) {
            error.insertAfter(element);
        },
        submitHandler: function (form) {
            let submit_btn = $('[type="submit"]', form).html(loading_button_html).attr('disabled', true);
            $.ajax({
                type: 'POST',
                url: _base_url + "api/customer/name_entry",
                data: $('#name-entry-form').serialize(),
                success: function (response) {
                    submit_btn.html('Save').attr('disabled', false);
                    if (response.result.status == "success") {
                        _name = response.result.UserDetails.UserName;
                        //window.location.reload();
                        hideNameEntry();
                    }
                    else {
                        toast('Error', response.result.message, 'error');
                    }
                },
                error: function (response) {
                    submit_btn.html('Save').attr('disabled', false);
                },
            });
        }
    });
    /********************************************************************************* */
    $('[data-action="logout"').click(function () {
        $.ajax({
            type: 'POST',
            url: _base_url + "api/customer/customer_logout",
            success: function (response) {
                window.location.href = _base_url;
            },
            error: function (response) {
            },
        });
    });
    /********************************************************************************* */
    $('[data-action="resend-otp"').click(function () {
        $('[type="submit"]', login_otp_popup_form).html(loading_button_html).attr('disabled', true);
        $.ajax({
            type: 'POST',
            url: _base_url + "api/customer/resend_otp",
            data: $('#login-otp-popup-form').serialize(),
            success: function (response) {
                if (response.result.status == "success") {
                    startOTPTimer();
                    toast('OTP Sent', response.result.message, 'success');
                }
                else {
                    toast(null, response.result.message, 'error');
                }
                $('[type="submit"]', login_otp_popup_form).html('Continue').attr('disabled', false);
            },
            error: function (response) {
                $('[type="submit"]', login_otp_popup_form).html('Continue').attr('disabled', false);
            },
        });
    });
    /********************************************************************************* */
    $('#login-popup [data-action="close-popup"]').click(function () {
        hideLogin();
    });
    $('#otp-popup [data-action="close-popup"]').click(function () {
        $('#otp-popup').hide(500);
    });
    $('[data-action="close-frequency-popup"]').click(function () {
        $('#frequency-popup').hide(500);
    });
    $('[data-action="frequency-select"]').click(function () {
        Cookies.set('frequency', $('input[name="frequency"]:checked').val()); // save frequency
        var web_url_slug = Cookies.get('web_url_slug') + '?step=1';
        Cookies.set('time', null); // rest time
        Cookies.set('hours', null); // rest hours
        Cookies.set('professionals_count', null); // rest professionals_count
        window.open(_base_url + web_url_slug, "_self");
    });
    $('[data-action="frequency-popup"]').click(function () {
        $('#frequency-popup').show(500);
    });
    /********************************************************************************* */
    if (_id != null) {
        $('.before-login').hide();
        $('.after-login').show();
    }
    if (_id != null && (nullOrEmpty(_name) || nullOrEmpty(_email))) {
        showNameEntry();
    }
    function showNameEntry() {
        if (nullOrEmpty(_name) == false) {
            $('#name-entry-form input[name="name"]').val(_name);
        }
        $('#name-popup').show(500);
        toast('Deatails Required', "Please enter the details to continue", 'info');
    }
    function hideNameEntry() {
        $('#name-popup').hide(500);
    }
    /********************************************************************************* */
    address_form_validator = $('#new-address-popup-form').validate({
        focusInvalid: false,
        ignore: [],
        rules: {
            "street": {
                required: true,
            },
            "building": {
                required: true,
            },
            "flat_no": {
                required: true,
            },
            "area_id": {
                required: true,
            }
        },
        messages: {
            "street": {
                required: "Enter street name",
            },
            "building": {
                required: "Enter building name",
            },
            "flat_no": {
                required: "Enter flat number",
            },
            "area_id": {
                required: "Select your area",
            }
        },
        errorPlacement: function (error, element) {
            if (element.attr("name") == "area_id") {
                error.insertAfter(element.parent());
            }
            else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            let address_id = $('input[name="address_id"]', form).val();
            let api_endpoint = "add_address";
            let submit_btn_text = "Save Address";
            if (address_id != "") {
                api_endpoint = "edit_address";
                submit_btn_text = "Update Address";
            }
            let submit_btn = $('[type="submit"]', form);
            submit_btn.html(loading_button_html).prop("disabled", true);
            $.ajax({
                type: 'POST',
                url: _base_url + "api/customer/" + api_endpoint,
                dataType: 'json',
                data: $('#new-address-popup-form').serialize(),
                success: function (response) {
                    submit_btn.html(submit_btn_text).prop("disabled", false);
                    if (response.result.status == "success") {
                        /********************************************************************* */
                        let next_action = $('input[name="next_action"]', form).val();
                        if (next_action == 'set_booking_address') {
                            // set address on booking form
                            $('input[name="address_id"]', booking_form).val(response.result.address.customer_address_id);
                        }
                        else if (next_action == 'pick-this'){
                            //alert();
                            //$('input[name="address_id"]', booking_form).val(response.result.address.customer_address_id);
                            //$('.booking-address-text').html(response.result.address.building + `, ` + response.result.address.flat_no + `, ` + response.result.address.area_name);
                        }
                        /********************************************************************* */
                        hideNewAddress();
                        addressListFetch();
                        toast('Saved', response.result.message, 'success');
                    }
                    else {
                    }
                },
                error: function (response) {
                    submit_btn.html(submit_btn_text).prop("disabled", false);
                },
            });
        }
    });
    $('#new-address-popup-form select[name="area_id"]').change(function () {
        $(this).valid();
    });
});
function addressListFetch() {
    $('#address-def-btn-container').hide();
    $('#address-list').html(loading_html);
    $.ajax({
        type: 'GET',
        url: _base_url + "api/customer/address_list",
        success: function (response) {
            if (response.result.status == "success") {
                _address_list = response.result.address_list;
                let pick_address = _address_list.find(address => {
                    return address.default_address == 1
                });
                if (pick_address) {
                    $('.booking-address-text').html(pick_address.building + `, ` + pick_address.flat_no + `, ` + pick_address.area_name);
                }
                /******************************************************************************* */

                /******************************************************************************* */
                renderAddressList(_address_list);
            }
            else {
            }
        },
        error: function (response) {
        },
    });
}
function hideNewAddress() {
    $('#address-form-popup').hide(500);
}
$('.user-btn').click(function () {
    $('.mobile-dropdown').toggle(500);
});
$('[data-action="frequency-home-entry"]').click(function () {
    $('#service_type_id').val(this.getAttribute('data-service_type_id'));
    Cookies.set('web_url_slug', this.getAttribute('data-web_url_slug')); // save url slug
    if (this.getAttribute('data-service_type_model_id') == 1) {
        $('#frequency-popup').show(500);
    }
    else {
        window.open(_base_url + this.getAttribute('data-web_url_slug'), "_self");
    }
});

function toast(title = null, text = null, icon = "info", settings = {}) {
    $.toast({
        heading: title,
        text: text,
        showHideTransition: 'slide',
        position: 'top-center',
        icon: icon,
        stack: 1,
        allowToastClose: false
    })
}
function ajaxError(title = 'Newtwork Error', response) {
    //toast(title, 'An error occured!');
}
function formValidationError(title, error) {
    toast(title, error.text());
}
/*************************************************************************** */
$('[data-action="new-address"]').click(function (e) {
    event_ = e;
    let address_id = $(this).attr('data-id');
    let next_action = $(this).attr('data-next-action');
    if (next_action){
        $('input[name="next_action"]', address_form_popup).val(next_action)
    }
    if (address_id > 0) {
        // show for edit purpose only
        showAddressForm(address_id, e);
    }
    else {
        showAddressForm();
    }
});
function showAddressForm(address_id, event) {
    address_form_validator.resetForm(); // remove errors
    $('#new-address-popup-form')[0].reset(); // reset fields
    if (address_id) {
        let address = _address_list.find(address => {
            return address.address_id == address_id
        });
        $('h4', address_form_popup).html('Edit Address');
        $('input[name="address_id"]', address_form_popup).val(address.address_id); // for edit purpose
        $('input[name="street"]', address_form_popup).val(address.street);
        $('input[name="building"]', address_form_popup).val(address.building);
        $('input[name="flat_no"]', address_form_popup).val(address.flat_no);
        $('select[name="area_id"]', address_form_popup).selectpicker('val', address.area_id);
        event.type = 'change';
        $('input[name="latitude"]', address_form_popup).val(address.lat).trigger(event);
        $('input[name="longitude"]', address_form_popup).val(address.long).trigger(event);
        $('[type="submit"]', address_form_popup).html('Update Address');
    }
    else {
        $('h4', address_form_popup).html('Add Address');
        $('input[name="address_id"]', address_form_popup).val(''); // for new purpose
        $('select[name="area_id"]', address_form_popup).selectpicker('val', '');
        $('[type="submit"]', address_form_popup).html('Save Address');
        //
        navigator.geolocation.getCurrentPosition(showPosition, showError);
    }
    $('#address-pick-popup').hide(500);
    $('#address-form-popup').show(500);
}
function hideAddressForm() {
    $('input[name="next_action"]', address_form_popup).val('');
    $('#address-form-popup').hide(500);
}
$('#address-form-popup [data-action="close"]').click(function () {
    hideAddressForm(500);
});
/*************************************************************************** */
/************************************************************* */
function scrollToElement(element) {
    if ($(element).is(":visible")) {
        $("html").animate(
            {
                scrollTop: element.offset().top - 100
            },
            800 //speed
        );
    }
    else {
        // some element not visible so use parent
        $("html").animate(
            {
                scrollTop: element.parent().offset().top - 100
            },
            800 //speed
        );
    }
}
/************************************************************* */

$(document).ready(function () {
    $('.mob-total-left').click(function () {
        $('.booking-summary-section').toggle(500);
        $('.booking-main-btn-section').addClass('active');
    });
    $('.mob-booking-title').click(function () {
        $('.booking-summary-section').hide(500);
        $('.booking-main-btn-section').removeClass('active');
    });
    $(".scroll").jScroll();
});
/************************************************************* */
// address add form based codes
function addressForm() {
    if ($('#latitude').val() == '' || $('#longitude').val() == '') {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition, showError);
        } else {
            x.innerHTML = "Geolocation is not supported by this browser.";
        }
    } else {
        locationPickr($('#latitude').val(), $('#longitude').val());
    }
}
function showPosition(position) {
    if (event_) {
        event_.type = 'change';
        $('input[name="latitude"]', address_form_popup).val(position.coords.latitude).trigger(event_);
        $('input[name="longitude"]', address_form_popup).val(position.coords.longitude).trigger(event_);
    }
    else {
        $('#latitude').val(position.coords.latitude);
        $('#longitude').val(position.coords.longitude);
    }
    locationPickr(position.coords.latitude, position.coords.longitude);
}
function showError(error) {
    switch (error.code) {
        case error.PERMISSION_DENIED:
            $('.us3').locationpicker({
                location: {
                    latitude: 25.055277,
                    longitude: 55.1586003
                },
                radius: .5,
                inputBinding: {
                    latitudeInput: $('#latitude'),
                    longitudeInput: $('#longitude'),
                    radiusInput: $('.us3-radius'),
                    locationNameInput: $('.us3-address')
                },
                enableAutocomplete: true,
                onchanged: function (currentLocation, radius, isMarkerDropped) {
                    // Uncomment line below to show alert on each Location Changed event
                    //alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
                }
            });
            break;
        case error.POSITION_UNAVAILABLE:
            console.log("Location information is unavailable.");
            break;
        case error.TIMEOUT:
            console.log("The request to get user location timed out.");
            break;
        case error.UNKNOWN_ERROR:
            console.log("An unknown error occurred.");
            break;
    }
}
function locationPickr(latitude, longitude) {
    $('.us3').locationpicker({
        location: {
            latitude: latitude,
            longitude: longitude
        },

        radius: .5,
        inputBinding: {
            latitudeInput: $('#latitude'),
            longitudeInput: $('#longitude'),
            radiusInput: $('.us3-radius'),
            locationNameInput: $('.us3-address')
        },
        //markerIcon: _base_url +'images/picker.png',
        enableAutocomplete: true,
        onchanged: function (currentLocation, radius, isMarkerDropped) {
            console.log(radius);
            // Uncomment line below to show alert on each Location Changed event
            //alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
        }
    });
}
/************************************************************* */
function mapCallback() { }

function nullOrEmpty(string) {
    if (string == null) {
        return true;
    }
    string.trim();
    if (string == "") {
        return true;
    }
    return false;
}
function fillOTPfromBoxes() {
    $('input[name="otp"]').val($('input[name="otp-1"]').val() + $('input[name="otp-2"]').val() + $('input[name="otp-3"]').val() + $('input[name="otp-4"]').val());
}
function isOTPBoxesFilled() {
    let filled = true;
    $('input[name*="otp-"]').each(function (index, obj) {
        if (this.value == "") {
            filled = false;
        }
        else {
        }
    });
    return filled;
}
var otp_interval = null;
function startOTPTimer() {
    var counter = _otp_expire_seconds;
    $('#resend-otp-timer').text('Resend OTP in ' + moment.utc(_otp_expire_seconds * 1000).format('mm:ss'));
    $('#resend-otp-timer').show();
    $('#resend-otp').hide();
    clearInterval(otp_interval);
    otp_interval = setInterval(function () {
        counter--;
        // Display 'counter' wherever you want to display it.
        if (counter <= 0) {
            clearInterval(otp_interval);
            $('#resend-otp-timer').hide();
            $('#resend-otp').show();
            return;
        } else {
            $('#resend-otp-timer').text('Resend OTP in ' + moment.utc(counter * 1000).format('mm:ss'));
        }
    }, 1000);
    $('input[name*="otp"]').val('');
    $('input[name="otp-1"]').focus();
}
$().ready(function () {
    $('input[name*="otp"]').keyup(function (e) {
        var key = e.keyCode || e.charCode;
        //e.preventDefault();
        let current_box = Number($(this).attr('data-box'));
        let input_length = $('input[name="otp-' + current_box + '"]').val().length;
        let current_box_value = $('input[name="otp-' + current_box + '"]').val();
        if (key == 8 || key == 46) {
            // deleting
            if (current_box_value == "") {
                $('input[name="otp-' + (current_box - 1) + '"]').focus().select();
            }
            else {
                $('input[name="otp-' + (current_box) + '"]').val('');
            }
        }
        else {
            if (input_length > 1) {
                $('input[name="otp-' + current_box + '"]').val(current_box_value.substr(current_box_value.length - 1));
                if (current_box <= 3) {
                    $('input[name="otp-' + (current_box + 1) + '"]').focus();
                }
            }
            else {
                if (current_box <= 3 && current_box_value != "") {
                    $('input[name="otp-' + (current_box + 1) + '"]').focus();
                }
            }
        }
        fillOTPfromBoxes();
        if (isOTPBoxesFilled() === true) {
            $('#login-otp-popup-form button[type="submit"]').focus();
            $('#login-otp-popup-form').submit();
        }
    });
    $('input[name*="otp"]').click(function () {
        this.focus(); //sets focus to element
        var val = this.value; //store the value of the element
        this.value = ''; //clear the value of the element
        this.value = val; //set that value back.
        this.select();
    });
});
function loader(show = true){
    if (show == true){
        $('#loader').show();
        $("body").addClass("overflow-hidden");
    }
    else{
        $('#loader').hide();
        $("body").removeClass("overflow-hidden");
    }
}