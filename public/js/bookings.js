$().ready(function () {
    let booking_cancel_form = $('#booking-cancel-form');
    let change_pay_mode_form = $('#change-pay-mode-popup-form');
    let cancel_booking_div;
    $('.js-cancel-booking').click(function () {
        booking_cancel_form_validator.resetForm(); // remove errors
        $('#booking-cancel-form')[0].reset(); // reset fields
        let booking = JSON.parse($(this).closest('.past-bookings-main').find("input[name='booking[]']").val());
        console.log(booking);
        $('[name="booking_id"]', booking_cancel_form).val(booking.booking_id);
        $('[name="date"]', booking_cancel_form).val(moment(booking.date, 'YYYY-MM-DD').format('DD/MM/YYYY'));
        $('#cancel-booking-ref').html(booking.booking_reference);
        $('#cancel-booking-date').html(moment(booking.date, 'YYYY-MM-DD').format('DD MMM, YYYY'));
        $('#cancel-booking-frequency').html(booking.frequency);
        cancel_booking_div = $(this).closest('.past-bookings-main');
        showCancelPopup();
    });
    booking_cancel_form_validator = $('#booking-cancel-form').validate({
        focusInvalid: true,
        ignore: [],
        rules: {
            "booking_id": {
                required: true,
            },
            "date": {
                required: true,
            },
            "cancel_reason": {
                required: true,
            }
        },
        messages: {
            "booking_id": {
                required: "Booking ID required",
            },
            "date": {
                required: "Date required",
            },
            "cancel_reason": {
                required: "Select cancel reason",
            }
        },
        errorPlacement: function (error, element) {
            error.insertAfter(element);
        },
        submitHandler: function (form) {
            let submit_btn = $('button[type="submit"]', form);
            submit_btn.html(loading_button_html).prop("disabled", true);
            $.ajax({
                type: 'POST',
                url: _base_url + "api/customer/booking_cancel",
                dataType: 'json',
                data: booking_cancel_form.serialize(),
                success: function (response) {
                    submit_btn.html('Yes').prop("disabled", false);
                    if (response.result.status == "success") {
                        cancel_booking_div.remove();
                        hideCancelPopup();
                        toast('Cancelled', response.result.message, 'success');
                    } else {
                        toast('Failed', response.result.message, 'error');
                    }
                },
                error: function (response) {
                    submit_btn.html('Yes').prop("disabled", false);
                },
            });
        }
    });
    reschedule_form_validator = $('#reschedule-popup-form').validate({
        focusInvalid: true,
        ignore: [],
        rules: {
            "date": {
                required: true,
            },
            "time_from": {
                required: true,
            }
        },
        messages: {
            "date": {
                required: "Select reschedule date",
            },
            "time_from": {
                required: "Select reschedule time",
            }
        },
        errorPlacement: function (error, element) {
            if (element.attr("name") == "date") {
                error.insertAfter($('#calendar').append());
            }
            else if (element.attr("name") == "time_from") {
                error.insertAfter($('#times-holder').append());
            }
            else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            let submit_btn = $('button[type="submit"]', reschedule_form);
            submit_btn.html(loading_button_html).prop("disabled", true);
            $.ajax({
                type: 'POST',
                url: _base_url + "api/customer/reschedule",
                dataType: 'json',
                data: reschedule_form.serialize(),
                success: function (response) {
                    submit_btn.html('Reschedule').prop("disabled", false);
                    if (response.result.status == "success") {
                        hideReschedulePopup();
                        Swal.fire({
                            title: "Rescheduled !",
                            html: response.result.message,
                            icon: "success",
                            allowOutsideClick: false,
                            confirmButtonText: 'OK',
                        }).then((result) => {
                            if (result.isConfirmed) {
                                location.reload();
                            }
                        });
                    } else {
                        toast('Failed', response.result.message, 'error');
                    }
                },
                error: function (response) {
                    submit_btn.html('Reschedule').prop("disabled", false);
                },
            });
        }
    });
    $('#reschedule-popup [data-action="close"]').click(function () {
        location.reload(); // ui issue with owl corousel
        hideReschedulePopup();
    });
    change_pay_mode_validator = $('#change-pay-mode-popup-form').validate({
        focusInvalid: true,
        ignore: [],
        rules: {
            "payment_method": {
                required: true,
            },
        },
        messages: {
            "payment_method": {
                required: "Select payment mode",
            },
        },
        errorPlacement: function (error, element) {
            if (element.attr("name") == "payment_method") {
                error.insertAfter($('#payment-method-holder').append());
            }
            else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            let payment_method = $('input[name="payment_method"]:checked', change_pay_mode_form).val();
            if (payment_method == 2) {

            }
            change_pay_mode_form = $('#change-pay-mode-popup-form');
            let submit_btn = $('button[type="submit"]', change_pay_mode_form);
            submit_btn.html(loading_button_html).prop("disabled", true);
            $.ajax({
                type: 'POST',
                url: _base_url + "api/customer/change_payment_mode",
                dataType: 'json',
                data: {
                    booking_id: $('input[name="booking_id"]', change_pay_mode_form).val(),
                    PaymentMethod: $('input[name="payment_method"]:checked', change_pay_mode_form).val(),
                },
                success: function (response) {
                    console.log(response);
                    if (response.result.status == "success") {
                        if (payment_method == 1) {
                            // cash
                            hidePayModePopup();
                            Swal.fire({
                                title: "Payment Mode Changed !",
                                html: response.result.message,
                                icon: "success",
                                allowOutsideClick: false,
                                confirmButtonText: 'OK',
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    location.reload();
                                }
                            });
                        }
                        else if (payment_method == 2) {
                            // card
                            /******************* open url with post data from api ************** */
                            var form_fields_html = ``;
                            $.each(response.result.checkout_data.payfort_data, function (field, value) {
                                form_fields_html += `<input type="hidden" name="` + field + `" value="` + value + `" />`;
                            });
                            let form = $('<form action="' + response.result.checkout_url + '" method="post">' +
                                form_fields_html +
                                '</form>');
                            $('body').append(form);
                            form.submit();
                        }
                    } else {
                        submit_btn.html(resetPayBtnName(payment_method)).prop("disabled", false);
                        //toast('Failed', response.result.message, 'error');
                        hidePayModePopup();
                        Swal.fire({
                            title: "Failed !",
                            html: response.result.message,
                            icon: "error",
                            allowOutsideClick: false,
                            confirmButtonText: 'OK',
                            timer: 3000,
                        }).then((result) => {
                            if (result.isConfirmed || result.dismiss === swal.DismissReason.timer) {
                                location.reload();
                            }
                        });
                    }
                },
                error: function (response) {
                    submit_btn.html('Complete').prop("disabled", false);
                },
            });
        }
    })
    $('[data-action="change-pay-mode"]').click(function () {
        let booking = JSON.parse($(this).closest('.past-bookings-main').find("input[name='booking[]']").val());
        showPayModePopup(booking);
    });
    $('#change-pay-mode-popup [data-action="close"]').click(function () {
        hidePayModePopup();
    });
});
function hidePayModePopup() {
    $('#change-pay-mode-popup').hide(500);
}
function showPayModePopup(booking) {
    console.log(booking);
    change_pay_mode_form = $('#change-pay-mode-popup-form');
    let payment_type_id = booking.payment_type_id; // current mode
    $('input[name="booking_id"]', change_pay_mode_form).val(booking.booking_id);
    $("li[id*='payment-mode-']", change_pay_mode_form).show(); // show all
    $('input[name="payment_method"]', change_pay_mode_form).prop("checked", false).trigger('change'); // unselect
    $('#payment-mode-' + payment_type_id, change_pay_mode_form).hide(); // hide current mode
    change_pay_mode_validator.resetForm();
    $('#change-pay-mode-popup').show(500);
}
$('#booking-cancel-form [data-action="close"]').click(function () {
    hideCancelPopup();
});
function showCancelPopup() {
    $('#booking-cancel-confirm').show(500);
}
function hideCancelPopup() {
    $('#booking-cancel-form')[0].reset(); // reset fields
    $('#booking-cancel-confirm').hide(500);
}

available_datetime_req = null;
function available_datetimeRender() {
    reschedule_form = $('#reschedule-popup-form');
    $('#times-holder', reschedule_form).html(loading_html);
    available_datetime_req = $.ajax({
        type: 'GET',
        url: _base_url + "api/customer/available_datetime",
        dataType: 'json',
        beforeSend: function () {
            if (available_datetime_req != null) {
                available_datetime_req.abort();
            }
        },
        success: function (response) {
            var dates_html = ``;
            $.each(response.result.available_dates, function (index, date) {
                dates_html += `<div class="item">
                    <input id="date-`+ index + `" value="` + moment(date, 'YYYY-MM-DD').format('DD/MM/YYYY') + `" name="date" class="" type="radio">
                    <label for="date-`+ index + `">
                        <div class="calendar-tmb-main">
                            <div class="calendar-tmb-day">`+ moment(date, 'YYYY-MM-DD').format('ddd') + `</div>
                            <div class="calendar-tmb-date">`+ moment(date, 'YYYY-MM-DD').format('DD') + `</div>
                            <div class="calendar-tmb-month">`+ moment(date, 'YYYY-MM-DD').format('MMM') + `</div>
                        </div>
                    </label>
                </div>`;
            });
            $('#calendar', reschedule_form).html(dates_html);
            $('input[name="date"]', reschedule_form).change(function () {
                available_timeRender();
            });
            $('#calendar.owl-carousel').trigger('destroy.owl.carousel'); //these 3 lines kill the owl, and returns the markup to the initial state
            $('#calendar.owl-carousel').find('.owl-stage-outer').children().unwrap();
            $('#calendar.owl-carousel').removeClass("owl-center owl-loaded owl-text-select-on");
            $("#calendar.owl-carousel").owlCarousel({
                nav: true,
                loop: false,
                dots: false,
                margin: 15,
                autoplay: false,
                smartSpeed: 100,
                responsive: {
                    0: {
                        items: 5
                    },
                    600: {
                        items: 5
                    },
                    1000: {
                        items: 5
                    },
                    1100: {
                        items: 5
                    },
                    1200: {
                        items: 5,
                        //margin: 50
                    }
                }
            }); //re-initialise the owl
            $('input[name="date"]:first', reschedule_form).trigger('click');
            renderTimes(response.result.available_times);
        },
        error: function (response) {
        },
    });
}
function renderTimes(available_times) {
    reschedule_form = $('#reschedule-popup-form');
    var times_html = ``;
    $.each(available_times, function (index, time) {
        times_html += `<li>
                    <input id="time-`+ index + `" value="` + time + `" name="time_from" class="" type="radio">
                    <label for="time-`+ index + `">
                        <!--<p>AED 5 Extra</p>-->`+ moment(time, "HH:mmm").format("hh:mm A") + `
                    </label>
                </li>`;
    });
    if (times_html == '') {
        times_html = `<div class="alert alert-info" role="alert">
                            <i class="fa fa-exclamation-circle" aria-hidden="true"></i> No time slots available for the selected date!
                    </div>`;
    }
    $('#times-holder', reschedule_form).html(times_html);
    $('input[name="time"]', reschedule_form).change(function () {
    });
}
available_time_req = null;
function available_timeRender() {
    reschedule_form = $('#reschedule-popup-form');
    $('#times-holder', reschedule_form).html(loading_html);
    available_time_req = $.ajax({
        type: 'GET',
        url: _base_url + "api/customer/available_time",
        data: {
            date: $('input[name="date"]:checked', reschedule_form).val()
        },
        dataType: 'json',
        beforeSend: function () {
            if (available_time_req != null) {
                available_time_req.abort();
            }
        },
        success: function (response) {
            renderTimes(response.result.available_times);
        },
        error: function (response) {
        },
    });
}
function showReschedulePopup(booking) {
    reschedule_form = $('#reschedule-popup-form');
    console.log(booking);
    $('.booking_id', reschedule_form).html(booking.booking_reference);
    $('input[name="booking_id"]', reschedule_form).val(booking.booking_id);
    available_datetimeRender();
    $('#reschedule-popup').show(500);
}
function hideReschedulePopup() {
    $('input[name="booking_id"]', reschedule_form).val(''); // reset
    $('#reschedule-popup').hide(500);
}
$('[data-action="reshedule"]').click(function () {
    let booking = JSON.parse($(this).closest('.past-bookings-main').find("input[name='booking[]']").val());
    showReschedulePopup(booking);
});

$('[data-action="retry-payment"]').click(function () {
    let booking = JSON.parse($(this).closest('.past-bookings-main').find("input[name='booking[]']").val());
    if (booking.payment_type_id == 2) {
        var form_fields_html = ``;
        $.each(booking.checkout_data.payfort_data, function (field, value) {
            form_fields_html += `<input type="hidden" name="` + field + `" value="` + value + `" />`;
        });
        let form = $('<form action="' + booking.checkout_url + '" method="post">' +
            form_fields_html +
            '</form>');
        $('body').append(form);
        form.submit();
    }
});