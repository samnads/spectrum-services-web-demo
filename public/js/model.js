$().ready(function () {
    addressForm();
    $('[data-action="address-pick-popup"]').click(function () {
        $('#address-pick-holder').html(loading_html);
        showAddressPick();
        $.ajax({
            type: 'GET',
            url: _base_url + "api/customer/address_list",
            cache: true,
            success: function (response) {
                if (response.result.status == "success") {
                    _address_list = response.result.address_list;
                    renderAddressOnPickList(_address_list);
                }
                else {
                }
            },
            error: function (response) {
            },
        });
    });
    $('#address-pick-popup [data-action="close"]').click(function () {
        hideAddressPick();
    });
    coupon_apply_form = $('#coupon-apply-form');
    coupon_form_validator = $('#coupon-apply-form').validate({
        focusInvalid: false,
        ignore: [],
        rules: {
            "coupon_code": {
                required: true,
            },
        },
        messages: {
            "coupon_code": {
                required: "Enter coupon code",
            },
        },
        errorPlacement: function (error, element) {
            error.insertAfter(element);
        },
        submitHandler: function (form) {
            let submit_btn = $('button[type="submit"]', form);
            submit_btn.html(loading_button_html).prop("disabled", true);
            _cart.coupon_code = $('input[name="coupon_code"]', form).val();
            calculate();
        }
    });
    if (_id == null) {
    }
    else {
        showDefaultAddressOnStep();
    }
    /*********************************************************************************************/
    // apple pay avilability check
    var merchantIdentifier = "merchant.com.Azinova.Spectrum.userApp";
    if (window.ApplePaySession) {
        var promise = ApplePaySession.canMakePaymentsWithActiveCard(merchantIdentifier);
        promise.then(function (canMakePayments) {
            if (canMakePayments) {
                // Display Apple Pay button here.
                logit('hi, I can do ApplePay');
            }
        });
    } else {
        logit('ApplePay is not available on this browser');
        $('.pay-mode-li-3').hide(); // hide apple pay
    }
    /*********************************************************************************************/
});
function showDefaultAddressOnStep() {
    if (_address_list.length == 0) {
        $.ajax({
            type: 'GET',
            url: _base_url + "api/customer/address_list",
            cache: true,
            success: function (response) {
                if (response.result.status == "success") {
                    _address_list = response.result.address_list;
                    if (_address_list.length > 0){
                        let pick_address = _address_list.find(address => {
                            return address.default_address == 1
                        });
                        $('.booking-address-text').html(pick_address.building + `, ` + pick_address.flat_no + `, ` + pick_address.area_name);
                        $('#booking-form input[name="address_id"]').val(pick_address.address_id);
                    }
                }
                else {
                }
            },
            error: function (response) {
            },
        });
    }
    else {
        let pick_address = _address_list.find(address => {
            return address.default_address == 1
        });
        $('.booking-address-text').html(pick_address.building + `, ` + pick_address.flat_no + `, ` + pick_address.area_name);
    }
}
function renderAddressOnPickList(address_list) {
    address_html = ``;
    let addresses_list_html = ``;
    let address_id = $('input[name="address_id"', booking_form).val();
    if (address_list.length == 0) {
        $('#address-pick-holder').html(`<div class="alert alert-info" role="alert">
        No address found, please add atleast one address to start booking.
        </div>`);
    }
    else {
        $.each(address_list, function (index, address) {
            address_html += `<div class="col-sm-12 manage-address-section">
                                        <div class="d-flex manage-address-main">
                                            <div class="manage-address-content flex-grow-1">
                                            <input value='` + JSON.stringify(address) + `' name="pick_address[]" type="hidden">
                                                    <input id="address-`+ index + `" value="` + address.address_id + `" name="pick_address_id" class="" type="radio" ` + (address.address_id == address_id ? 'checked' : '') + `>
                                                <label for="address-`+ index + `">
                                                    <span></span>
                                                    <p>` + address.building + `</p>
                                                    ` + address.flat_no + `, ` + address.building + `, ` + address.street + `
                                                </label>
                                            </div>
                                        </div>
                                    </div>`;
        });
        $('#address-pick-holder').html(address_html);
    }
    $('#address-pick-holder input[name="pick_address_id"]').click(function () {
        $('input[name="address_id"', booking_form).val(this.value);
        let pick_address = JSON.parse($(this).closest('.manage-address-content').find("input[name='pick_address[]']").val());
        console.log(pick_address);
        $('.booking-address-text').html(pick_address.building + `, ` + pick_address.flat_no + `, ` + pick_address.area_name);
        toast('Changed', 'Booking address changed.', 'info');
        hideAddressPick();
    });
}
function showAddressPick() {
    $('#address-pick-popup').show(500);
}
function hideAddressPick() {
    $('#address-pick-popup').hide(500);
}
/************************************************************************************************* */
$('[data-action="coupon-apply-popup"]').click(function () {
    showCouponPopup();
});
$('#coupon-apply-popup [data-action="close"]').click(function () {
    hideCouponPopup();
});
$('[data-action="remove-coupon"]').click(function () {
    _cart.coupon_code = null;
    $('#applied-coupon-widget').hide(500);
    $('#add-coupon-widget').show(500);
    toast('Coupon removed', "Applied coupon removed!", 'info');
    calculate();
});
function showCouponPopup() {
    coupon_apply_form.trigger("reset");
    coupon_form_validator.resetForm();
    $('#coupon-apply-popup').show(500);
}
function hideCouponPopup() {
    $('#coupon-apply-popup').hide(500);
}
/************************************************************************************************* */