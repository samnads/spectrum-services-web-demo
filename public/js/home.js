$(document).ready(function () {
    var owl = $("#banner");
    owl.owlCarousel({
        items: 1,
        loop: true,
        margin: 0,
        autoplay: true,
        autoplayTimeout: 4000,
        autoplayHoverPause: true
    });

    var owl = $("#category");
    owl.owlCarousel({
        nav: false,
        loop: false,
        dots: true,
        margin: 15,
        autoplay: true,
        autoplayTimeout: 2000,
        smartSpeed: 800,
        autoplayHoverPause: true,
        responsive: {
            0: {
                items: 2
            },
            600: {
                items: 3
            },
            1000: {
                items: 5
            },
            1100: {
                items: 5
            },
            1200: {
                items: 6,
                margin: 30
            }
        }
    });

    var owl = $("#special-offers");
    owl.owlCarousel({
        nav: true,
        loop: true,
        dots: false,
        margin: 15,
        autoplay: true,
        autoplayTimeout: 2000,
        smartSpeed: 800,
        autoplayHoverPause: true,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 2
            },
            1000: {
                items: 2
            },
            1100: {
                items: 2
            },
            1200: {
                items: 2,
                margin: 50
            }
        }
    });

    $(document).on('click', '#special-offers .item', function () {
        Cookies.set('time', null); // rest time
        window.open(_base_url + 'special-offer/' + $(this).attr('data-id'), "_self")
    });

    var owl = $("#add-ons");
    owl.owlCarousel({
        nav: false,
        loop: false,
        dots: true,
        margin: 15,
        autoplay: true,
        autoplayTimeout: 2000,
        smartSpeed: 800,
        autoplayHoverPause: true,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 2
            },
            1000: {
                items: 3
            },
            1100: {
                items: 3
            },
            1200: {
                items: 3,
                //margin: 50
            }
        }
    });
});