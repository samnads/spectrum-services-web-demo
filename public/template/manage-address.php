	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Spectrum Booking</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="theme-color" content="#0c3995">
<link rel="icon" type="image/png" href="images/favicon.png"/>

<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" >
<link rel="stylesheet" type="text/css" href="css/bootstrap-select.css"/>

<link rel="stylesheet" type="text/css" href="css/style.css"/>
<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css"/>
<link rel="stylesheet" type="text/css" href="css/animation.css"/>
<link rel="stylesheet" type="text/css" href="css/owl.carousel.min.css">

</head>

<body>

<div class="wrapper-main">
	
    
    <?php require_once('include/header.php') ?>
	
    
    <section>
		<div class="container">
			<div class="row inner-wrapper m-0">
                 <div class="col-sm-12 my-account-wrapper">


					  
					  <div class="col-sm-12">
					       <div class="d-flex">
							  <div class="step-back-icon"><a href="my-account.php" class="back-arrow" title="Click to Back"></a></div>
							  <div class="booking-page-title flex-grow-1"><h3>Manage Address</h3></div>
							  <div class="add-new-address">
							       <div class="addon-btn-main">
										<a class="sp-btn show-add-address-popup">Add New</a>
								   </div>
							  </div>
							</div>
					  </div>
					  
					  
					  <div class="row m-0 pt-3">
                           <div class="col-sm-12 manage-address-section">
								<div class="d-flex manage-address-main">
									 <div class="manage-address-content flex-grow-1">
									      <input id="manage-address1" value="cash" name="manage-address" class="" type="radio">
                                          <label for="manage-address1">
										  <span></span>
										  <p>Home</p>
										     Test building, Unit No - 11, Street
										  </label>
									 </div>
									 <div class="manage-address-edit v-center">
									      <div class="manage-address-edit-icons">
										  	   <label class="delete-action"><i class="fa fa-trash"></i></label>
										       <label class="show-add-address-popup"><i class="fa fa-pencil"></i></label>
										  </div>
									 </div>
								</div>
						   </div>
						   
						   
						   <div class="col-sm-12 manage-address-section">
								<div class="d-flex manage-address-main">
									 <div class="manage-address-content flex-grow-1">
									      <input id="manage-address2" value="cash" name="manage-address" class="" checked="checked" type="radio">
                                          <label for="manage-address2">
										  <span></span>
										  <p>Office</p>
										     Test building, Unit No - 11, Street - test street,The Springs
										  </label>
									 </div>
									 <div class="manage-address-edit v-center">
									      <div class="manage-address-edit-icons">
										  	   <label class="delete-action"><i class="fa fa-trash"></i></label>
										       <label class="show-add-address-popup"><i class="fa fa-pencil"></i></label>
										  </div>
									 </div>
								</div>
						   </div>
						   
						   
						   <div class="col-sm-12 manage-address-section">
								<div class="d-flex manage-address-main">
									 <div class="manage-address-content flex-grow-1">
									      <input id="manage-address3" value="cash" name="manage-address" class="" type="radio">
                                          <label for="manage-address3">
										  <span></span>
										  <p>Villa</p>
										     Test building, Test street,The Springs
										  </label>
									 </div>
									 <div class="manage-address-edit v-center">
									      <div class="manage-address-edit-icons">
										  	   <label class="delete-action"><i class="fa fa-trash"></i></label>
										       <label class="show-add-address-popup"><i class="fa fa-pencil"></i></label>
										  </div>
									 </div>
								</div>
						   </div>
						   
						   
						   
						   
						   
						   
							<div class="col-sm-12 booking-main-btn-section p-0 pt-4">
								 <div class="row m-0">
									   <div class="col-lg-3 col-md-4 col-sm-6 p-0 pt-3">
											<input value="Set Default Address" class="text-field-btn" type="submit">
									   </div>
								 </div>
							</div>
						   
					  </div>
				 </div>
			</div>
		</div>  
    </section>

</div>

<?php require_once('include/footer.php') ?>
          
</body>
</html>
