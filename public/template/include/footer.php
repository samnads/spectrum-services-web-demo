<footer class="footer-wrapper">
	   <div class="container">
	   <div class="row footer-bottom m-0">
          <div class="col-sm-6 p-0 footer-copy">
            <p>2023 © <span>Spectrum Booking .</span> All rights reserved.</p>
          </div>
          <div class="col-sm-6 p-0 footer-design">
            <div class="designed"> <a href="http://azinovatechnologies.com/" target="_blank">
              <div class="azinova-logo"></div>
              </a>
              <p class="no-padding">Powered by :</p>
              <div class="clear"></div>
            </div>
          </div>
        </div>
		</div>
	</footer>
	
	
	
<div class="popup-main frequency-popup">
     <div class="row min-vh-100 m-0">
          <div class="mx-auto my-auto shadow popup-main-cont">
               
               <div class="popup-close"><img src="images/el-close-white.png" alt=""></div>
			   
                    <div class="col-sm-12 popup-head-text"><h4>Choose Your Frequency</h4></div>
			   
			   
			   
			   <div class="row m-0">
			   
			        <div class="col-sm-12 frequency-main one-time">
					     <input id="frequency1" value="cash" name="how-often" class="" type="radio">
                         <label for="frequency1"> <span></span> &nbsp; One Time <p>This service, will notrepeat again.</p></label>
						 
						 <!--<div class="frequency-offer">5%<span>OFF</span></div>-->
					</div>
					
					<div class="col-sm-12 frequency-main weekly">
					     <input id="frequency2" value="card" name="how-often" class="" type="radio">
                         <label for="frequency2"> <span></span> &nbsp; Weekly <p>This service for the same ay every week</p></label>
						 
						 <div class="frequency-offer">10%<span>OFF</span></div>
					</div>
					
					
					<div class="col-sm-12 frequency-main every-2week">
					     <input id="frequency3" value="card" name="how-often" class="" type="radio">
                         <label for="frequency3"> <span></span> &nbsp; Every 2 Week <p>This service for every two weeks.</p></label>
						 
						 <div class="frequency-offer">15%<span>OFF</span></div>
					</div>
					
                    <div class="col-sm-6 frequency-main pt-3">
					     <a href="date-and-time.php">
					     <input value="Select Frequency" class="text-field-btn" type="submit">
						 </a>
					</div>                             
												 
										   
			   </div>
			   
			   
          </div>
     </div>
</div><!-- Frequency Popup-->





<div class="popup-main login-popup">
     <div class="row min-vh-100 m-0">
          <div class=" mx-auto my-auto shadow popup-main-cont">
               
               <div class="popup-close"><img src="images/el-close-white.png" alt=""></div>
			   
                    <div class="col-sm-12 popup-head-text"><h4>Login /Sign up</h4></div>
					
			   
			   
			   
			   <div class="row m-0">
			   
			        <div class="col-sm-12 login-content p-0">
			             <p>We are glad to have you here. please login or Signup to complete your  boking.</p>
					</div>
					
					<div class="col-sm-12 login-field">
					     <input name="" class="text-field" type="number">
						 <div class="country-code">+971</div>
					</div>
					
					
			        
                    <div class="col-sm-12 frequency-main pt-3">
					     <input value="Continue" class="text-field-btn SHOW-OTP-BTN" type="submit"><!--Remove SHOW-OTP-BTN class from button field while programming. its using for show the demo flow-->
					</div>                             				   
			   </div>
			   
			   
          </div>
     </div>
</div><!-- Login Popup-->
	
	
	


<div class="popup-main otp-popup">
     <div class="row min-vh-100 m-0">
          <div class=" mx-auto my-auto shadow popup-main-cont">
               
               <div class="popup-close"><img src="images/el-close-white.png" alt=""></div>
			   
                    <div class="col-sm-12 popup-head-text"><h4>Verify phone number</h4></div>
					
			   
			   
			   
			   <div class="row m-0">
			   
			        <div class="col-sm-12 otp-content">
			             <p><strong>Enter the code that was sent to</strong></p>
						 <h5>+971 50 123 4556</h5>
					</div>
					
					<div class="col-sm-12 otp-field p-0">
						 <input type="number" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==4) return false;"  class="text-field" />
					</div>
					
					
					<div class="col-sm-12 otp-field p-0">
					     <p><strong>Didn't receive a code?</strong></p>
						 <a href="#" class="pull-left">Resend SMS</a>         <a href="#" class="pull-right">Send code via WhatsApp</a>
					</div>
					
					
			        
                    <div class="col-sm-12 frequency-main pt-3">
					     <input value="Continue" class="text-field-btn" type="submit">
					</div>                             				   
			   </div>
			   
			   
          </div>
     </div>
</div><!-- OTP Popup-->







<div class="popup-main add-address-popup">
     <div class="row min-vh-100 m-0">
          <div class=" mx-auto my-auto shadow popup-main-cont">
               
               <div class="popup-close"><img src="images/el-close-white.png" alt=""></div>
			   
                    <div class="col-sm-12 popup-head-text"><h4>Add Address</h4></div>
			   
					   <div class="row m-0">
					   
					        <div class="col-md-6 address-google-map">
							    <div class="map-search"><input name="" placeholder="Address" class="text-field" type="text"></div>
								
								<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3613.4846948105287!2d55.154268710472095!3d25.08544900023128!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e5f134e609581e1%3A0x5312bbe8506b0c9c!2sEmirates%20Golf%20Club-%20Majilis%20Course.!5e0!3m2!1sen!2sin!4v1590989224759!5m2!1sen!2sin" allowfullscreen="" aria-hidden="false" tabindex="0" width="100%" frameborder="0"></iframe>
							</div>
							
							
							<div class="col-md-6 address-details">
							     <div class="row m-0">
								 
								      <div class="col-sm-12 address-options p-0 pb-4">
									       <ul>
												 <li class="home">
													 <input id="address1" value="address" name="address-opt" class="" checked="" type="radio">
													 <label for="address1">Home</label>
												 </li>
												 
												 
												 <li class="office">
													 <input id="address2" value="address" name="address-opt" class="" type="radio">
													 <label for="address2">Office</label>
												 </li>
												 
												 
												 <li class="other">
													 <input id="address3" value="address" name="address-opt" class="" type="radio">
													 <label for="address3">Other</label>
												 </li>
										   </ul>
									  </div>
									  
								      
									  <div class="col-sm-12 text-field-main">
											 <input name="" placeholder="Street" class="text-field" type="text">
									  </div>
									  
									  
									  <div class="col-sm-12 p-0">
										   <div class="row text-field-main">
										        <div class="col-sm-8">
											         <input name="" placeholder="Building" class="text-field" type="text">
										        </div>
												
												<div class="col-sm-4">
											         <input name="" placeholder="Flat No:" class="text-field" type="text">
										        </div>
										   </div>
									  </div>
									  
									  
									  <div class="col-sm-12 text-field-main">
										   <select class="selectpicker form-control" id="" data-container="body" data-live-search="true" data-hide-disabled="true">
												<option value="0">One</option>
												<option value="1">Two</option>
												<option value="2">Three</option>
												<option value="3">Four</option>
												<option value="4">Five</option>
												<option value="5">Six</option>
												<option value="6">Seven</option>
												<option value="7">Eight</option>
												<option value="8">Nine</option>
												<option value="9">Ten</option>
										</select>
									  </div>
									  
									  
									  <div class="col-sm-12 text-field-main">
										   <input value="Add Address" class="text-field-btn" type="submit">
									  </div>
									  
								 </div>
							</div>
					   
							

                          				   
					   </div>

          </div>
     </div>
</div><!-- Address Popup-->







<div class="popup-main promocode-popup">
     <div class="row min-vh-100 m-0">
          <div class=" mx-auto my-auto shadow popup-main-cont">
               
               <div class="popup-close"><img src="images/el-close-white.png" alt=""></div>
			   
                    <div class="col-sm-12 popup-head-text"><h4>Promo Codes</h4></div>
			   
					   <div class="row m-0">
					   
					        <div class="col-sm-12 promo-code-field">
							     <input name="" placeholder="Enter Promo Code" class="text-field" type="text">
							</div>

							
							<div class="col-sm-12 p-0">
								 <h5>Available Codes</h5>
							</div>
							
					   <div class="col-sm-12 promocode-cont-set">
						   
							
							<div class="d-flex promocode-cont-main">
							  <div class="promocode-cont flex-grow-1">
							       <p><strong>SP5368</strong><br>
								   <span>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</span></p>
							  </div>
							  <div class="promocode-cont-btn v-center">
							       <input value="Apply" class="text-field-btn" type="submit">
							  </div>
							</div>
							
							
							<div class="d-flex promocode-cont-main">
							  <div class="promocode-cont flex-grow-1">
							       <p><strong>SP5368</strong><br>
								   <span>Lorem Ipsum is simply dummy text of the printing Lorem Ipsum is simply dummy text of the printing and typesetting industry.</span></p>
							  </div>
							  <div class="promocode-cont-btn v-center">
							       <input value="Apply" class="text-field-btn" type="submit">
							  </div>
							</div>
							
							
							<div class="d-flex promocode-cont-main">
							  <div class="promocode-cont flex-grow-1">
							       <p><strong>SP5368</strong><br>
								   <span>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</span></p>
							  </div>
							  <div class="promocode-cont-btn v-center">
							       <input value="Apply" class="text-field-btn" type="submit">
							  </div>
							</div>
							
					  </div>
							
							
							
							<div class="col-sm-6 frequency-main pt-3">
								 <input value="Confirm" class="text-field-btn" type="submit">
							</div>                             				   
					   </div>

          </div>
     </div>
</div><!-- Promo Code Popup-->






<div class="popup-main reset-password-popup">
     <div class="row min-vh-100 m-0">
          <div class=" mx-auto my-auto shadow popup-main-cont">
               <div class="popup-close"><img src="images/el-close-white.png" alt=""></div>
                    <div class="col-sm-12 popup-head-text"><h4>Reset Password</h4></div>
					
			   
			   <div class="row m-0 pt-1">
			   

					<div class="col-sm-12 reset-password-field-main p-0">
					     <p>Old Password</p>
						 <div class="reset-password-field">
						      <input type="password"  class="text-field" />
						      <div class="password-show-btn fa fa-fw fa-eye-slash"></div>
						 </div>
					</div>
					
					<div class="col-sm-12 reset-password-field-main p-0">
					     <p>New Password</p>
						 <div class="reset-password-field">
						      <input type="password"  class="text-field" />
						      <div class="password-show-btn fa fa-fw fa-eye-slash"></div>
						 </div>
					</div>
					
					<div class="col-sm-12 reset-password-field-main p-0">
					     <p>Confirm Password</p>
						 <div class="reset-password-field">
						      <input type="password"  class="text-field" />
						      <div class="password-show-btn fa fa-fw fa-eye-slash"></div>
						 </div>
					</div>
			        
                    <div class="col-sm-12 frequency-main pt-3">
					     <input value="Reset" class="text-field-btn" type="submit">
					</div>                             				   
			   </div>
			   
			   
          </div>
     </div>
</div><!-- Reset Password Popup-->





<div class="popup-main cancel-booking-popup">
     <div class="row min-vh-100 m-0">
          <div class=" mx-auto my-auto shadow popup-main-cont">
               <div class="popup-close"><img src="images/el-close-white.png" alt=""></div>
                    <div class="col-sm-12 popup-head-text"><h4>Cancel Booking</h4></div>
					
			   
			   <div class="row m-0 pt-1">
			   
                    <div class="col-sm-12 p-0 mb-3">
					     <p class="text-red">Are you sure want to cancel the boking?</p>
						 <h5><strong>16 Oct, 2023 5:04 pm</strong> ( One Day Booking)</h5>
					</div>
					
					<div class="col-sm-12 maids-section clearfix p-0 pt-1">
							   <div class="pb-maids"><img src="images/maid.jpg" alt=""></div>
							   <div class="pb-maids"><img src="images/maid.jpg" alt=""></div>
							   <div class="pb-maids"><img src="images/maid.jpg" alt=""></div>
						  </div>
			        
					<div class="col-sm-12 p-0 pt-3">
                    <div class="row pt-3">
					     <div class="col-sm-6 mb-2"><a class="close-popup"><input value="Keep Booking" class="text-field-btn" type="submit"></a></div>
						 <div class="col-sm-6"><a href="cancelled-booking.php"><input value="Cancel Booking" class="text-field-btn" type="submit"></a></div>
					</div> 
					</div>                            				   
			   </div>
			   
			   
          </div>
     </div>
</div><!-- Cancel Booking Popup-->




<div class="popup-main maid-popup">
     <div class="row min-vh-100 m-0">
          <div class=" mx-auto my-auto shadow popup-main-cont">
               <div class="popup-close"><img src="images/el-close-white.png" alt=""></div>
			   <div class="col-sm-12 popup-head-text"><h4>Professionals Details</h4></div>
			   <div class="row m-0">
			        <div class="col-sm-6 which-housekeeper-thumb p-0 pe-4">
					     <div class="which-housekeeper-thumb-image"><img src="images/maid.jpg" alt=""></div>
					     <div class="which-housekeeper-thumb-name v-center">Elizabeth Delyn</div>
					     <div class="which-housekeeper-thumb-rating"><img src="images/5star.png" alt=""></div>
					</div>
					
					<div class="col-sm-6 housekeeper-thumb-rating p-0">
					     <div class="col-sm-12 housekeeper-rating-cont p-0">
						      <p>Excellent</p>
							  <div class="progress excellent">
                                   <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:50%"></div>
                              </div>
						 </div>
						 
						 <div class="col-sm-12 housekeeper-rating-cont p-0">
						      <p>Good</p>
							  <div class="progress good">
                                   <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:70%"></div>
                              </div>
						 </div>
						 
						 <div class="col-sm-12 housekeeper-rating-cont p-0">
						      <p>Average</p>
							  <div class="progress average">
                                   <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:80%"></div>
                              </div>
						 </div>
						 
						 <div class="col-sm-12 housekeeper-rating-cont p-0">
						      <p>Bad</p>
							  <div class="progress bad">
                                   <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:60%"></div>
                              </div>
						 </div>
						 
						 <div class="col-sm-12 housekeeper-rating-cont p-0">
						      <p>Poor</p>
							  <div class="progress poor">
                                   <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:10%"></div>
                              </div>
						 </div>
					</div>
			   </div>
			   
			   
			   <div class="row review-housekeeper-main-box m-0">
			   
			        <div class="col-sm-12 review-housekeeper-main d-flex">
					     <div class="customer-photo"><img src="images/maid.jpg" alt=""></div>
						 <div class="review-customer-cont flex-grow-1">
						      <p class="comment">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce id malesuada eros. Praesent at orci vel nisl vestibulum cursus ac vitae.</p>
							  <p class="comment-date">Thomas <span>10 Dec/2023</span></p>
						 </div>
					</div>
					
					
					<div class="col-sm-12 review-housekeeper-main d-flex">
					     <div class="customer-photo"><img src="images/maid.jpg" alt=""></div>
						 <div class="review-customer-cont flex-grow-1">
						      <p class="comment">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce id malesuada eros. Praesent at orci vel nisl vestibulum cursus ac vitae.</p>
							  <p class="comment-date">Thomas <span>10 Dec/2023</span></p>
						 </div>
					</div>
					
					
					<div class="col-sm-12 review-housekeeper-main d-flex">
					     <div class="customer-photo"><img src="images/maid.jpg" alt=""></div>
						 <div class="review-customer-cont flex-grow-1">
						      <p class="comment">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce id malesuada eros. Praesent at orci vel nisl vestibulum cursus ac vitae.</p>
							  <p class="comment-date">Thomas <span>10 Dec/2023</span></p>
						 </div>
					</div>
					
					
					<div class="col-sm-12 review-housekeeper-main d-flex">
					     <div class="customer-photo"><img src="images/maid.jpg" alt=""></div>
						 <div class="review-customer-cont flex-grow-1">
						      <p class="comment">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce id malesuada eros. Praesent at orci vel nisl vestibulum cursus ac vitae.</p>
							  <p class="comment-date">Thomas <span>10 Dec/2023</span></p>
						 </div>
					</div>
					
					
					<div class="col-sm-12 review-housekeeper-main d-flex">
					     <div class="customer-photo"><img src="images/maid.jpg" alt=""></div>
						 <div class="review-customer-cont flex-grow-1">
						      <p class="comment">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce id malesuada eros. Praesent at orci vel nisl vestibulum cursus ac vitae.</p>
							  <p class="comment-date">Thomas <span>10 Dec/2023</span></p>
						 </div>
					</div>
					
			   </div>
          </div>
     </div>
</div><!-- Maid Popup-->





<script type="text/javascript" src="js/jquery-3.7.1.min.js"></script>
<script type="text/javascript" src="js/bootstrap4.min.js"></script>
<script type="text/javascript" src="js/bootstrap-select.js"></script>


<script type="text/javascript" src="js/owl.carousel.js"></script>
<script type="text/javascript" src="js/jquery.fixie.js"></script> 
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/datepicker.js"></script>
<script type="text/javascript" src="js/jquery.jscroll.js"></script>


<script type="text/javascript">
	
	$().ready(function() {
		$(".scroll").jScroll();
	});
	
</script>


<script type="text/javascript">
$(document).ready(function() {
	
	var strategy = undefined;
    var match = document.location.search.match(/strategy=(.\S+)/);
    if (match) strategy = match[1];

    $(function() {
      $('.sticker').fixie({
        topMargin: 37,
        pinSlop: 0,
        strategy: strategy,
        //pinnedBodyClass: 'showGlobalChange'
      });
    });
	
	$(".scroll").jScroll();
	
	
	var owl = $("#package-category");
	owl.owlCarousel({
	  items: 7,
      autoWidth:true,
	  loop:false,
	  nav: true,
	  dots: false,
	  margin: 10,
	  autoplay: false,
	  autoplayTimeout: 2000,
	  smartSpeed: 800,
	  autoplayHoverPause: false,
	  responsive: {
                  0: {
                    items: 4
                  },
                  600: {
                    items: 4
                  },
                  1000: {
                    items: 5
                  },
				  1100: {
                    items: 6
                  },
				  1200: {
                    items: 6,
					//margin: 50
                  }
	 }
	});
	
	
	
	
	var owl = $("#banner");
	owl.owlCarousel({
	  items: 1,
	  loop: true,
	  margin: 0,
	  autoplay: true,
	  autoplayTimeout: 4000,
	  autoplayHoverPause: true
	});
	
	
	var owl = $("#category");
	owl.owlCarousel({
	  nav: false,
	  loop: false,
	  dots: true,
	  margin: 15,
	  autoplay: true,
	  autoplayTimeout: 2000,
	  smartSpeed: 800,
	  autoplayHoverPause: true,
	  responsive: {
                  0: {
                    items: 2
                  },
                  600: {
                    items: 3
                  },
                  1000: {
                    items: 5
                  },
				  1100: {
                    items: 5
                  },
				  1200: {
                    items: 6,
					margin: 30
                  }
	 }
	});
	
	
	var owl = $("#special-offers");
	owl.owlCarousel({
	  nav: true,
	  loop: true,
	  dots:false,
	  margin: 15,
	  autoplay: true,
	  autoplayTimeout: 2000,
	  smartSpeed: 800,
	  autoplayHoverPause: true,
	  responsive: {
                  0: {
                    items: 1
                  },
                  600: {
                    items: 2
                  },
                  1000: {
                    items: 2
                  },
				  1100: {
                    items: 2
                  },
				  1200: {
                    items: 2,
					margin: 50
                  }
	 }
	});
	
	
	
	var owl = $("#housekeepers");
	owl.owlCarousel({
	  nav: false,
	  loop: false,
	  dots:true,
	  margin: 15,
	  autoplay: false,
	  autoplayTimeout: 2000,
	  smartSpeed: 800,
	  autoplayHoverPause: true,
	  responsive: {
                  0: {
                    items: 2
                  },
                  600: {
                    items: 4
                  },
                  1000: {
                    items: 5
                  },
				  1100: {
                    items: 5
                  },
				  1200: {
                    items: 6,
					//margin: 50
                  }
	 }
	});
	
	
	
	var owl = $("#add-ons");
	owl.owlCarousel({
	  nav: false,
	  loop: false,
	  dots:true,
	  margin: 15,
	  autoplay: true,
	  autoplayTimeout: 2000,
	  smartSpeed: 800,
	  autoplayHoverPause: true,
	  responsive: {
                  0: {
                    items: 1
                  },
                  600: {
                    items: 2
                  },
                  1000: {
                    items: 3
                  },
				  1100: {
                    items: 3
                  },
				  1200: {
                    items: 3,
					//margin: 50
                  }
	 }
	});
	

	var owl = $("#calendar");
	owl.owlCarousel({
	  nav: true,
	  loop: false,
	  dots: false,
	  margin: 15,
	  autoplay: false,
	  autoplayTimeout: 2000,
	  smartSpeed: 800,
	  autoplayHoverPause: false,
	  responsive: {
                  0: {
                    items: 5
                  },
                  600: {
                    items: 5
                  },
                  1000: {
                    items: 10
                  },
				  1100: {
                    items: 10
                  },
				  1200: {
                    items: 10,
					//margin: 50
                  }
	 }
	});

	
	$('.play').on('click', function() {
	  owl.trigger('play.owl.autoplay', [2000])
	})
	$('.stop').on('click', function() {
	  owl.trigger('stop.owl.autoplay')
	})
	
	
	$('.user-btn').click(function() {
		$('.mobile-dropdown').toggle( 500);
	});
	
	
	$('.popup-close, .close-popup').click(function() {
		$('.popup-main').hide( 500);
	});
	
	
	$('.frequency-popup-btn').click(function() {
		$('.frequency-popup').show( 500);
	});
	
	$('.show-login-popup').click(function() {
		$('.login-popup').show( 500);
	});
	

	$('.SHOW-OTP-BTN').click(function() {
		$('.otp-popup').show( 500);
		$('.login-popup').hide( 500);
	});
	
	
	$('.show-add-address-popup').click(function() {
		$('.add-address-popup').show( 500);
	});
	
	
	$('.show-promocode-popup').click(function() {
		$('.promocode-popup').show( 500);
	});
	
	
	$('.show-reset-password-popup').click(function() {
		$('.reset-password-popup').show( 500);
	});
	
	
	$('.show-cancel-booking-popup').click(function() {
		$('.cancel-booking-popup').show( 500);
	});
	
	
	
	$(".delete-action").click(function(){
	  $(this).closest(".manage-address-section").remove();
	});
	
	
	
	$('.mob-total-left').click(function() {
		$('.booking-summary-section').toggle(500);
		$('.booking-main-btn-section').addClass('active');
	});
	
	$('.mob-booking-title').click(function() {
		$('.booking-summary-section').hide(500);
		$('.booking-main-btn-section').removeClass('active');
	});
	
	
	$('.which-housekeeper-thumb-btn').click(function() {
		$('.maid-popup').show( 500);
	});
	
	
	
})


</script>