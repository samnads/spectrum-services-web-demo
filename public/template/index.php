	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Spectrum Booking</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="theme-color" content="#0c3995">
<link rel="icon" type="image/png" href="images/favicon.png"/>

<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" >
<link rel="stylesheet" type="text/css" href="css/style.css"/>
<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css"/>
<link rel="stylesheet" type="text/css" href="css/animation.css"/>
<link rel="stylesheet" type="text/css" href="css/owl.carousel.min.css">

</head>

<body>

<div class="wrapper-main">
	
    
    <?php require_once('include/header.php') ?>
	
    
    <section>
		<div class="container">
			<div class="row banner-wrapper m-0">
				 <div id="banner" class="owl-carousel owl-theme p-0">
					   <div class="item position-relative">
							<img src="images/booking-banner.jpg" class="object-fit_cover pc-view" alt="" />
							<img src="images/booking-banner-mob.jpg" class="object-fit_cover mob-view" alt="" />
					   </div>
				  
					   <div class="item position-relative">
							<img src="images/booking-banner.jpg" class="object-fit_cover pc-view" alt="" />
							<img src="images/booking-banner-mob.jpg" class="object-fit_cover mob-view" alt="" />
					   </div>
					   
					   <div class="item position-relative">
							<img src="images/booking-banner.jpg" class="object-fit_cover pc-view" alt="" />
							<img src="images/booking-banner-mob.jpg" class="object-fit_cover mob-view" alt="" />
					   </div>
				</div> 
			</div>
		</div>  
    </section>
	
	
	
	
	
	<section class="booking-category-wrapper">
    <div class="container">
	    <h3>Category</h3>
    	<div class="row booking-category-main m-0">
		
		     <div id="category" class="owl-carousel owl-theme p-0">
				  <div class="item">
					   <div class="col-sm-12 booking-cat-thumb-main">
						  <div class="col-12 booking-cat-thumb-image"><img src="images/house-cleaning-icon.png" alt=""/></div>
						  <div class="col-12 booking-cat-thumb-text"><h2>House Cleaning<br />Services</h2></div>
						  <div class="col-12 booking-cat-thumb-btn"><a href="javascript:void(0);" class="sp-btn frequency-popup-btn">Book Now</a></div>
					  </div>
				  </div>
				  
				  
				  <div class="item">
				       <div class="col-sm-12 booking-cat-thumb-main">
					        <div class="col-12 booking-cat-thumb-image"><img src="images/deepcleaning-icon.png" alt=""/></div>
					        <div class="col-12 booking-cat-thumb-text"><h2>Deep Cleaning<br />Services</h2></div>
					        <div class="col-12 booking-cat-thumb-btn"><a href="deep-cleaning.php" class="sp-btn">Book Now</a></div>
				       </div>
				  </div>
				  
				  
				  <div class="item">
				       <div class="col-sm-12 booking-cat-thumb-main">
					        <div class="col-12 booking-cat-thumb-image"><img src="images/furniture-cleaning-icon.png" alt=""/></div>
					        <div class="col-12 booking-cat-thumb-text"><h2>Furniture Cleaning<br />Services</h2></div>
					        <div class="col-12 booking-cat-thumb-btn"><a href="#" class="sp-btn">Book Now</a></div>
				       </div>
				  </div>
			 </div>
        </div>
     </div>   
    </section>
	
	
	
	
	<section class="special-offers-wrapper">
    <div class="container">
	    <h3>Special Offers for you</h3>
    	<div class="row m-0">
             <div id="special-offers" class="owl-carousel owl-theme p-0">
				   <div class="item position-relative">
						<img src="images/offer-1.jpg" class="" alt="" />
				   </div>
			  
				   <div class="item position-relative">
						<img src="images/offer-2.jpg" class="" alt="" />
				   </div>
				   
				   
			</div>
        </div>
     </div>   
    </section>
    

</div>










<?php require_once('include/footer.php') ?>
          
</body>
</html>
