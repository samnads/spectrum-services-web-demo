@extends('layouts.main', ['body_css_class' => 'booking-section-page'])
@section('title', $service_type['service_type_name'])
@section('meta_description', $service_type['service_type_name'])
@section('content')
    <section>
        <div class="container">
            <form id="booking-form" name="booking-form" autocomplete="off" accept-charset="utf-8">
                <input name="id" value="{{ session('customer_id') }}" type="hidden">
                <input name="address_id" value="{{ session('customer_default_address_id') }}" type="hidden">
                <input name="token" value="{{ session('customer_token') }}" type="hidden">
                <input name="service_type_id" value="{{ $service_type_id }}" type="hidden">
                <input name="rush_slot_id" type="hidden">
                <input name="No_weeks" value="0" type="hidden">
                <input name="frequency" value="OD" type="radio" checked style="display: none">
                <input name="frequency_discount" type="hidden">
                <input name="professionals_count" value="1" type="checkbox" checked style="display: none">
                <input name="hours" value="2" type="checkbox" checked style="display: none">
                <input name="cleaning_materials" value="no" type="radio" checked style="display: none">
                <div class="row inner-wrapper m-0">
                    @include('includes.package.step-1')
                    @include('includes.package.step-2')
                    @include('includes.package.step-3')
                    @include('includes.package.summary')
                </div>
            </form>
            <!--<center><iframe style="border:6px dotted green" name="myframe" id="myframe" width="600"
                    height="500"></iframe></center>
            <form action="https://sbcheckout.payfort.com/FortAPI/paymentPage" method="post" id="payfort-form"
                target="myframe">
                <INPUT type="hidden" name="service_command" value="TOKENIZATION">
                <INPUT type="hidden" name="language" value="en">
                <INPUT type="hidden" name="merchant_identifier" value="CKxScKab">
                <INPUT type="hidden" name="access_code" value="XIj8KVLKSPumGuyfnu6g">
                <INPUT type="hidden" name="signature" value="">
                <INPUT type="hidden" name="return_url" value="">
                <INPUT type="hidden" name="merchant_reference" value="">
                <input value="Show Payment Form" type="submit" id="form1">
            </form>-->
        </div>
        @include('popups.maid-details-popup')
        @include('popups.address-pick-popup')
        @include('popups.coupon-apply-popup')
        @include('popups.package-details-popup')
    </section>
@endsection
@push('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap-select.css') }}">
@endpush
@push('scripts')
    <script type="text/javascript" src="{{ asset('js/bootstrap-select.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.query-object.js') }}"></script>
    <script type="text/javascript" src="https://applepay.cdn-apple.com/jsapi/v1/apple-pay-sdk.js"></script>
    <script type="text/javascript" src="{{ asset('js/model.js?v=') . Config::get('version.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/model.package.js?v=') . Config::get('version.js') }}"></script>
@endpush
