@extends('layouts.main', ['body_css_class' => 'booking-section-page'])
@section('title', $special_offer['name'])
@section('meta_description', $special_offer['name'])
@section('content')
    <section>
        <div class="container">
            <form id="booking-form" name="booking-form" autocomplete="off" accept-charset="utf-8">
                <input name="id" value="{{ session('customer_id') }}" type="hidden">
                <input name="address_id" value="{{ session('customer_default_address_id') }}" type="hidden">
                <input name="token" value="{{ session('customer_token') }}" type="hidden">
                <input name="service_type_id" value="{{ $special_offer['service_type_id'] }}" type="hidden">
                <input name="offer_id" value="{{ $special_offer['offer_id'] }}" type="hidden">
                <input name="rush_slot_id" type="hidden">
                <input name="No_weeks" value="0" type="hidden">
                <input name="frequency_discount" type="hidden">
                <input name="frequency" value="{{ $special_offer['frequency_code'] }}" type="hidden">
                <div class="row inner-wrapper m-0">
                    @include('includes.special-offer.step-1')
                    @include('includes.special-offer.step-2')
                    @include('includes.special-offer.step-3')
                    @include('includes.special-offer.summary')
                </div>
            </form>
        </div>
        @include('popups.maid-details-popup')
        @include('popups.address-pick-popup')
        @include('popups.coupon-apply-popup')
    </section>
@endsection
@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap-select.css') }}">
@endpush
@push('scripts')
    <script type="text/javascript">
        var _frequencies = @json(frequencies());
        var _service_types = @json(service_types());
    </script>
    <script type="text/javascript" src="{{ asset('js/bootstrap-select.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.query-object.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/model.js?v=') . Config::get('version.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/model.special-offer.js?v=') . Config::get('version.js') }}"></script>
@endpush
