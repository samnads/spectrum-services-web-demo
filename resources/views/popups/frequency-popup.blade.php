<div class="popup-main frequency-popup" id="frequency-popup">
    <div class="row min-vh-100 m-0">
        <div class="mx-auto my-auto shadow popup-main-cont">
            <div class="popup-close" data-action="close-frequency-popup"><img src="images/el-close-white.png" alt=""></div>
            <div class="col-sm-12 popup-head-text">
                <h4>Choose Your Frequency</h4>
            </div>
            <div class="row m-0">
                @foreach ($frequencies as $frequency)
                    <div class="col-sm-12 frequency-main frequency-{{ $frequency['id'] }}">
                        <input id="frequency{{ $frequency['id'] }}" value="{{ $frequency['code'] }}" name="frequency"
                            class="" type="radio">
                        <label for="frequency{{ $frequency['id'] }}"> <span></span> &nbsp; {{ $frequency['name'] }}<p>
                                {{ $frequency['description'] }}</p></label>
                        @if ($frequency['percentage'] > 0)
                            <div class="frequency-offer">{{ $frequency['percentage'] }}%<span>OFF</span></div>
                        @endif
                    </div>
                @endforeach
                <div class="col-sm-6 frequency-main pt-3">
                    <input value="Next" class="text-field-btn" data-action="frequency-select"
                        type="button">
                    <input value="Next" class="text-field-btn" data-action="frequency-change"
                        type="button" style="display: none">
                </div>
            </div>
        </div>
    </div>
</div><!-- Frequency Popup-->
