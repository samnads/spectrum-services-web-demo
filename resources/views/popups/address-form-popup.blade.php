<div class="popup-main add-address-popup" id="address-form-popup">
    <form id="new-address-popup-form">
        <input name="next_action" type="hidden">
        <input name="address_id" type="hidden">
        <div class="row min-vh-100 m-0">
            <div class=" mx-auto my-auto shadow popup-main-cont">
                <div class="popup-close" data-action="close"><img src="{{ asset('images/el-close-white.png') }}"
                        alt="">
                </div>
                <div class="col-sm-12 popup-head-text">
                    <h4></h4>
                </div>
                <div class="row m-0">
                    <div class="col-md-6 position-relative p-0 mb-3">
                        <div class="map-search">
                            <input name="selected_address" placeholder="Seach..." class="text-field us3-address"
                                type="search">
                        </div>
                        <input name="latitude" id="latitude" type="hidden">
                        <input name="longitude" id="longitude" type="hidden">
                        <input type="hidden" class="us3-radius" value="0">
                        <div class="us3" style="height: 300px; width:100%"></div>
                    </div>
                    <div class="col-md-6 address-details">
                        <div class="row m-0">
                            <div class="col-sm-12 p-0">
                                <div class="row text-field-main">
                                    <div class="col-sm-4 m-flat-no">
                                        <label>Flat No.</label>
                                        <input name="flat_no" placeholder="" class="text-field" type="text">
                                    </div>
                                    <div class="col-sm-8">
                                        <label>Building</label>
                                        <input name="building" placeholder="" class="text-field" type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 text-field-main">
                                <label>Street</label>
                                <input name="street" placeholder="" class="text-field" type="text">
                            </div>
                            <div class="col-sm-12 text-field-main">
                                <select class="selectpicker form-control" id="" data-container="body"
                                    data-live-search="true" data-hide-disabled="true" name="area_id">
                                    <option value="">-- Select Area --</option>
                                    @foreach ($data['result']['areas'] as $key => $area)
                                        <option value="{{ $area['id'] }}">{{ $area['name'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-sm-12 text-field-main pb-0">
                                <button class="text-field-btn" type="submit">Save Address</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
