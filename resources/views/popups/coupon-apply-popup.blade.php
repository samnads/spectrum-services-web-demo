<div class="popup-main promocode-popup" id="coupon-apply-popup">
    <form id="coupon-apply-form">
        <div class="row min-vh-100 m-0">
            <div class=" mx-auto my-auto shadow popup-main-cont">
                <div class="popup-close" data-action="close"><img src="{{asset('images/el-close-white.png')}}" alt=""></div>
                <div class="col-sm-12 popup-head-text">
                    <h4>Voucher Code</h4>
                </div>
                <div class="row m-0">
                    <div class="col-sm-12 promo-code-field">
                        <input name="coupon_code" placeholder="Enter voucher code..." class="text-field" type="search">
                    </div>
                    <div class="col-sm-12 frequency-main pt-3">
                        <button class="text-field-btn" type="submit">Apply</button>
                    </div>
                </div>
                <!--<div class="row m-0">
                    <div class="col-sm-12 promo-code-field">
                        <input name="" placeholder="Enter Promo Code" class="text-field" type="text">
                    </div>
                    <div class="col-sm-12 p-0">
                        <h5>Available Codes</h5>
                    </div>
                    <div class="col-sm-12 promocode-cont-set">
                        <div class="d-flex promocode-cont-main">
                            <div class="promocode-cont flex-grow-1">
                                <p><strong>SP5368</strong><br>
                                    <span>Lorem Ipsum is simply dummy text of the printing and typesetting
                                        industry.</span>
                                </p>
                            </div>
                            <div class="promocode-cont-btn v-center">
                                <input value="Apply" class="text-field-btn" type="submit">
                            </div>
                        </div>
                        <div class="d-flex promocode-cont-main">
                            <div class="promocode-cont flex-grow-1">
                                <p><strong>SP5368</strong><br>
                                    <span>Lorem Ipsum is simply dummy text of the printing Lorem Ipsum is simply dummy
                                        text
                                        of the printing and typesetting industry.</span>
                                </p>
                            </div>
                            <div class="promocode-cont-btn v-center">
                                <input value="Apply" class="text-field-btn" type="submit">
                            </div>
                        </div>
                        <div class="d-flex promocode-cont-main">
                            <div class="promocode-cont flex-grow-1">
                                <p><strong>SP5368</strong><br>
                                    <span>Lorem Ipsum is simply dummy text of the printing and typesetting
                                        industry.</span>
                                </p>
                            </div>
                            <div class="promocode-cont-btn v-center">
                                <input value="Apply" class="text-field-btn" type="submit">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 frequency-main pt-3">
                        <button class="text-field-btn" type="submit">Apply</button>
                    </div>
                </div>-->
            </div>
        </div>
    </form>
</div>
