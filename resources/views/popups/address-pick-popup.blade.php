<div class="popup-main add-address-popup" id="address-pick-popup">
    <form id="address-pick-popup-form">
        <input name="address_id" type="hidden">
        <div class="row min-vh-100 m-0">
            <div class="mx-auto my-auto shadow popup-main-cont">
                <!--<div class="popup-close" data-action="close"><img src="{{ asset('images/el-close-white.png') }}"
                        alt="">
                </div>-->
                <div class="col-sm-12 popup-head-text">
                    <h4>Select Booking Address</h4>
                </div>
                <div class="col-sm-12 mb-3 text-right">
                    <a class="sp-btn show-add-address-popup" data-action="new-address" data-next-action="pick-this">Add New</a>
                </div>
                <div class="row m-0">
                    <div class="col-sm-12 p-0" id="address-pick-holder">
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
