<div class="popup-main" id="addon-details-popup">
    <div class="row min-vh-100 m-0">
        <div class=" mx-auto my-auto shadow popup-main-cont">
            <div class="popup-close"><img src="{{ asset('images/el-close-white.png') }}" alt=""></div>
            <div class="row m-0">
                <div class="col-12 p-0"><img id="addon-img" src="{{ asset('images/add-ons1.jpg') }}" alt=""></div>
                <div class="col-12 content">
                    <h3><span class="name">#</span></h3>
                    <p><span class="description">#</span></p>
                    <div class="col-12 booking-packages-price p-0 pb-5 pt-3">
                        <label class="package_amount">AED <span>#</span> #</label>
                    </div>
                    <div class="col-sm-12 p-0 pb-3 addon-includes">
                        <h4><strong>What's included</strong></h4>
                        <ul>
                            <!-- points goes here -->
                        </ul>
                    </div>
                    <div class="col-sm-12 p-0 addon-excludes">
                        <h4><strong>What's excluded</strong></h4>
                        <ul>
                            <!-- points goes here -->
                        </ul>
                    </div>
                </div>
                <div class="col-12 pak-det-popup-bot-btn js-close">Back to Booking</div>
            </div>
        </div>
    </div>
</div>
