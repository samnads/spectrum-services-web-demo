<div class="popup-main maid-popup" id="maid-details-popup">
     <div class="row min-vh-100 m-0">
          <div class=" mx-auto my-auto shadow popup-main-cont">
               <div class="popup-close" data-action="close"><img src="{{asset('images/el-close-white.png')}}" alt=""></div>
			   <div class="col-sm-12 popup-head-text"><h4>Professionals Details</h4></div>
			   <div class="row m-0">
			        <div class="col-sm-6 which-housekeeper-thumb p-0 pe-4">
					     <div class="which-housekeeper-thumb-image"><img src="#" alt="" class="js-maid-img"></div>
					     <div class="which-housekeeper-thumb-name js-maid-name v-center"></div>
					     <div class="which-housekeeper-thumb-rating"><img src="{{asset('images/5star.png')}}" alt=""></div>
					</div>
					<div class="col-sm-6 housekeeper-thumb-rating p-0">
					     <div class="col-sm-12 housekeeper-rating-cont p-0">
						      <p>Excellent</p>
							  <div class="progress excellent">
                                   <div class="progress-bar" role="progressbar" aria-valuenow="1" aria-valuemin="0" aria-valuemax="5" style="width:20%"></div>
                              </div>
						 </div>
						 <div class="col-sm-12 housekeeper-rating-cont p-0">
						      <p>Good</p>
							  <div class="progress good">
                                   <div class="progress-bar" role="progressbar" aria-valuenow="2" aria-valuemin="0" aria-valuemax="5" style="width:40%"></div>
                              </div>
						 </div>
						 <div class="col-sm-12 housekeeper-rating-cont p-0">
						      <p>Average</p>
							  <div class="progress average">
                                   <div class="progress-bar" role="progressbar" aria-valuenow="3" aria-valuemin="0" aria-valuemax="5" style="width:60%"></div>
                              </div>
						 </div>
						 <div class="col-sm-12 housekeeper-rating-cont p-0">
						      <p>Bad</p>
							  <div class="progress bad">
                                   <div class="progress-bar" role="progressbar" aria-valuenow="4" aria-valuemin="0" aria-valuemax="5" style="width:80%"></div>
                              </div>
						 </div>
						 
						 <div class="col-sm-12 housekeeper-rating-cont p-0">
						      <p>Poor</p>
							  <div class="progress poor">
                                   <div class="progress-bar" role="progressbar" aria-valuenow="5" aria-valuemin="0" aria-valuemax="5" style="width:100%"></div>
                              </div>
						 </div>
					</div>
			   </div>
			   <div class="row review-housekeeper-main-box m-0" style="display: none">
			        <div class="col-sm-12 review-housekeeper-main d-flex">
					     <div class="customer-photo"><img src="#" alt=""></div>
						 <div class="review-customer-cont flex-grow-1">
						      <p class="comment">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce id malesuada eros. Praesent at orci vel nisl vestibulum cursus ac vitae.</p>
							  <p class="comment-date">Thomas <span>10 Dec/2023</span></p>
						 </div>
					</div>
			   </div>
          </div>
     </div>
</div>