<div class="popup-main" id="name-popup">
    <form id="name-entry-form">
        <div class="row min-vh-100 m-0">
            <div class=" mx-auto my-auto shadow popup-main-cont">
                <div class="col-sm-12 popup-head-text">
                    <h4>Enter details</h4>
                </div>
                <div class="row m-0">
                    <div class="col-sm-12 login-content p-0">
                        <p>We are glad to have you here. please enter your name and email to complete the profile.</p>
                    </div>
                    <div class="col-sm-12 pb-3">
                        <input name="name" class="text-field" type="text" placeholder="Type your name..." autocomplete="off">
                    </div>
                    <div class="col-sm-12">
                        <input name="email" class="text-field" type="email" placeholder="Type your email..." autocomplete="off">
                    </div>
                    <div class="col-sm-12 frequency-main pt-3">
                        <button class="text-field-btn" type="submit">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div><!-- Name Entry Popup-->
