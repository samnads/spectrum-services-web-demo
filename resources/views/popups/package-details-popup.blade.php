<div class="popup-main packages-details-popup" id="package-details-popup">
	<div class="row min-vh-100 m-0">
		<div class=" mx-auto my-auto shadow popup-main-cont">
			<div class="popup-close js-close"><img src="{{ asset('images/el-close-white.png') }}" alt=""></div>
			<div class="row m-0">
				<!--<div class="col-12 p-0 package_tumbnail"><img id="package-img" src="#" alt=""></div>-->
				<div class="col-12 packages-details-popup-cont">
					<h3><span class="package_details_name name">#</span></h3>
					<p><span class="package_details_description description">#</span></p>
					<div class="col-12 booking-packages-price p-0 pb-5 pt-3">
						<label class="package_amount">AED <span>#</span> #</label>
					</div>
				</div>
				<div class="col-12 pak-det-popup-bot-btn js-close">Back to Booking</div>
			</div>
		</div>
	</div>
</div>