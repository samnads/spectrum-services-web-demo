<div class="popup-main" id="change-pay-mode-popup">
    <form id="change-pay-mode-popup-form">
        <input name="booking_id" type="hidden" />
        <input name="PaymentMethod" type="hidden" />
        <div class="row min-vh-100 m-0">
            <div class=" mx-auto my-auto shadow popup-main-cont">
                <div class="popup-close" data-action="close"><img src="{{ asset('images/el-close-white.png') }}"
                        alt=""></div>
                <div class="col-sm-12 popup-head-text">
                    <h4>Change Payment Mode<span class="booking_id"></span></h4>
                </div>
                <div class="row m-0 mt-3">
                    <div class="col-sm-12 payment-method-wrapper p-0">
                        <div class="col-sm-12 booking-form-list payment-method p-0">
                            <ul id="payment-method-holder">
                                @foreach ($data['result']['payment_types'] as $key => $payment_type)
                                    @if ($payment_type['show_in_web'] == 1)
                                        <li class="{{ $payment_type['code'] }}-opt"
                                            id="payment-mode-{{ $payment_type['id'] }}">
                                            <input id="payment-method-{{ $payment_type['id'] }}"
                                                value="{{ $payment_type['id'] }}" name="payment_method" class=""
                                                type="radio">
                                            <label for="payment-method-{{ $payment_type['id'] }}">
                                                <img src="{{ asset('images/payment-'. $payment_type['code'].'.jpg') }}" alt="{{$payment_type['code']}}">
                                            </label>
                                        </li>
                                    @endif
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row m-0">
                    <div class="col-sm-12 p-0 m-auto" id="addresses-list">
                        <div class="col-sm-12 booking-main-btn-section">
                            <div class="row m-0">
                                <div class="col-lg-5 col-md-4 col-sm-6 p-0">
                                    <button class="text-field-btn" type="submit">Complete</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div><!-- Address Popup-->
