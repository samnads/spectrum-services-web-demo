@extends('layouts.payment')
@section('title', 'Payment')
@section('content')
    <section>
        <div class="container">
            <div class="row inner-wrapper">
                <div class="container no-left-right-padding">
                    <div class="col-md-12 col-sm-12">
                        <h2 class="text-center">Make Payment</h2>
                        <div class="col-md-12 col-sm-12 booking-his-main no-left-right-padding">
                            <div class="col-md-6 col-sm-12  user-profile no-left-right-padding"
                                style="float: none; margin:0 auto;">
                                <form name="edit-profile-form" method="POST" action="{{ url('payment/confirmation') }}">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                    <div class="col-md-12 col-sm-12 register-field  no-left-right-padding">
                                        <div class="booking-form-field">
                                            <div class="text-field-main">
                                                <p>Amount</p>
                                                <input name="amount" class="text-field no-arrow" type="number"
                                                    value="{{ Request::get('amount') }}" step="any">
                                                    <input name="customer_id" class="text-field no-arrow" type="hidden"
                                                    value="{{ @Request::get('id') }}">
                                                <input name="transaction_charge" class="text-field no-arrow" type="hidden"
                                                    value="{{ Request::get('amount') * 0.03 }}">
                                                    <input name="total_payable" class="text-field no-arrow" type="hidden"
                                                    value="{{ Request::get('amount') + (Request::get('amount') * 0.03) }}">
                                            </div>
                                        </div><!--booking-form-field end-->
                                    </div><!--register-field end-->
                                    <div class="col-md-12 col-sm-12 register-field  no-left-right-padding">
                                        <div class="booking-form-field">
                                            <div class="text-field-main">
                                                <p>Description</p>
                                                <textarea class="text-field-big" rows="6" name="description">{{ Request::get('message') }}</textarea>
                                            </div>
                                        </div><!--booking-form-field end-->
                                    </div><!--register-field end-->
                                    <div class="col-md-12 col-sm-12 register-field  no-left-right-padding">
                                        <div class="booking-form-field">
                                            <div class="col-md-12 col-sm-12 text-field-main no-left-right-padding">
                                                <div class="col-md-6 col-sm-10 text-field-main no-left-padding">
                                                    <button class="text-field-btn" type="submit">MAKE PAYMENT</button>
                                                </div>
                                            </div>
                                        </div><!--booking-form-field end-->
                                        <div class="clear"></div><!--clear end-->
                                    </div><!--register-field end-->
                                </form>
                            </div>
                        </div>
                    </div><!--welcome-cont-main end-->
                </div><!--container end-->
            </div><!--row content-wrapper end-->
        </div>
    </section>
@endsection
@push('styles')
@endpush
@push('scripts')
@endpush
