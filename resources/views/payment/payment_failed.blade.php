@extends('layouts.payment')
@section('title', 'Payment Failed')
@section('content')
    <section>
        <div class="container">
            <div class="row inner-wrapper">
                <div class="container no-left-right-padding">
                    <div class="alert alert-danger mt-3" role="alert">
                        <h4>Payment Failed !</h4>
                    </div>
                    <div class="alert alert-light mt-3 border" role="alert">
                        <p>Payment failed...</p>
                        <hr>
                        <p class="mb-0">For any questions and clarifications on your payment please
                            call us at <strong>800 7274</strong> or email us at
                            <strong>info@spectrumservices.ae</strong>
                        </p>
                    </div>
                    <div class="card border-danger" style="max-width: 25rem;">
                        <div class="card-header bg-transparent border-danger">Payment Details</div>
                        <div class="card-body">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th scope="row">Customer Name</th>
                                        <td>{{ $customer['customer_name'] }}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Order Ref.</th>
                                        <td>{{ $online_payment['reference_id'] }}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Email ID</th>
                                        <td>{{ @$customer['email_address'] }}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Contact Number</th>
                                        <td>{{ $customer['mobile_number_1'] }}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Net Payable</th>
                                        <td>AED {{ $online_payment['amount'] + $online_payment['transaction_charge'] }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    @if (session('direct_payment_url'))
                        <div class="col-md-6 col-sm-6 register-field mt-2">
                            <div class="booking-form-field">
                                <div class="col-md-6 col-sm-6 text-field-main no-left-right-padding">
                                    <div class="col-md-6 col-sm-10 text-field-main no-left-padding">
                                        <a href="{{ session('direct_payment_url') }}"><button class="text-field-btn" type="button">Retry
                                Payment</button></a>
                                    </div>
                                </div>
                            </div><!--booking-form-field end-->
                            <div class="clear"></div><!--clear end-->
                        </div><!--register-field end-->
                    @endif
                </div><!--container end-->
            </div><!--row content-wrapper end-->
        </div>
    </section>
@endsection
@push('styles')
@endpush
@push('scripts')
@endpush
