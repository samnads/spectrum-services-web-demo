<html>

<head>
    <title>Verifying...</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous">
    </script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css"
        integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.0/jquery.min.js"
        integrity="sha512-3gJwYpMe3QewGELv8k/BX9vcqhryRdzRMxVfq6ngyWXwo03GFEzjsUm8Q7RZcHPHksttq7/GFoxjCVUjkjvPdw=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
</head>

<body class="d-flex align-items-center">
    <div class="container text-center">
        <div class="row">
            <div class="col">
                <div id="loading" class="mb-5">
                    <div class="spinner-border text-info" role="status">
                        <span class="visually-hidden">Loading...</span>
                    </div>
                </div>
                <p class="display-1">Please wait...</p>
                <p class="display-6 mb-5">Do not press refresh or back button !</p>
                <p class="display-6 text-primary" id="response_message">&nbsp;</p>
                <p class="display-6 text-success" id="response_message_success">&nbsp;</p>
                <p class="display-6 text-danger" id="response_message_error">&nbsp;</p>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        var _base_url = "{{ url('') }}/";
        var _api_url = "{{ Config::get('url.api_url') }}";
        var _current_url = "{{ strtok(url()->full(), '?') }}";

        function verifyPayment() {
            $.ajax({
                url: _api_url + 'customer/payment/payfort/verify',
                method: "POST",
                cache: false,
                dataType: "json",
                data: {
                    'merchant_reference': '{{ request()->merchant_reference }}'
                },
                success: function(response) {
                    $('#response_message').html("&nbsp;");
                    if (response.result.status == "success") {
                        var status = response.result.data;
                        console.log(status);
                        if (status.fort_id && response.result.data.data.authorized_amount > 0) {
                            $('#response_message_success').html(status.response_message);
                            window.location.href = _base_url + 'booking/success/' +
                                '{{ request()->merchant_reference }}';
                        } else{
                            $('#response_message_error').html("Failed");
                            setTimeout(function() {
                                window.location.href =
                                    _base_url + 'booking/failed/' +
                                    '{{ request()->merchant_reference }}';
                            }, 3000);
                        }
                    } else {
                        $('#response_message_error').html(response.result.message);
                        verifyPayment();
                    }
                },
                error: function(error) {
                    console.log(error);
                    //$('#response_message_error').html(error.responseText);
                    $('#response_message').html("Still Processing...");
                    verifyPayment();
                }
            });
        }
        verifyPayment();
        $(function() {
            //setInterval(verifyPayment, 3000);
        });
    </script>
</body>

</html>
