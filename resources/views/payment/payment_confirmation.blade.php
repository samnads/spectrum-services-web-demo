@extends('layouts.payment')
@section('title', 'Payment Confirmation')
@section('content')
    <section>
        <div class="container">
            <div class="row inner-wrapper">
                <div class="container no-left-right-padding">
                    <div class="col-md-12 col-sm-12 mt-2">
                        <h2 class="text-center">Payment Confirmation</h2>
                        <div class="alert alert-secondary mt-2" role="alert">
                            <h4 class="alert-heading">Dear {{ $customer['customer_name'] }}</h4>
                            <p> Please confirm your requested payment of <b>AED {{ $total_payable }}</b></p>
                            <hr>
                            <p class="mb-0">{{ Request::post('description') }}</p>
                        </div>
                        <div class="card" style="max-width: 25rem;">
                            <div class="card-header bg-transparent">Order Details</div>
                            <div class="card-body">
                                <table class="table">
                                    <tbody>
                                        <tr>
                                            <th scope="row">Customer Name</th>
                                            <td>{{ $customer['customer_name'] }}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Order Ref.</th>
                                            <td>{{ $online_payment['reference_id'] }}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Email ID</th>
                                            <td>{{ @$customer['email_address'] }}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Contact Number</th>
                                            <td>{{ $customer['mobile_number_1'] }}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Service Charge</th>
                                            <td>AED {{ $amount }}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Transaction Charge</th>
                                            <td>AED {{ $transaction_charge }}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Total payable</th>
                                            <td>AED {{ $total_payable }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 mt-2">
                            <div class="col-md-6 col-sm-12">
                                <form name="edit-profile-form" method="POST" action="{{ $payfort_payment_url }}">
                                    @foreach ($payfort_params as $a => $b)
                                        <input type='hidden' name='{{ htmlentities($a) }}'
                                            value='{{ htmlentities($b) }}'>
                                    @endforeach
                                    <div class="col-md-12 col-sm-12 register-field  no-left-right-padding">
                                        <div class="booking-form-field">
                                            <div class="col-md-12 col-sm-12 text-field-main no-left-right-padding">
                                                <div class="col-md-6 col-sm-10 text-field-main no-left-padding">
                                                    <button class="text-field-btn" type="submit">PAY NOW</button>
                                                </div>
                                            </div>
                                        </div><!--booking-form-field end-->
                                        <div class="clear"></div><!--clear end-->
                                    </div><!--register-field end-->
                                </form>
                            </div>
                        </div>
                    </div><!--welcome-cont-main end-->
                </div><!--container end-->
            </div><!--row content-wrapper end-->
        </div>
    </section>
@endsection
@push('styles')
@endpush
@push('scripts')
@endpush
