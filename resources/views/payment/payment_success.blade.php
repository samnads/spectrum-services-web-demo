@extends('layouts.payment')
@section('title', 'Payment Success')
@section('content')
    <section>
        <div class="container">
            <div class="row inner-wrapper">
                <div class="container no-left-right-padding">
                    <div class="alert alert-success mt-3" role="alert">
                        <h4>Payment Success !</h4>
                    </div>
                    <div class="alert alert-light mt-3 border" role="alert">
                        <p>Thank you for your payment...</p>
                        <hr>
                        <p class="mb-0">For any questions and clarifications on your payment please
                            call us at <strong>800 7274</strong> or email us at
                            <strong>info@spectrumservices.ae</strong>
                        </p>
                    </div>
                    <div class="card border-success" style="max-width: 25rem;">
                        <div class="card-header bg-transparent border-success">Payment Details</div>
                        <div class="card-body">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th scope="row">Customer Name</th>
                                        <td>{{ $customer['customer_name'] }}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Order Ref.</th>
                                        <td>{{ $online_payment['reference_id'] }}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Payment Ref.</th>
                                        <td>{{ $online_payment['transaction_id'] }}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Email ID</th>
                                        <td>{{ @$customer['email_address'] }}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Contact Number</th>
                                        <td>{{ $customer['mobile_number_1'] }}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Net Payable</th>
                                        <td>AED {{ $online_payment['amount'] + $online_payment['transaction_charge'] }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div><!--container end-->
            </div><!--row content-wrapper end-->
        </div>
    </section>
@endsection
@push('styles')
@endpush
@push('scripts')
@endpush
