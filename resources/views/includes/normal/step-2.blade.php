<div class="col-lg-8 col-md-12 booking-form-left step-2" style="display: none">
    <div class="col-sm-12">
        <div class="d-flex">
            <div class="step-back-icon"><a href="#" onclick="return false;" class="back-arrow" title="Click to Back" data-action="prev-step" data-step="2"></a></div>
            <div class="booking-page-title flex-grow-1">
                <h3>Service Details</h3>
            </div>
            <div class="booking-steps">Step 2 of 3</div>
        </div>
    </div>
    <div class="col-sm-12 booking-alert-main">
        <div class="d-flex booking-alert">
            <div class="booking-alert-icon"><i class="fa fa-refresh"></i></div>
            <div class="booking-alert-cont flex-grow-1">
                <p><strong>Frequency</strong><br /><span class="frequency-name">&nbsp;</span></p>
            </div>
            <div class="booking-alert-btn frequency-popup-btn" data-action="frequency-popup">Change</div>
        </div>
    </div>

    <div class="col-sm-12 cleaning-materials-wrapper">
        <div class="col-sm-12 p-0 pb-2">
            <h4>Do you require cleaning materials?</h4>
        </div>
        <div class="col-sm-12 booking-form-list p-0">
            <ul id="cleaning-materials-holder">
                <li><input id="cleaning_materials_0" value="no" name="cleaning_materials" class="" type="radio">
                    <label for="cleaning_materials_0">No, I have them</label>
                </li>

                <li><input id="cleaning_materials_1" value="yes" name="cleaning_materials" class="" type="radio">
                    <label for="cleaning_materials_1">Yes, Please</label>
                </li>
            </ul>
        </div>
    </div>

    <div class="col-sm-12 number-of-weeks-wrapper no-of-weeks" style="display: none">
        <div class="row">

            <div class="col-sm-6 col-8">
                <div class="col-sm-12 p-0 pb-2">
                    <h4>No of weeks</h4>
                </div>
                <div class="col-sm-10 booking-form-field p-0">
                    <input name="No_weeks" class="text-field no-arrow" type="number" min="2" max="50" />
                </div>
            </div>

            <div class="col-sm-6 col-4">
                <div class="col-sm-12 p-0 pb-2">
                    <h4>Never End</h4>
                </div>
                <div class="col-sm-12 booking-form-field p-0">
                    <label class="switch">
                        <input type="checkbox" name="never_end" checked>
                        <span class="slider round"></span>
                    </label>
                </div>
            </div>

        </div>
    </div>


    


    <div class="col-sm-12 cleaning-materials-wrapper">
        <div class="col-sm-12 p-0 pb-2">
            <h4>Do you have any specific cleaning instruction?</h4>
        </div>
        <div class="col-sm-12 booking-form-field p-0">
            <textarea name="instructions" cols="" rows="" class="text-field-big" spellcheck="false" placeholder="Write here..."></textarea>
        </div>
    </div>
    <div class="col-sm-12 add-ons-wrapper">
        <div class="col-sm-12 p-0 pb-2">
            <h3>Popular Add-ons</h3>
            <h4>People also added</h4>
        </div>
        <div class="col-sm-12 add-ons-scroll-section">
            <div id="add-ons" class="owl-carousel owl-theme p-0">
                @foreach ($service_type_addons as $key => $addon)
                <div class="item">
                    <input name="addon[]" type="hidden" value="{{ json_encode($addon) }}" />
                    <input id="addon_{{ $addon['service_addons_id'] }}" value="{{ $addon['service_addons_id'] }}" name="addons[]" class="" type="checkbox">
                    <label for="addon_{{ $addon['service_addons_id'] }}">
                        <div class="add-ons-scroll-thumb">
                            <div class="add-ons-scroll-image"><img src="{{ $addon['image_url'] }}" alt="" />
                            </div>
                            <div class="add-ons-scroll-cont">
                                <h4>{{ $addon['service_addon_name'] }}</h4>
                                <p><!--<strong>{{ $addon['service_addon_name'] }}</strong>-->
                                    {{ substr($addon['service_addon_description'], 0, 30) }} <a href="javascript:void(0);" data-action="addon-details" title="Click for more details">read more...</a>
                                </p>
                            </div>
                            <div class="add-ons-scroll-price">AED <span> {{ $addon['strike_amount'] }} </span>
                                {{ $addon['amount'] }}
                            </div>
                            <div class="add-ons-scroll-btn">

                                <div class="addon-btn-main">
                                    <a class="sp-btn">Add</a>
                                </div>

                                <div class="addon-btn-count">
                                    <input data-addons_id="{{ $addon['service_addons_id'] }}" class="addon-btn-minus" data-action="addon-minus" type="button">
                                    <input id="addons_quanity_{{ $addon['service_addons_id'] }}" type="number" min="0" max="{{ $addon['cart_limit'] }}" data-addons_id="{{ $addon['service_addons_id'] }}" name="addons_quanity[]" value="0" class="addon-text-field no-arrow" readonly>
                                    <input data-addons_id="{{ $addon['service_addons_id'] }}" data-action="addon-plus" class="addon-btn-plus" type="button">
                                </div>

                            </div>
                        </div>
                    </label>
                </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="col-sm-12 booking-main-btn-section">
        <div class="row m-0">
            <div class="col-sm-6 col-6 mob-total-left">
                <div class="row total-price m-0">
                    <div class="col-12 book-det-left p-0">
                        <p class="p-0">Total</p>
                    </div>
                    <div class="col-12 book-det-right p-0">
                        <p class="p-0"><span>AED</span>
                            <calc-amount class="total_payable">00.00</calc-amount>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6 col-6 booking-main-btn">
                <button class="text-field-btn" type="button" data-action="next-step" data-step="2">Next</button>
            </div>
        </div>
    </div>
</div>