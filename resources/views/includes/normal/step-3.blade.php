<div class="col-lg-8 col-md-12 booking-form-left step-3" style="display: none">
    <div class="col-sm-12">
        <div class="d-flex">
            <div class="step-back-icon"><a href="#" onclick="return false;" class="back-arrow" title="Click to Back" data-action="prev-step" data-step="3"></a></div>
            <div class="booking-page-title flex-grow-1">
                <h3>Checkout</h3>
            </div>
            <div class="booking-steps">Step 3 of 3</div>
        </div>
    </div>
    <div class="col-sm-12 payment-method-wrapper">
        <div class="col-sm-12 p-0 pb-2">
            <h4>Payment method</h4>
        </div>
        <div class="col-sm-12 booking-form-list payment-method p-0">
            <ul id="payment-method-holder">
                @foreach ($payment_types as $key => $payment_type)
                <li class="{{ $payment_type['code'] }}-opt pay-mode-li-{{ $payment_type['id'] }}">
                    <input id="payment_type_{{ $key }}" value="{{ $payment_type['id'] }}" name="payment_method" class="" type="radio">
                    <label for="payment_type_{{ $key }}">
                        <img src="{{ asset('images/payment-'. $payment_type['code'].'.jpg') }}" alt="{{$payment_type['code']}}">
                    </label>
                </li>
                @endforeach
            </ul>
        </div>
    </div>
    <div class="col-sm-12 booking-alert-main booking-address-alert after-login" style="display: none;">
        <div class="d-flex booking-alert">
            <div class="booking-alert-icon"><i class="fa fa-map-marker"></i></div>
            <div class="booking-alert-cont flex-grow-1">
                <p><strong class="booking-address-type">Address</strong><br /><span class="booking-address-text">
                    </span></p>
            </div>
            <div class="booking-alert-btn" data-action="address-pick-popup">Change</div>
        </div>
    </div>
    <div class="col-sm-12">
        <div class="row book-details-main-set m-0 pt-5">
            <div class="col-sm-12 p-0 pb-3">
                <h3>Payment Summary</h3>
            </div>
            <div class="col-sm-12 p-0 pb-2">
                <div class="row booking-amount m-0">
                    <div class="col-7 book-det-left ps-0 pe-0">
                        <p>Sub Total</p>
                    </div>
                    <div class="col-5 book-det-right ps-0 pe-0">
                        <p><span>AED</span> <calc-amount class="taxable_amount">00.00</calc-amount></p>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 p-0 pb-2">
                <div class="row booking-amount m-0">
                    <div class="col-7 book-det-left ps-0 pe-0">
                        <p>Total (inc VAT <calc-amount class="tax_percentage">0.0</calc-amount>%)</p>
                    </div>
                    <div class="col-5 book-det-right ps-0 pe-0">
                        <p><span>AED</span> <calc-amount class="taxed_amount">00.00</calc-amount></p>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 p-0 pb-2 payment_type_charge">
                <div class="row booking-amount m-0">
                    <div class="col-7 book-det-left ps-0 pe-0">
                        <p>Convenience Fee</p>
                    </div>
                    <div class="col-5 book-det-right ps-0 pe-0">
                        <p><span>AED</span> <calc-amount class="payment_type_charge">00.00</calc-amount></p>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 p-0 pb-4">
                <div class="row total-price m-0">
                    <div class="col-7 book-det-left ps-0 pe-0">
                        <p>Total Payable</p>
                    </div>
                    <div class="col-5 book-det-right ps-0 pe-0">
                        <p><span>AED</span> <calc-amount class="total_payable">00.00</calc-amount></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 booking-main-btn-section">
            <div class="row m-0">
                <div class="col-sm-6 col-6 mob-total-left">
                    <div class="row total-price m-0">
                        <div class="col-12 book-det-left p-0">
                            <p class="p-0">Total</p>
                        </div>
                        <div class="col-12 book-det-right p-0">
                            <p class="p-0"><span>AED</span>
                                <calc-amount class="total_payable">00.00</calc-amount>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 col-6 booking-main-btn" id="submit-btn-pay-mode-1">
                    <button class="text-field-btn" type="submit">Complete</button>
                </div>
                <div class="col-lg-4 col-sm-6 col-6 booking-main-btn" id="submit-btn-pay-mode-2" style="display: none">
                    <button class="text-field-btn" type="submit">Pay Now</button>
                </div>
                <div class="col-lg-4 col-sm-6 col-6 booking-main-btn" id="submit-btn-pay-mode-3" style="display: none">
                    <button type="submit" id="applePaybtn"></button>
                </div>
            </div>
        </div>
    </div>
</div>