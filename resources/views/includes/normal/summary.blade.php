<div class="col-lg-4 col-md-5 booking-summary-section" id="booking-summary">
    <div class="col-sm-12 book-details-main mob-booking-title">
        <h3>Booking Summary</h3>
    </div>
    <div class="col-lg-11 col-md-12 booking-summary-main clearfix scroll">
        <div class="row m-0">
            <div class="col-sm-12 book-details-main mob-summary-title pb-2">
                <h3>Booking Summary</h3>
            </div>

            <div class="col-sm-12 book-details-main">
                <div class="row m-0">
                    <div class="col-6 book-det-left ps-0 pe-0">
                        <p>Service</p>
                    </div>
                    <div class="col-6 book-det-right ps-0 pe-0">
                        <p>{{ $service_type['service_type_name'] }}</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 book-details-main">
                <div class="row m-0">
                    <div class="col-6 book-det-left ps-0 pe-0">
                        <p>Frequency</p>
                    </div>
                    <div class="col-6 book-det-right ps-0 pe-0">
                        <p class="frequency-name">&nbsp;</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 book-details-main">
                <div class="row m-0">
                    <div class="col-6 book-det-left ps-0 pe-0">
                        <p>Duration</p>
                    </div>
                    <div class="col-6 book-det-right ps-0 pe-0">
                        <p class="duration">&nbsp;</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 book-details-main">
                <div class="row m-0">
                    <div class="col-5 book-det-left ps-0 pe-0">
                        <p>Date & Time</p>
                    </div>
                    <div class="col-7 book-det-right ps-0 pe-0">
                        <p class="date">&nbsp;</p>
                        <p class="time">&nbsp;</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 book-details-main">
                <div class="row m-0">
                    <div class="col-6 book-det-left ps-0 pe-0">
                        <p>Material</p>
                    </div>
                    <div class="col-6 book-det-right ps-0 pe-0">
                        <p class="is_materials_included">&nbsp;</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 book-details-main addons" style="display: none">
                <div class="row m-0">
                    <div class="col-6 book-det-left ps-0 pe-0">
                        <p>Addons</p>
                    </div>
                    <div class="col-6 book-det-right ps-0 pe-0 addons-list">
                    </div>
                </div>
            </div>
            <div class="col-sm-12 book-details-main">
                <div class="row m-0">
                    <div class="col-7 book-det-left ps-0 pe-0">
                        <p>Number of Professionals</p>
                    </div>
                    <div class="col-5 book-det-right ps-0 pe-0">
                        <p class="professionals_count">&nbsp;</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 book-details-main">
                <div class="row m-0">
                    <div class="col-5 book-det-left ps-0 pe-0">
                        <p>Crew</p>
                    </div>
                    <div class="col-7 book-det-right ps-0 pe-0">
                        <p class="crew_names">[ Auto Assign ]</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row book-details-main-set m-0 pt-5">
            <div class="col-sm-12 book-details-main pb-0">
                <h3>Payment Summary</h3>
            </div>
            <div class="col-sm-12 promo-code-section">
                <div class="col-sm-12 promocode-main" data-action="coupon-apply-popup">
                    <div class="d-flex promocode-set show-promocode-popup">
                        <div class="promocode-icon"><img src="images/promo-code.png" alt="" />
                        </div>
                        <div class="promocode-cont flex-grow-1">
                            <p><strong>Promo Code</strong><br /><span class="promo-code-message">No voucher applied.</span></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 book-details-main">
                <div class="row booking-amount m-0">
                    <div class="col-6 book-det-left ps-0 pe-0">
                        <p>Service Fee</p>
                    </div>
                    <div class="col-6 book-det-right ps-0 pe-0">
                        <p><span>AED</span>
                            <calc-amount class="service_fee">00.00</calc-amount>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 book-details-main cleaning_material_fee" style="display: none">
                <div class="row booking-amount m-0">
                    <div class="col-6 book-det-left ps-0 pe-0">
                        <p>Material Fee</p>
                    </div>
                    <div class="col-6 book-det-right ps-0 pe-0">
                        <p><span>AED</span>
                            <calc-amount class="cleaning_material_fee">00.00</calc-amount>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 book-details-main rush_slot_charge" style="display: none">
                <div class="row booking-amount m-0">
                    <div class="col-6 book-det-left ps-0 pe-0">
                        <p>Extra Slot Fee</p>
                    </div>
                    <div class="col-6 book-det-right ps-0 pe-0">
                        <p><span>AED</span>
                            <calc-amount class="rush_slot_charge">00.00</calc-amount>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 book-details-main addons_amount" style="display: none">
                <div class="row booking-amount m-0">
                    <div class="col-6 book-det-left ps-0 pe-0">
                        <p>Addons</p>
                    </div>
                    <div class="col-6 book-det-right ps-0 pe-0">
                        <p><span>AED</span>
                            <calc-amount class="addons_amount">00.00</calc-amount>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 book-details-main discount_total" style="display: none">
                <div class="row booking-amount m-0">
                    <div class="col-6 book-det-left ps-0 pe-0">
                        <p>Discount</p>
                    </div>
                    <div class="col-6 book-det-right ps-0 pe-0">
                        <p><span>AED</span> <calc-amount class="discount_total">00.00</calc-amount></p>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 book-details-main">
                <div class="row booking-amount m-0">
                    <div class="col-6 book-det-left ps-0 pe-0">
                        <p>Taxable Amount</p>
                    </div>
                    <div class="col-6 book-det-right ps-0 pe-0">
                        <p><span>AED</span> <calc-amount class="taxable_amount">00.00</calc-amount></p>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 book-details-main">
                <div class="row booking-amount m-0">
                    <div class="col-6 book-det-left ps-0 pe-0">
                        <p>Total (inc VAT <calc-amount class="tax_percentage">0.0</calc-amount>%)</p>
                    </div>
                    <div class="col-6 book-det-right ps-0 pe-0">
                        <p><span>AED</span> <calc-amount class="taxed_amount">00.00</calc-amount></p>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 book-details-main payment_type_charge" style="display: none">
                <div class="row booking-amount m-0">
                    <div class="col-6 book-det-left ps-0 pe-0">
                        <p>Convenience Fee</p>
                    </div>
                    <div class="col-6 book-det-right ps-0 pe-0">
                        <p><span>AED</span> <calc-amount class="payment_type_charge">00.00</calc-amount></p>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 book-details-main">
                <div class="row total-price m-0">
                    <div class="col-5 book-det-left ps-0 pe-0">
                        <p>Total</p>
                    </div>
                    <div class="col-7 book-det-right ps-0 pe-0">
                        <p><span>AED</span> <calc-amount class="total_payable">00.00</calc-amount></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>