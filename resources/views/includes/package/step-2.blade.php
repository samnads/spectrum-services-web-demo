<div class="col-lg-8 col-md-12 booking-form-left step-2" style="display: none">
    <div class="col-sm-12">
        <div class="d-flex">
            <div class="step-back-icon"><a href="#" onclick="return false;" class="back-arrow" title="Click to Back"
                    data-action="prev-step" data-step="2"></a></div>
            <div class="booking-page-title flex-grow-1">
                <h3>Date & Time</h3>
            </div>
            <div class="booking-steps">Step 2 of 3</div>
        </div>
    </div>
    <div class="col-sm-12 calender-wrapper pt-4">
        <div class="col-sm-12 p-0">
            <h4>When would you like your service?</h4>
        </div>
        <div class="col-sm-12 calendar-main p-0">
            <div id="calendar" class="owl-carousel owl-theme p-0">
                @foreach ($available_datetime['available_dates'] as $key => $date)
                    <div class="item">
                        <input id="booking_date_{{ $key }}"
                            value="{{ Carbon\Carbon::createFromFormat('Y-m-d', $date)->format('d/m/Y') }}"
                            name="date" class="" type="radio" {{ $key == 0 ? 'checked' : '' }}>
                        <label for="booking_date_{{ $key }}">
                            <div class="calendar-tmb-main">
                                <div class="calendar-tmb-day">
                                    {{ Carbon\Carbon::createFromFormat('Y-m-d', $date)->format('D') }}</div>
                                <div class="calendar-tmb-date">
                                    {{ Carbon\Carbon::createFromFormat('Y-m-d', $date)->format('d') }}</div>
                                <div class="calendar-tmb-month">
                                    {{ Carbon\Carbon::createFromFormat('Y-m-d', $date)->format('M') }}</div>
                            </div>
                        </label>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="col-sm-12 what-time-wrapper">
        <div class="col-sm-12 p-0 pb-2">
            <h4>What time would you like us to start?<!--<label class="what-time-btn">See All</label>--></h4>
        </div>
        <div class="col-sm-12 booking-form-list p-0">
            <ul id="booking-times-holder">
            </ul>
        </div>
    </div>
    <div class="col-sm-12 cleaning-materials-wrapper">
        <div class="col-sm-12 p-0 pb-2">
            <h4>Do you have any specific cleaning instruction?</h4>
        </div>
        <div class="col-sm-12 booking-form-field p-0">
            <textarea name="instructions" cols="" rows="" class="text-field-big" spellcheck="false"
                placeholder="Write here..."></textarea>
        </div>
    </div>
    <div class="col-sm-12 booking-main-btn-section">
        <div class="row m-0">
            <div class="col-sm-6 col-6 mob-total-left">
                <div class="row total-price m-0">
                    <div class="col-12 book-det-left p-0">
                        <p class="p-0">Total</p>
                    </div>
                    <div class="col-12 book-det-right p-0">
                        <p class="p-0"><span>AED</span>
                            <calc-amount class="total_payable">00.00</calc-amount>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6 col-6 booking-main-btn">
                <button class="text-field-btn" type="button" data-action="next-step" data-step="2">Next</button>
            </div>
        </div>
    </div>
</div>
