<div class="col-lg-8 col-md-12 booking-form-left step-1">
    <div class="col-sm-12 ">
        <div class="d-flex">
            <div class="step-back-icon"><a href="{{ url('') }}" class="back-arrow" title="Click to Back"></a></div>
            <div class="booking-page-title flex-grow-1">
                <h3>{{ $service_type['service_type_name'] }}</h3>
            </div>
            <div class="booking-steps">Step 1 of 3</div>
        </div>
    </div>
    <div class="col-sm-12 booking-packages-nav-main">
        <div class="col-sm-12 booking-packages-nav sticker">
            <div class="booking-packages-categoty-scroll">
                <div id="package-category" class="owl-carousel owl-theme p-0">
                    @foreach ($packages_grouped as $key => $package)
                        <div class="item {{ $key === array_key_first($packages_grouped) ? 'selected' : '' }}"><a
                                href="#{{ strtolower($key) }}">{{ $key }}</a></div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-12 booking-packages-cont-main pb-5">
        @foreach ($packages_grouped as $group => $packages)
            <div class="col-sm-12 booking-packages-category">
                <div class="col-sm-12 booking-packages-category-title pb-3">
                    <div id="{{ strtolower($group) }}" class="booking-category-nav-position">&nbsp;</div>
                    <h3>{{ $group }}</h3>
                </div>

                <div class="col-sm-12">
                    @foreach ($packages as $key => $package)
                        <div class="col-sm-12 booking-packages">
                            <input value="{{ json_encode($package) }}" name="package[]" type="hidden">
                            <input id="package-{{ $package['package_id'] }}" value="{{ $package['package_id'] }}"
                                name="packages[]" class="" type="checkbox">
                            <label for="package-{{ $package['package_id'] }}" class="w-100">
                                <div class="col-sm-12 booking-packages-content-main d-flex">
                                    <div class="booking-packages-cont v-center flex-grow-1">
                                        <h4>{{ $package['package_name'] }}</h4>
                                        <p>{{ substr($package['package_description'], 0, 100) }} <a href="javascript:void(0);" data-action="package-details" title="Click for more details">read more...</a></p>
                                    </div>
                                    <div class="booking-packages-price v-center">
                                        <label>AED <span>{{ $package['actual_amount'] }}</span>
                                            {{ $package['amount'] }}</label>
                                    </div>
                                    <div class="booking-packages-btn v-center">
                                        <div class="addon-btn-main"> <a class="sp-btn">Add</a> </div>
                                        <div class="addon-btn-count">
                                            <input data-package_id="{{ $package['package_id'] }}"
                                                class="addon-btn-minus" type="button" data-action="package-minus">
                                            <input data-package_id="{{ $package['package_id'] }}"
                                                id="package_quanity_{{ $package['package_id'] }}" min="0"
                                                max="5" name="package_quanity[]" value="0" type="number"
                                                class="addon-text-field no-arrow" readonly>
                                            <input data-package_id="{{ $package['package_id'] }}"
                                                class="addon-btn-plus" type="button" data-action="package-plus">
                                        </div>
                                    </div>
                                </div>
                            </label>
                        </div>
                    @endforeach
                </div>
            </div>
        @endforeach
    </div>
    <div class="col-sm-12 booking-main-btn-section">
        <div class="row m-0">
            <div class="col-sm-6 col-6 mob-total-left">
                <div class="row total-price m-0">
                    <div class="col-12 book-det-left p-0">
                        <p class="p-0">Total</p>
                    </div>
                    <div class="col-12 book-det-right p-0">
                        <p class="p-0"><span>AED</span>
                            <calc-amount class="total_payable">00.00</calc-amount>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6 col-6 booking-main-btn">
                <button class="text-field-btn" type="button" data-action="next-step" data-step="1">Next</button>
            </div>
        </div>
    </div>
</div>
