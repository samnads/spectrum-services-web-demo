<header>
    <div class="container p-0">
        <div class="row m-0">
            <div class="col-lg-3 col-md-12 logo-main">
                <div class="logo"><a href="{{ url('') }}" title="Click to Home"><img
                            src="{{ asset('images/logo.png') }}" alt="" /></a></div>

                <div class="mobile-icon user-btn"><i class="fa fa-user"></i></div>

                <div class="mobile-dropdown">
                    <ul class="before-login">
                        <li><a href="{{url('')}}">Home</a></li>
                        <li>
                            <a href="javascript:void(0);" data-action="login-popup">Login</a>
                        </li>
                    </ul>
                    <ul class="after-login" style="display: none">
                        <li><a href="{{url('')}}">Home</a></li>
                        <li>
                            <a href="#" data-action="my-account">My Account</a>
                            <ul>
                                <li><a href="#" data-action="logout">Logout</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>



                <div class="mobile-icon">
                    <a href="tel:+971000000000"><i class="fa fa-phone"></i></a>
                </div>

                <div class="clear"></div>

            </div>
            <div class="col-lg-9 col-md-12 menu-section p-0">
                <nav id="primary_nav_wrap">
                    <ul class="before-login">
                        <li><a href="{{url('')}}">Home</a></li>
                        <li>
                            <a href="javascript:void(0);" data-action="login-popup">Login</a>
                        </li>
                    </ul>
                    <ul class="after-login" style="display: none">
                        <li><a href="{{url('')}}">Home</a></li>
                        <li>
                            <a href="#" data-action="my-account">My Account</a>
                            <ul>
                                <li><a href="#" data-action="logout">Logout</a></li>
                            </ul>
                        </li>
                    </ul>
                    <div class="clear"></div>
                </nav>
            </div>
        </div>
    </div>
</header>
