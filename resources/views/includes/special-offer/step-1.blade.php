<div class="col-lg-8 col-md-12 booking-form-left step-1">
    <div class="col-sm-12">
        <div class="d-flex">
            <div class="step-back-icon"><a href="{{ url('') }}" class="back-arrow" title="Click to Back"></a></div>
            <div class="booking-page-title flex-grow-1">
                <h3>Date & Time</h3>
            </div>
            <div class="booking-steps">Step 1 of 3</div>
        </div>
    </div>
    <div class="col-sm-12 booking-alert-main">
        <div class="d-flex booking-alert">
            <div class="booking-alert-icon"><i class="fa fa-refresh"></i></div>
            <div class="booking-alert-cont flex-grow-1">
                <p><strong>Frequency</strong><br /><span class="frequency-name">{{ $special_offer['frequency'] }}</span>
                </p>
            </div>
        </div>
    </div>
    <div class="col-sm-12 calender-wrapper">
        <div class="col-sm-12 p-0">
            <h4>When would you like your service?</h4>
        </div>
        <div class="col-sm-12 calendar-main p-0">
            <div id="calendar" class="owl-carousel owl-theme p-0">
                @foreach ($special_offer['dates'] as $key => $date)
                    <div class="item">
                        <input id="booking_date_{{ $key }}"
                            value="{{ Carbon\Carbon::createFromFormat('Y-m-d', $date)->format('d/m/Y') }}"
                            name="date" class="" type="radio" {{ $key == 0 ? 'checked' : '' }}>
                        <label for="booking_date_{{ $key }}">
                            <div class="calendar-tmb-main">
                                <div class="calendar-tmb-day">
                                    {{ Carbon\Carbon::createFromFormat('Y-m-d', $date)->format('D') }}
                                </div>
                                <div class="calendar-tmb-date">
                                    {{ Carbon\Carbon::createFromFormat('Y-m-d', $date)->format('d') }}
                                </div>
                                <div class="calendar-tmb-month">
                                    {{ Carbon\Carbon::createFromFormat('Y-m-d', $date)->format('M') }}
                                </div>
                            </div>
                        </label>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="col-sm-12 what-time-wrapper">
        <div class="col-sm-12 p-0 pb-2">
            <h4>What time would you like us to start?<!--<label class="what-time-btn">See All</label>--></h4>
        </div>
        <div class="col-sm-12 booking-form-list p-0">
            <ul id="booking-times-holder">
            </ul>
        </div>
    </div>
    <div class="col-sm-12 how-many-hours-wrapper">
        <div class="col-sm-12 p-0 pb-2">
            <h4>How many hours do you need the Housekeeper to stay?</h4>
        </div>
        <div class="col-sm-12 booking-form-list p-0">
            <ul id="hours-count-holder">
                <li><input id="booking_hours_1" value="{{ $special_offer['no_of_hours'] }}" name="hours"
                        class="" type="radio" checked>
                    <label for="booking_hours_1">{{ $special_offer['no_of_hours'] }}</label>
                </li>
            </ul>
        </div>
    </div>
    <div class="col-sm-12 how-mainy-housekeepers-wrapper">
        <div class="col-sm-12 p-0 pb-2">
            <h4>How many Housekeepers do you need?</h4>
        </div>
        <div class="col-sm-12 booking-form-list p-0">
            <ul id="professionals-count-holder">
                <li><input id="professionals_count_1" value="{{ $special_offer['no_of_maids'] }}"
                        name="professionals_count" class="" type="radio" checked>
                    <label for="professionals_count_1">{{ $special_offer['no_of_maids'] }}</label>
                </li>
            </ul>
        </div>
    </div>
    <div class="col-sm-12 which-housekeeper-wrapper">
        <div class="col-sm-12 p-0 pb-2">
            <h4>Which Housekeeper do you prefer?</h4>
        </div>
        <div class="col-sm-12 p-0">
            <div id="housekeepers" class="owl-carousel owl-theme p-0">
                <div class="item">
                    <input id="which-maid1" value="0" name="which-maid" checked="checked" class=""
                        type="checkbox">
                    <label for="which-maid1">
                        <div class="which-housekeeper-thumb">
                            <div class="which-housekeeper-thumb-image"><img src="{{ asset('images/auto-maid.jpg') }}"
                                    alt="" />
                            </div>
                            <div class="which-housekeeper-thumb-name v-center">Auto-assign</div>
                            <div class="which-housekeeper-thumb-auto-assign">We'll assign the best Housekeeper
                            </div>
                        </div>
                    </label>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-12 booking-main-btn-section">
        <div class="row m-0">
            <div class="col-sm-6 col-6 mob-total-left">
                <div class="row total-price m-0">
                    <div class="col-12 book-det-left p-0">
                        <p class="p-0">Total</p>
                    </div>
                    <div class="col-12 book-det-right p-0">
                        <p class="p-0"><span>AED</span>
                            <calc-amount class="total_payable">00.00</calc-amount>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6 col-6 booking-main-btn">
                <button class="text-field-btn" type="button" data-action="next-step" data-step="1">Next</button>
            </div>
        </div>
    </div>
</div>
