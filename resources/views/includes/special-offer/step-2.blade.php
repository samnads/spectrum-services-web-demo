<div class="col-lg-8 col-md-12 booking-form-left step-2" style="display: none">
    <div class="col-sm-12">
        <div class="d-flex">
            <div class="step-back-icon"><a href="#" onclick="return false;" class="back-arrow" title="Click to Back"
                    data-action="prev-step" data-step="2"></a></div>
            <div class="booking-page-title flex-grow-1">
                <h3>Service Details</h3>
            </div>
            <div class="booking-steps">Step 2 of 3</div>
        </div>
    </div>
    <div class="col-sm-12 booking-alert-main">
        <div class="d-flex booking-alert">
            <div class="booking-alert-icon"><i class="fa fa-refresh"></i></div>
            <div class="booking-alert-cont flex-grow-1">
                <p><strong>Frequency</strong><br /><span class="frequency-name">{{ $special_offer['frequency'] }}</span>
                </p>
            </div>
        </div>
    </div>
    <div class="col-sm-12 pb-4 no-of-weeks" style="display: none">
        <div class="col-sm-12 p-0 pb-2">
            <h4>No of weeks</h4>
        </div>
        <div class="col-sm-12 booking-form-field p-0">
            <input name="No_weeks" class="text-field no-arrow" type="number" min="1" />
        </div>
    </div>
    <div class="col-sm-12 cleaning-materials-wrapper">
        <div class="col-sm-12 p-0 pb-2">
            <h4>Do you require cleaning materials?</h4>
        </div>
        <div class="col-sm-12 booking-form-list p-0">
            <ul id="cleaning-materials-holder">
                @if ($special_offer['cleaning_materials'] == 'no')
                    <li><input id="cleaning_materials_0" value="no" name="cleaning_materials" class=""
                            type="radio" {{ $special_offer['cleaning_materials'] == 'no' ? 'checked' : '' }} disabled>
                        <label for="cleaning_materials_0">No, I have them</label>
                    </li>
                @else
                    <li><input id="cleaning_materials_1" value="yes" name="cleaning_materials" class=""
                            type="radio" {{ $special_offer['cleaning_materials'] == 'yes' ? 'checked' : '' }}
                            disabled>
                        <label for="cleaning_materials_1">Yes, Please</label>
                    </li>
                @endif
            </ul>
        </div>
    </div>
    <div class="col-sm-12 cleaning-materials-wrapper">
        <div class="col-sm-12 p-0 pb-2">
            <h4>Do you have any specific cleaning instruction?</h4>
        </div>
        <div class="col-sm-12 booking-form-field p-0">
            <textarea name="instructions" cols="" rows="" class="text-field-big" spellcheck="false"
                placeholder="Write here..."></textarea>
        </div>
    </div>
    <div class="col-sm-12 booking-main-btn-section">
        <div class="row m-0">
            <div class="col-sm-6 col-6 mob-total-left">
                <div class="row total-price m-0">
                    <div class="col-12 book-det-left p-0">
                        <p class="p-0">Total</p>
                    </div>
                    <div class="col-12 book-det-right p-0">
                        <p class="p-0"><span>AED</span>
                            <calc-amount class="total_payable">00.00</calc-amount>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6 col-6 booking-main-btn">
                <button class="text-field-btn" type="button" data-action="next-step" data-step="2">Next</button>
            </div>
        </div>
    </div>
</div>
