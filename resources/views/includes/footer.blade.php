@include('popups.login-popup')
@include('popups.otp-popup')
@include('popups.name-popup')
<footer class="footer-wrapper">
    <div class="container">
        <div class="row footer-bottom m-0">
            <div class="col-sm-6 p-0 footer-copy">
                <p>2023 © <span>Spectrum Booking .</span> All rights reserved.</p>
            </div>
            <div class="col-sm-6 p-0 footer-design">
                <div class="designed" style="height: 40px">
                    {{--<a href="http://azinovatechnologies.com/" target="_blank">
                        <div class="azinova-logo"></div>
                    </a>--}}
                    <p class="no-padding">Powered by Spectrum Services</p>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </div>
</footer>
