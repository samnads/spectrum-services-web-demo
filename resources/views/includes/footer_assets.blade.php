<script type="text/javascript">
    var _base_url = "{{ url('') }}/";
    var _api_url = "{{ Config::get('url.api_url') }}";
    var _current_url = "{{ strtok(url()->full(), '?') }}";
    var _id = {{@session('customer_id') ? @session('customer_id') : 'null'}}; // customer id
    var _name = {!!@session('customer_name') ? '"'.session('customer_name').'"' : 'null'!!}; // customer name
    var _email = {!!@session('customer_email') ? '"'.session('customer_email').'"' : 'null'!!}; // customer email
    var _otp_expire_seconds = 60;
</script>
<script type="text/javascript" src="{{ asset('js/jquery-3.7.1.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.validate.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/owl.carousel.js') }}"></script>
<!-- <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script> -->
<script type="text/javascript" src="{{ asset('js/bootstrap4.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/js.cookie.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/moment.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.toast.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/sweetalert2.all.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/main.js?v=') . Config::get('version.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.jscroll.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.fixie.js') }}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB0ztqRwv8zz7IMF7NTCcfF2MIw488DKR4&libraries=places&callback=mapCallback&loading=async"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-locationpicker/0.1.12/locationpicker.jquery.min.js"
        integrity="sha512-KGE6gRUEc5VBc9weo5zMSOAvKAuSAfXN0I/djLFKgomlIUjDCz3b7Q+QDGDUhicHVLaGPX/zwHfDaVXS9Dt4YA=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>