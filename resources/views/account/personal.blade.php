@extends('layouts.main')
@section('title', 'My Account')
@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <section>
        <div class="container">
            <div class="row inner-wrapper m-0">
                <div class="col-sm-12 my-account-wrapper">
                    <div class="col-sm-12">
                        <div class="d-flex">
                            <div class="step-back-icon"><a href="{{ url('account') }}" class="back-arrow"
                                    title="Click to Back"></a></div>
                            <div class="booking-page-title flex-grow-1">
                                <h3>Personal Details</h3>
                            </div>
                        </div>
                    </div>
                    <form id="profile_update">
                        <div class="row m-0 pt-1">
                            <div class="col-sm-12 personal-details-section">
                                <div class="d-flex personal-details-main edit-details">
                                    <div class="pd-icon-main pd-name-icon v-center"><img
                                            src="{{ asset('images/user-icon7.png') }}" alt="" /></div>
                                    <div class="pd-content-main flex-grow-1">
                                        <p>Full Name</p>
                                        <input name="name" id="profileName" placeholder="" class="text-field"
                                            value="{{ session('customer_name') }}" type="text" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 personal-details-section">
                                <div class="d-flex personal-details-main edit-details">
                                    <div class="pd-icon-main pd-name-icon v-center"><img
                                            src="{{ asset('images/user-icon9.png') }}" alt="" /></div>
                                    <div class="pd-content-main flex-grow-1">
                                        <p>Email</p>
                                        <input name="email" id="email" placeholder="" class="text-field"
                                            value="{{ session('customer_email') }}" type="text" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 personal-details-section">
                                <div class="d-flex personal-details-main">
                                    <div class="pd-icon-main pd-phone-icon v-center"><img
                                            src="{{ asset('images/user-icon8.png') }}" alt="" /></div>
                                    <div class="pd-content-main flex-grow-1">
                                        <p>Mobile Number</p>
                                        <input name="mobilenumber" id="profileMobile" placeholder="" class="text-field"
                                            value="{{ session('customer_mobile') }}" type="text" disabled>
                                        <input name="oldmobilenumber" id="oldmobilenumber" type="hidden"
                                            value="{{ session('customer_mobile') }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 booking-main-btn-section p-0 pt-4">
                                <div class="row m-0">
                                    <div class="col-lg-3 col-md-4 col-sm-6 p-0 pt-3">
                                        <button class="text-field-btn" id="profile-update-submit"
                                            type="submit">Update</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
@push('styles')
@endpush
@push('scripts')
    <script>
        $(document).ready(function() {
            /*$('.edit-profile-name-btn').click(function() {
                $("#profileName").attr("readonly", false);
            });
            $('.edit-profile-mobile-btn').click(function() {
                $("#profileMobile").attr("readonly", false);
            });
            $('.edit-profile-email-btn').click(function() {
                $("#profileEmail").attr("readonly", false);
            });*/
            profile_update_form = $('#profile_update');
            profile_update_form_validator = $('#profile_update').validate({
                focusInvalid: false,
                ignore: [],
                rules: {
                    "name": {
                        required: true,
                    },
                    "mobilenumber": {
                        required: true,
                        minlength: 9,
                        maxlength: 9
                    },
                },
                messages: {
                    "name": {
                        required: "Select your name",
                    },
                    "mobilenumber": {
                        required: "Enter mobile number",
                    },
                },
                errorPlacement: function(error, element) {
                    if (element.attr("name") == "name") {
                        error.insertAfter($('input[name="name"]', profile_update_form).parent()
                            .parent());
                    } else {
                        error.insertAfter(element);
                    }
                },
                submitHandler: function(form) {
                    let btn = $('button[type="submit"]', form);
                    btn.html(loading_button_html);
                    $.ajax({
                        type: 'POST',
                        url: _base_url + "api/customer/update_customer_data",
                        dataType: 'json',
                        //data: JSON.stringify($('#login-popup-form').serializeObjectForApi()),
                        data: $('#profile_update').serialize(),
                        //contentType: 'application/json;charset=UTF-8',
                        success: function(response) {
                            btn.html('Update');
                            if (response.result.status == "success") {
                                if (response.result.check_otp == true) {
                                    let oldnum = $("#oldmobilenumber").val();
                                    toast('OTP Send', 'Enter the OTP to continue !',
                                        'info');
                                    // $('.login-popup').hide(500);
                                    $('#login-otp-popup-form input[name="mobilenumber"]')
                                        .val(oldnum);
                                    $('#login-otp-popup-form input[name="oldmobilenumber"]')
                                        .val(response.result.UserDetails.mobile);
                                    $('#login-otp-popup-form input[name="is_update"]').val(
                                        1);
                                    $('.customer-full-mobile').html(response.result
                                        .UserDetails.mobile)
                                    $('#otp-popup').show(500);
                                } else {
                                    toast('Success', response.result.message,
                                        'success');
                                }
                            } else {
                                toast('Error', response.result.message, 'warning');
                            }
                        },
                        error: function(response) {
                            btn.html('Update');
                        },
                    });
                }
            });
        });
    </script>
@endpush
