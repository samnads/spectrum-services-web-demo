@extends('layouts.main')
@section('title', 'My Account')
@section('content')
    <section>
        <div class="container">
            <div class="row inner-wrapper m-0">
                <div class="col-sm-12 my-account-wrapper">
                    <div class="col-sm-12">
                        <h3>My account</h3>
                    </div>
                    <div class="row m-0 pt-5">
                        <div class="col-sm-5 p-0">
                            <div class="col-sm-8 user-details-main">
                                <div class="user-photo">
                                    <!--<img src="{{ session('customer_avatar') }}" class="img-fluid" />-->
                                    <img src="{{ asset('images/avatar.png') }}" class="img-fluid" />
                                    <!--<label class="" title="Edit Photo"><i class="fa fa-pencil"></i></label>-->
                                </div>

                                <div class="user-content">
                                    <h4>{{ session('customer_name') ?: '-' }}</h4>
                                    <p>{{ session('customer_mobile') ?: '-' }}</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-7 p-0">
                            <div class="row user-det-thumb-main m-0 d-flex align-items-stretch">
                                <div class="col-sm-4 user-det-thumb p-0">
                                    <a href="{{ url('account/personal') }}">
                                        <div class="user-det-icon"><img src="{{asset('images/user-icon1.png')}}" alt="" /></div>
                                        Personal Details
                                    </a>
                                </div>
                                <div class="col-sm-4 user-det-thumb p-0">
                                    <a href="{{ url('account/address') }}">
                                        <div class="user-det-icon"><img src="{{asset('images/user-icon2.png')}}" alt="" /></div>
                                        Manage Address
                                    </a>
                                </div>
                                <!--<div class="col-sm-4 user-det-thumb p-0">
             <a class="show-reset-password-popup">
             <div class="user-det-icon"><img src="{{asset('images/user-icon3.png')}}" alt="" /></div>
             Change Password
              </a>
             </div>-->
                                <div class="col-sm-4 user-det-thumb p-0">
                                    <a href="{{url('/bookings/upcoming')}}">
                                        <div class="user-det-icon"><img src="{{asset('images/user-icon4.png')}}" alt="" /></div>
                                        Upcoming Bookings
                                    </a>
                                </div>
                                <div class="col-sm-4 user-det-thumb p-0">
                                    <a href="{{url('/bookings/past')}}">
                                        <div class="user-det-icon"><img src="{{asset('images/user-icon5.png')}}" alt="" /></div>
                                        Past Bookings
                                    </a>
                                </div>
                                <div class="col-sm-4 user-det-thumb p-0">
                                    <a href="{{url('/bookings/cancelled')}}">
                                        <div class="user-det-icon"><img src="{{asset('images/user-icon5.png')}}" alt="" /></div>
                                        Cancelled Bookings
                                    </a>
                                </div>
                                <div class="col-sm-4 user-det-thumb p-0">
                                    <a href="#" data-action="logout">
                                        <div class="user-det-icon"><img src="{{asset('images/user-icon6.png')}}" alt="" /></div>
                                        Logout
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@push('styles')
@endpush
@push('scripts')
@endpush
