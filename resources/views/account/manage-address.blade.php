@extends('layouts.main')
@section('title', 'Manage Address')
@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <section>
        <div class="container">
            <div class="row inner-wrapper m-0">
                <div class="col-sm-12 my-account-wrapper">
                    <div class="col-sm-12">
                        <div class="d-flex">
                            <div class="step-back-icon"><a href="{{ url('account') }}" class="back-arrow"
                                    title="Click to Back"></a></div>
                            <div class="booking-page-title flex-grow-1">
                                <h3>Manage Address</h3>
                            </div>
                            <div class="add-new-address">
                                <div class="addon-btn-main">
                                    <a class="sp-btn show-add-address-popup" data-action="new-address">Add New</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <form id="default_address_form" novalidate="novalidate">
                        <div class="row m-0 pt-3">
                            <fieldset class="p-0 m-0" id="address-list">
                            </fieldset>
                            <div class="col-sm-12 booking-main-btn-section" style="display: none" id="address-def-btn-container">
                                <div class="row m-0 n-address-btn">
                                    <div class="col-lg-3 col-md-4 col-sm-6 booking-main-btn ps-0">
                                        <button class="text-field-btn" type="submit" name="set-default-address">Set Default Address</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
@push('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap-select.css') }}">
@endpush
@push('scripts')
    <script type="text/javascript" src="{{ asset('js/bootstrap-select.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/account.address.js?v=') . Config::get('version.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-locationpicker/0.1.12/locationpicker.jquery.min.js"
        integrity="sha512-KGE6gRUEc5VBc9weo5zMSOAvKAuSAfXN0I/djLFKgomlIUjDCz3b7Q+QDGDUhicHVLaGPX/zwHfDaVXS9Dt4YA=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
@endpush
