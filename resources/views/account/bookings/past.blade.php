@extends('layouts.main')
@section('title', 'Past Bookings')
@section('content')
    <section>
        <div class="container">
            <div class="row inner-wrapper m-0">
                <div class="col-sm-12 my-account-wrapper">
                    <div class="col-sm-12">
                        <div class="d-flex">
                            <div class="step-back-icon"><a href="{{ url('account') }}" class="back-arrow"
                                    title="Click to Back"></a></div>
                            <div class="booking-page-title flex-grow-1">
                                <h3>Past Bookings</h3>
                            </div>
                            <div class="booking-back"><a href="{{ url('bookings/upcoming') }}">Upcoming Bookings</a></div>
                        </div>
                    </div>
                    <div class="row m-0 pt-3">
                        @if (!$booking_history['result']['completed_list'])
                            <div class="co-sm-12">
                                <div class="alert alert-info" role="alert">
                                    <i class="fa fa-info-circle" aria-hidden="true"></i>
                                    {{ $booking_history['result']['message'] }}
                                </div>
                            </div>
                        @endif
                        @foreach ($booking_history['result']['completed_list'] as $key => $booking)
                            <div class="co-sm-12 past-bookings-main">
                                <input type="hidden" value="{{ json_encode($booking) }}" />
                                <div class="d-flex">
                                    <div class="past-bookings-content flex-grow-1">
                                        <div class="col-sm-12 p-0">
                                            <h4>{{ $booking['service'] }}</h4>
                                            <p><i class="fa fa-calendar" aria-hidden="true"></i>
                                                {{ \Carbon\Carbon::createFromFormat('Y-m-d', $booking['date'])->format('d M, Y') }}
                                            </p>
                                            <p><i class="fa fa-clock-o" aria-hidden="true"></i>
                                                {{ \Carbon\Carbon::createFromFormat('H:i', $booking['start_time'])->format('h:i A') }}
                                                -
                                                {{ \Carbon\Carbon::createFromFormat('H:i', $booking['end_time'])->format('h:i A') }}
                                            </p>
                                            <p>{{ $booking['frequency'] }}</p>
                                            <p>AED {{ $booking['total'] }} ({{ $booking['payment_method'] }})</p>
                                        </div>
                                        <div class="col-sm-12 maids-section clearfix p-0">
                                            <div class="pb-maids"><img src="images/maid.jpg" alt="" /></div>
                                            <div class="pb-maids"><img src="images/maid.jpg" alt="" /></div>
                                            <div class="pb-maids"><img src="images/maid.jpg" alt="" /></div>
                                        </div>
                                    </div>
                                    <div class="past-bookings-status v-center">
                                        <h4>{{ $booking['booking_reference'] }}</h4>
                                        <p class="text-right">{{ $booking['status'] }}</p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@push('styles')
@endpush
@push('scripts')
@endpush
