@extends('layouts.main')
@section('title', 'Upcoming Bookings')
@section('content')
    <section>
        <div class="container">
            <div class="row inner-wrapper m-0">
                <div class="col-sm-12 my-account-wrapper">
                    <div class="col-sm-12">
                        <div class="d-flex">
                            <div class="step-back-icon"><a href="{{ url('account') }}" class="back-arrow"
                                    title="Click to Back"></a></div>
                            <div class="booking-page-title flex-grow-1">
                                <h3>Upcoming Bookings</h3>
                            </div>
                            <div class="booking-back"><a href="{{ url('bookings/past') }}">Past Bookings</a></div>
                        </div>
                    </div>
                    <div class="row m-0 pt-3">
                        @if (!$booking_history['result']['completed_list'])
                            <div class="co-sm-12">
                                <div class="alert alert-info" role="alert">
                                    <i class="fa fa-info-circle" aria-hidden="true"></i>
                                    {{ $booking_history['result']['message'] }}
                                </div>
                            </div>
                        @endif
                        @foreach ($booking_history['result']['completed_list'] as $key => $booking)
                            <div class="co-sm-12 past-bookings-main">
                                <input type="hidden" value="{{ json_encode($booking) }}" name="booking[]" />
                                <div class="d-flex n-upcoming-booking-cont">
                                    <div class="past-bookings-content flex-grow-1">
                                        <div class="col-sm-12 p-0">
                                            <h4>{{ $booking['service'] }}</h4>
                                            <p><i class="fa fa-calendar" aria-hidden="true"></i>
                                                {{ \Carbon\Carbon::createFromFormat('Y-m-d', $booking['date'])->format('d M, Y') }}
                                            </p>
                                            <p><i class="fa fa-clock-o" aria-hidden="true"></i>
                                                {{ \Carbon\Carbon::createFromFormat('H:i', $booking['start_time'])->format('h:i A') }}
                                                -
                                                {{ \Carbon\Carbon::createFromFormat('H:i', $booking['end_time'])->format('h:i A') }} &nbsp; &nbsp;

                                                <i class="fa fa-repeat" aria-hidden="true"></i> {{ $booking['frequency'] }}
                                            </p>
                                            <!--<p><i class="fa fa-map-marker" aria-hidden="true"></i> {{ $booking['address'] }}</p>-->
                                            <h5 class="n-price-text bold">AED {{ $booking['total'] }} ({{ $booking['payment_method'] }})</h5>
                                        </div>
                                        <!--<div class="col-sm-12 maids-section clearfix p-0">
                                                    <div class="pb-maids"><img src="#" alt="" /></div>
                                                    <div class="pb-maids"><img src="#" alt="" /></div>
                                                    <div class="pb-maids"><img src="#" alt="" /></div>
                                                </div>-->
                                    </div>
                                    <div class="past-bookings-status v-center">
                                        <h4>{{ $booking['booking_reference'] }}</h4>
                                        <p class="text-right">{{ $booking['status'] }}</p>
                                        @if ($booking['menu']['cancel_this'] === true || $booking['menu']['cancel_all'] === true)
                                            <p class="cancelled text-right"><a
                                                    class="show-cancel-booking-popup js-cancel-booking">Cancel</a></p>
                                        @endif
                                        @if ($booking['menu']['retry_payment'] === true)
                                            <p class="cancelled text-right"><a
                                                    class="show-cancel-booking-popup" data-action="retry-payment">Retry Pyment</a></p>
                                        @endif
                                        @if ($booking['menu']['change_payment_method'] === true)
                                            <p class="cancelled text-right"><a
                                                    class="show-cancel-booking-popup" data-action="change-pay-mode">Change Payment Mode</a></p>
                                        @endif
                                        @if ($booking['menu']['reschedule'] === true)
                                            <p class="cancelled text-right"><a
                                                    class="show-cancel-booking-popup" data-action="reshedule">Reschedule</a></p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        @include('popups.booking-cancel-popup')
        @include('popups.reschedule-popup')
         @include('popups.change-pay-mode-popup')
    </section>
@endsection
@push('styles')
@endpush
@push('scripts')
    <script type="text/javascript" src="{{ asset('js/bookings.js?v=') . Config::get('version.js') }}"></script>
@endpush
