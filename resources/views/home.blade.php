@extends('layouts.main')
@section('title', 'Home')
@section('content')
    <section>
        <div class="container">
            <div class="row banner-wrapper m-0">
                <div id="banner" class="owl-carousel owl-theme p-0">
                    <div class="item position-relative">
                        <img src="{{ asset('images/booking-banner.jpg') }}" class="object-fit_cover pc-view" alt="" />
                        <img src="{{ asset('images/booking-banner-mob.jpg') }}" class="object-fit_cover mob-view"
                            alt="" />
                    </div>

                    <div class="item position-relative">
                        <img src="{{ asset('images/booking-banner.jpg') }}" class="object-fit_cover pc-view"
                            alt="" />
                        <img src="{{ asset('images/booking-banner-mob.jpg') }}" class="object-fit_cover mob-view"
                            alt="" />
                    </div>

                    <div class="item position-relative">
                        <img src="{{ asset('images/booking-banner.jpg') }}" class="object-fit_cover pc-view"
                            alt="" />
                        <img src="{{ asset('images/booking-banner-mob.jpg') }}" class="object-fit_cover mob-view"
                            alt="" />
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="booking-category-wrapper">
        <div class="container">
            <h3>Category</h3>
            <div class="row booking-category-main m-0">
                <input id="service_type_id" type="hidden">
                <div id="category" class="owl-carousel owl-theme p-0">

                    @foreach ($service_types as $service_type)
                        <div class="item">
                            <div class="col-sm-12 booking-cat-thumb-main">
                                <div class="col-12 booking-cat-thumb-image"><img src="{{ $service_type['icon_url'] }}"
                                        alt="{{ $service_type['service_type'] }}" /></div>
                                <div class="col-12 booking-cat-thumb-text">
                                    <h2>{{ $service_type['service_type'] }}</h2>
                                </div>
                                <div class="col-12 booking-cat-thumb-btn"><a href="javascript:void(0);"
                                        class="sp-btn frequency-popup-btn" data-action="frequency-home-entry"
                                        data-service_type_id="{{ $service_type['service_type_id'] }}"
                                        data-service_type_model_id="{{ $service_type['service_type_model_id'] }}"
                                        data-service_type_model="{{ $service_type['service_type_model'] }}"
                                        data-web_url_slug="{{ $service_type['web_url_slug'] }}">Book Now</a></div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
    @if ($special_offers['result']['offerbanner'])
        <section class="special-offers-wrapper">
            <div class="container">
                <h3>Special Offers for you</h3>
                <div class="row m-0">
                    <div id="special-offers" class="owl-carousel owl-theme p-0">
                        @foreach ($special_offers['result']['offerbanner'] as $key => $special_offer)
                            <div class="item position-relative" data-id="{{ $special_offer['offer_id'] }}">
                                <img src="{{ $special_offer['imageurl'] }}" class=""
                                    alt="{{ $special_offer['name'] }}" />
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </section>
    @endif
    <section class="app-download-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 app-download-left v-center">
                    <div>
                        <h2>We are Available on<br />Google Play & App Store</h2>
                        <p>Download the app from Google Play or App Store and start enjoying the benefits right away.</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 app-download-center"><img src="{{ asset('images/app.png') }}"
                        alt="" /></div>
                <div class="col-lg-4 col-md-6 app-download-right v-center">
                    <div class="row">
                        <div class="col-6  app-download-qrcode">
                            <a href="https://play.google.com/store/apps/details?id=com.spectrum.services"
                                target="_blank"><img src="{{ asset('images/android-qrcode.png') }}" alt="" /></a>
                        </div>
                        <div class="col-6 app-download-qrcode">
                            <a href="https://apps.apple.com/us/app/spectrum-services/id1361917863" target="_blank"><img
                                    src="{{ asset('images/iOS-qrcode.png') }}" alt="" /></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('popups.frequency-popup')
@endsection
@push('styles')
@endpush
@push('scripts')
    <script type="text/javascript" src="{{ asset('js/home.js?v=') . Config::get('version.js') }}"></script>
    <script type="text/javascript">
        var _frequencies = @json(frequencies());
        var _service_types = @json(service_types());
    </script>
@endpush
