 <!DOCTYPE html
     PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
 <html xmlns="http://www.w3.org/1999/xhtml">

 <head>
     <title>Spectrum - @yield('title', 'Default')</title>
     <!-- Required meta tags -->
     <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
     <meta name="theme-color" content="#0c3995">
     <meta name="csrf_token" content="{{ csrf_token() }}" />
     <meta name="description" content="@yield('meta_description', 'Spectrum Booking')" />
     <meta name='keywords' content=''>
     <meta name="author" content="Unknown">
     <meta name='copyright' content='Spectrum Services'>
     <meta name='language' content='EN'>
     <meta name='robots' content='index,follow'>
     <meta name='revisit-after' content='7 days'>
     <meta name='rating' content='General'>
     <meta name='distribution' content='Global'>
     @include('includes.header_assets')
     <!-- Page Specific Styles START -->
     @stack('styles')
     <!-- Page Specific Styles END -->
 </head>

 <body class="{{ @$body_css_class }}">
    <div class="loading" id="loader"></div>
     @if (strpos($_SERVER['REQUEST_URI'], '-demo/') !== false ||
             strpos($_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'], '127.0.0.1') !== false)
         <div class="demo-ribbon">
             <span>DEMO</span>
         </div>
     @endif
     <div class="wrapper-main">
         @include('includes.header')
         @yield('content')
         @include('includes.footer')
         @include('includes.footer_assets')
         <!-- Page Specific Scripts START -->
         @stack('scripts')
         <!-- Page Specific Scripts END -->
     </div>
 </body>

 </html>
