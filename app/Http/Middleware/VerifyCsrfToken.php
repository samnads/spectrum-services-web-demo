<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array<int, string>
     */
    protected $except = [
        'payment/payfort/processing',
        'payment/payfort/verify',
        'payment/payfort/online-payment/process',
        'webhook/payfort/transaction-feedback',
        'webhook/payfort/notification',
        'booking/payment/payfort/process/*'
    ];
}
