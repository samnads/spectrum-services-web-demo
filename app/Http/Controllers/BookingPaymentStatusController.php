<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class BookingPaymentStatusController extends Controller
{
    public function success(Request $request, $reference_id)
    {
        $params = $request->all();
        foreach ($params as $key => $value) {
            $data['params'][$key] = $value;
        }
        $data['params']['id'] = session('customer_id');
        $data['params']['token'] = session('customer_token');
        $data['params']['reference_id'] = $reference_id;
        $response = customerApiCall('booking_details_by_ref', @$data ?: [], 'POST');
        $response = $response['result']['data'];
        foreach ($response['bookings'] as $key => $booking) {
            @$response['amount']['service_fee'] += $booking['service_charge'];
            @$response['amount']['vat_amount'] += $booking['vat_charge'];
            @$response['amount']['total_payable'] += $booking['total'];
        }
        @$response['amount']['payment_type_charge'] = $response['bookings'][0]['payment_type_charge'];
        if ($response['bookings'][0]['payment_type_id'] == 1) {
            $response['booking_status'] = 'Pending Approval';
            $response['payment_status'] = 'Not Paid';
        } else {
            $response['booking_status'] = 'Waiting';
            $response['payment_status'] = 'Paid';
        }
        $response['data'] = customerApiCall('data', [], 'GET');
        if (isset($request['test'])) {
            dd($response);
        }
        return view('booking.success', $response);
    }
    public function failed(Request $request, $reference_id)
    {
        $params = $request->all();
        foreach ($params as $key => $value) {
            $data['params'][$key] = $value;
        }
        $data['params']['id'] = session('customer_id');
        $data['params']['token'] = session('customer_token');
        $data['params']['reference_id'] = $reference_id;
        $response = customerApiCall('booking_details_by_ref', @$data ?: [], 'POST');
        //dd(session('customer_id'));
        $response = $response['result']['data'];
        foreach ($response['bookings'] as $key => $booking) {
            @$response['amount']['service_fee'] += $booking['service_charge'];
            @$response['amount']['vat_amount'] += $booking['vat_charge'];
            @$response['amount']['total_payable'] += $booking['total'];
        }
        @$response['amount']['payment_type_charge'] = $response['bookings'][0]['payment_type_charge'];
        if ($response['bookings'][0]['payment_type_id'] == 2) {
            $response['booking_status'] = 'Payment Pending';
            $response['payment_status'] = 'Failed';
        }
        else {
            $response['booking_status'] = 'Unknown';
            $response['payment_status'] = 'Unknown';
        }
        $response['data'] = customerApiCall('data', [], 'GET');
        if (isset($request['test'])) {
            dd($response);
        }
        return view('booking.failed', $response);
    }
}
