<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class PayfortPaymentController extends Controller
{
    public function booking_payment_payfort_process(Request $request, $booking_id)
    {
        $client = new \GuzzleHttp\Client([
            'verify' => false,
        ]);
        $response = $client->request('POST', Config::get('url.api_url') . 'customer/booking/payment/payfort/process/' . $booking_id, [
            'headers' => [],
            'form_params' => $request->all(),
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        $reference_id = $responseBody['result']['booking']['reference_id'];
        //dd($responseBody);
        if ($responseBody['result']['status'] == 'success') {
            return redirect('booking/success/'. $reference_id);
        } else {
            return redirect('booking/failed/' . $reference_id);
        }
    }
}
