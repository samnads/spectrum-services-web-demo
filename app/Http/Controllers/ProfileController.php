<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class ProfileController extends Controller
{
    public function account(Request $request)
    {
        $data['data'] = customerApiCall('data', [], 'GET');
        return view('account.account', $data);
    }
    public function account_personal(Request $request)
    {
        $data['data'] = customerApiCall('data', [], 'GET');
        return view('account.personal', $data);
    }
    public function account_address(Request $request)
    {
        $data['data'] = customerApiCall('data', [], 'GET');
        //dd($data);
        return view('account.manage-address', $data);
    }
    public function upcoming_bookings(Request $request)
    {
        $params['params']['booking_type'] = 'waiting';
        $data['booking_history'] = customerApiCall('booking_history', $params, 'GET');
        $data['data'] = customerApiCall('data', [], 'GET');
        $data['cancel_reasons'] = customerApiCall('cancel_reasons', [], 'GET');
        //dd($data);
        return view('account.bookings.upcoming', $data);
    }
    public function past_bookings(Request $request)
    {
        $params['params']['booking_type'] = 'done';
        $data['booking_history'] = customerApiCall('booking_history', $params, 'GET');
        $data['data'] = customerApiCall('data', [], 'GET');
        //dd($data);
        return view('account.bookings.past', $data);
    }
    public function cancelled_bookings(Request $request)
    {
        $params['params']['booking_type'] = 'cancelled';
        $data['booking_history'] = customerApiCall('booking_history', $params, 'GET');
        $data['data'] = customerApiCall('data', [], 'GET');
        //dd($data);
        return view('account.bookings.cancelled', $data);
    }
}
