<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Session;

class LoginController extends Controller
{
    public function customer_login(Request $request)
    {
        $params = $request->all();
        foreach ($params as $key => $value) {
            $data['params'][$key] = $value;
        }
        return customerApiCall('customer_login', $data);
    }
    public function check_otp(Request $request)
    {
        $params = $request->all();
        foreach ($params as $key => $value) {
            $data['params'][$key] = $value;
        }
        $responseBody = customerApiCall('check_otp', $data);
        if ($responseBody['result']['status'] == 'success') {
            $session_data = [
                'customer_id' => $responseBody['result']['UserDetails']['id'],
                'customer_name' => $responseBody['result']['UserDetails']['UserName'],
                'customer_token' => $responseBody['result']['UserDetails']['token'],
                'customer_avatar' => $responseBody['result']['UserDetails']['image'],
                'customer_mobile' => $responseBody['result']['UserDetails']['mobile'],
                'customer_email_address' => $responseBody['result']['UserDetails']['email_address'],
                'customer_default_address_id' => $responseBody['result']['UserDetails']['default_address_id'],
            ];
            session($session_data);
        }
        return $responseBody;
    }
    public function customer_logout(Request $request)
    {
        $params = $request->all();
        foreach ($params as $key => $value) {
            $data['params'][$key] = $value;
        }
        $request->session()->flush();
        return customerApiCall('customer_logout', @$data, 'POST');
    }
}
