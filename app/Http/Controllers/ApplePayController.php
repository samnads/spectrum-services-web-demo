<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\Customer;
use App\Models\CustomerAddress;
use App\Models\Frequencies;
use App\Models\CustomerPayments;
use App\Models\OnlinePayment;
use Carbon\Carbon;
use Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Mail;
use Response;
use stdClass;

class ApplePayController extends Controller
{
    public function payfort_apple_test(Request $request)
    {
        $url = 'https://sbpaymentservices.payfort.com/FortAPI/paymentApi';
        /****************************************************************************** */
        $js_string = '{
            "shippingContact": {
                "addressLines": [
                "UAE",
                "12 A"
                ],
                "administrativeArea": "",
                "country": "United Arab Emirates",
                "countryCode": "AE",
                "familyName": "N",
                "givenName": "nayana",
                "locality": "Uae",
                "postalCode": "",
                "subAdministrativeArea": "",
                "subLocality": "Uae"
            },
            "token": {
                "paymentData": {
                "data": "Ya1BCB2uWAGrxrFae+2Q+4jDTtQvDyJH//+J/B93JnKEnhC1IUuYoyNFHgOgBbsMt+XSR82JOby2P50gKe50DBkVAQG3WDM9PQUMTg+oliFNxMV/ro62uPH5yyXpezF9o/B/7LRYkif8nIHPqp6UDHLBBzReJJbD6HL5icoQr+9F6rARhF13qMwPmwwkTgySJKn5mahAbl9iLkp6MrCdWfRXvekymL0QXQz+Ye7MlVcDIu8c9IHVEmAcWVL5twPpZIhdmEkypwcAmEWlNMp7Leq/MVGpDfhOHE4NP1e8BAy0tIl5v/DrCy+oUagD1muCwUHuj/ilK2/7tydLPUHHum6knfDG6l/230Z6pIenHbk+0ESHBT5oYx45Va8YSU1LIoCBj86QIsJD952GeDlJkYYZdPvIcNqEPaEsrNWYGw==",
                "signature": "MIAGCSqGSIb3DQEHAqCAMIACAQExDTALBglghkgBZQMEAgEwgAYJKoZIhvcNAQcBAACggDCCA+MwggOIoAMCAQICCEwwQUlRnVQ2MAoGCCqGSM49BAMCMHoxLjAsBgNVBAMMJUFwcGxlIEFwcGxpY2F0aW9uIEludGVncmF0aW9uIENBIC0gRzMxJjAkBgNVBAsMHUFwcGxlIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MRMwEQYDVQQKDApBcHBsZSBJbmMuMQswCQYDVQQGEwJVUzAeFw0xOTA1MTgwMTMyNTdaFw0yNDA1MTYwMTMyNTdaMF8xJTAjBgNVBAMMHGVjYy1zbXAtYnJva2VyLXNpZ25fVUM0LVBST0QxFDASBgNVBAsMC2lPUyBTeXN0ZW1zMRMwEQYDVQQKDApBcHBsZSBJbmMuMQswCQYDVQQGEwJVUzBZMBMGByqGSM49AgEGCCqGSM49AwEHA0IABMIVd+3r1seyIY9o3XCQoSGNx7C9bywoPYRgldlK9KVBG4NCDtgR80B+gzMfHFTD9+syINa61dTv9JKJiT58DxOjggIRMIICDTAMBgNVHRMBAf8EAjAAMB8GA1UdIwQYMBaAFCPyScRPk+TvJ+bE9ihsP6K7/S5LMEUGCCsGAQUFBwEBBDkwNzA1BggrBgEFBQcwAYYpaHR0cDovL29jc3AuYXBwbGUuY29tL29jc3AwNC1hcHBsZWFpY2EzMDIwggEdBgNVHSAEggEUMIIBEDCCAQwGCSqGSIb3Y2QFATCB/jCBwwYIKwYBBQUHAgIwgbYMgbNSZWxpYW5jZSBvbiB0aGlzIGNlcnRpZmljYXRlIGJ5IGFueSBwYXJ0eSBhc3N1bWVzIGFjY2VwdGFuY2Ugb2YgdGhlIHRoZW4gYXBwbGljYWJsZSBzdGFuZGFyZCB0ZXJtcyBhbmQgY29uZGl0aW9ucyBvZiB1c2UsIGNlcnRpZmljYXRlIHBvbGljeSBhbmQgY2VydGlmaWNhdGlvbiBwcmFjdGljZSBzdGF0ZW1lbnRzLjA2BggrBgEFBQcCARYqaHR0cDovL3d3dy5hcHBsZS5jb20vY2VydGlmaWNhdGVhdXRob3JpdHkvMDQGA1UdHwQtMCswKaAnoCWGI2h0dHA6Ly9jcmwuYXBwbGUuY29tL2FwcGxlYWljYTMuY3JsMB0GA1UdDgQWBBSUV9tv1XSBhomJdi9+V4UH55tYJDAOBgNVHQ8BAf8EBAMCB4AwDwYJKoZIhvdjZAYdBAIFADAKBggqhkjOPQQDAgNJADBGAiEAvglXH+ceHnNbVeWvrLTHL+tEXzAYUiLHJRACth69b1UCIQDRizUKXdbdbrF0YDWxHrLOh8+j5q9svYOAiQ3ILN2qYzCCAu4wggJ1oAMCAQICCEltL786mNqXMAoGCCqGSM49BAMCMGcxGzAZBgNVBAMMEkFwcGxlIFJvb3QgQ0EgLSBHMzEmMCQGA1UECwwdQXBwbGUgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkxEzARBgNVBAoMCkFwcGxlIEluYy4xCzAJBgNVBAYTAlVTMB4XDTE0MDUwNjIzNDYzMFoXDTI5MDUwNjIzNDYzMFowejEuMCwGA1UEAwwlQXBwbGUgQXBwbGljYXRpb24gSW50ZWdyYXRpb24gQ0EgLSBHMzEmMCQGA1UECwwdQXBwbGUgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkxEzARBgNVBAoMCkFwcGxlIEluYy4xCzAJBgNVBAYTAlVTMFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAE8BcRhBnXZIXVGl4lgQd26ICi7957rk3gjfxLk+EzVtVmWzWuItCXdg0iTnu6CP12F86Iy3a7ZnC+yOgphP9URaOB9zCB9DBGBggrBgEFBQcBAQQ6MDgwNgYIKwYBBQUHMAGGKmh0dHA6Ly9vY3NwLmFwcGxlLmNvbS9vY3NwMDQtYXBwbGVyb290Y2FnMzAdBgNVHQ4EFgQUI/JJxE+T5O8n5sT2KGw/orv9LkswDwYDVR0TAQH/BAUwAwEB/zAfBgNVHSMEGDAWgBS7sN6hWDOImqSKmd6+veuv2sskqzA3BgNVHR8EMDAuMCygKqAohiZodHRwOi8vY3JsLmFwcGxlLmNvbS9hcHBsZXJvb3RjYWczLmNybDAOBgNVHQ8BAf8EBAMCAQYwEAYKKoZIhvdjZAYCDgQCBQAwCgYIKoZIzj0EAwIDZwAwZAIwOs9yg1EWmbGG+zXDVspiv/QX7dkPdU2ijr7xnIFeQreJ+Jj3m1mfmNVBDY+d6cL+AjAyLdVEIbCjBXdsXfM4O5Bn/Rd8LCFtlk/GcmmCEm9U+Hp9G5nLmwmJIWEGmQ8Jkh0AADGCAYgwggGEAgEBMIGGMHoxLjAsBgNVBAMMJUFwcGxlIEFwcGxpY2F0aW9uIEludGVncmF0aW9uIENBIC0gRzMxJjAkBgNVBAsMHUFwcGxlIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MRMwEQYDVQQKDApBcHBsZSBJbmMuMQswCQYDVQQGEwJVUwIITDBBSVGdVDYwCwYJYIZIAWUDBAIBoIGTMBgGCSqGSIb3DQEJAzELBgkqhkiG9w0BBwEwHAYJKoZIhvcNAQkFMQ8XDTI0MDIwNjEzMjU0MFowKAYJKoZIhvcNAQk0MRswGTALBglghkgBZQMEAgGhCgYIKoZIzj0EAwIwLwYJKoZIhvcNAQkEMSIEINT/dyYPjTcNjk71X84ZIlf2ahq9i1f833nfsbdSS8Z4MAoGCCqGSM49BAMCBEcwRQIgf7dHun+bDTBU+APEOTa7Xt8ynVJ4ndA19liZZWjIJPwCIQCcRM9PVKAEZuf4RNmZ60UHT2G1thS4uC9ZPDSZQEcGuAAAAAAAAA==",
                "header": {
                    "publicKeyHash": "UoMmcgRJQIDTPgHvMdzPMHhaxJw3oqpC/FLZbKx4QhQ=",
                    "ephemeralPublicKey": "MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEmm16ozbTv6RFn31g1kukyIcVW14y4yzqIjcWtdjjchqErxoLS+XSjs/cHGWIbXA4mEYD7I8KtQYSoekQRbefdg==",
                    "transactionId": "2a8586b1482ec16c64210c6d3fdee4b1eb65b6d3d1476e1ce1d195be99a970f2"
                },
                "version": "EC_v1"
                },
                "paymentMethod": {
                "displayName": "Visa 0326",
                "network": "Visa",
                "type": "debit"
                },
                "transactionIdentifier": "2A8586B1482EC16C64210C6D3FDEE4B1EB65B6D3D1476E1CE1D195BE99A970F2"
            }
            }';
        /****************************************************************************** */
        $js_data = json_decode($js_string, true);
        $token_data = $js_data['token'];
        //dd($token_data);
        /****************************************************************************** */
        $payFortData = [
            'digital_wallet' => 'APPLE_PAY',
            'command' => 'PURCHASE',
            'access_code' => env('PAYFORT_APPLE_PAY_ACCESS_CODE'),
            'merchant_identifier' => env('PAYFORT_APPLE_PAY_MERCHANT_IDENTIFIER'),
            'merchant_reference' => 'Test-' . rand(10, 100) . rand(10, 100),
            'amount' => '100', // 100 means 1 AED
            'currency' => 'AED',
            'language' => 'en',
            'customer_email' => 'samnad.s@azinova.info',
            'apple_data' => $token_data['paymentData']['data'],
            'apple_signature' => $token_data['paymentData']['signature'],
            'apple_header' => [
                'apple_ephemeralPublicKey' => $token_data['paymentData']['header']['ephemeralPublicKey'],
                'apple_publicKeyHash' => $token_data['paymentData']['header']['publicKeyHash'],
                'apple_transactionId' => $token_data['paymentData']['header']['transactionId'],
            ],
            'apple_paymentMethod' => [
                'apple_displayName' => $token_data['paymentMethod']['displayName'],
                'apple_network' => $token_data['paymentMethod']['network'],
                'apple_type' => $token_data['paymentMethod']['type'],
            ],
            'customer_ip' => $_SERVER['REMOTE_ADDR'],
            'signature' => null, // will update later
        ];
        /****************************************************************************** */
        ksort($payFortData);
        $payFortData['signature'] = $this->payfortCalculateSignature($payFortData); // update signature
        echo "<fieldset style='padding:1em;margin:1em;background:#f6fff6;'><legend><b>Payfort API Input</b></legend><pre>";
        echo json_encode($payFortData, JSON_PRETTY_PRINT);
        echo "</pre></fieldset>";
        /****************************************************************************** */
        $client = new \GuzzleHttp\Client([
            'verify' => false,
        ]);
        $post_header = ["Content-Type" => "application/json"];
        $response_token_data = $client->request('POST', $url, [
            'headers' => $post_header, // no exception fix
            'http_errors' => false,
            'json' => $payFortData,
        ]);
        $token_data = json_decode((string) $response_token_data->getBody(), true);
        /****************************************************************************** */
        echo "<fieldset style='padding:1em;margin:1em;background:#fff6f6;'><legend><b>Payfort API Response</b></legend><pre>";
        echo json_encode($token_data, JSON_PRETTY_PRINT);
        echo "</pre></fieldset>";
    }
    public function payfortCalculateSignature(array $arrData, $signType = 'request')
    {
        $shaString = '';
        foreach ($arrData as $key => $val) {
            if ($key === 'signature') {
                continue;
            }
            if (gettype($val) !== 'array') {
                $shaString .= "$key=$val";
            } else {
                $sub_str = $key . '={';
                $index = 0;
                // ksort($val);
                foreach ($val as $k => $v) {
                    $sub_str .= "$k=$v";
                    if ($index < count($val) - 1) {
                        $sub_str .= ', ';
                    }
                    $index++;
                }
                $sub_str .= '}';
                $shaString .= $sub_str;
            }
        }
        if ($signType == 'request') {
            $shaString = env('PAYFORT_APPLE_PAY_SHA_REQUEST_PHRASE') . $shaString . env('PAYFORT_APPLE_PAY_SHA_REQUEST_PHRASE');
        } else {
            $shaString = env('PAYFORT_APPLE_PAY_SHA_RESPONSE_PHRASE') . $shaString . env('PAYFORT_APPLE_PAY_SHA_RESPONSE_PHRASE');
        }
        $signature = hash('sha256', $shaString);
        return $signature;
    }
}
