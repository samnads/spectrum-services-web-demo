<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CustomerApiController extends Controller
{
    public function available_time(Request $request)
    {
        $data['params']['date'] = $request->date;
        return customerApiCall('available_time', $data);
    }
    public function crew_list(Request $request)
    {
        $params = $request->all();
        foreach ($params as $key => $value) {
            $data['params'][$key] = $value;
        }
        return customerApiCall('crew_list', $data);
    }
    public function calculate(Request $request)
    {
        $params = $request->all();
        foreach ($params as $key => $value) {
            $data['params'][$key] = $value;
        }
        return customerApiCall('calculate', @$data ?: [], 'POST');
    }
    public function create_booking(Request $request)
    {
        $params = $request->all();
        foreach ($params as $key => $value) {
            $data['params'][$key] = $value;
        }
        return customerApiCall('create_booking', @$data ?: [], 'POST');
    }
    public function create_booking_new(Request $request)
    {
        $params = $request->all();
        foreach ($params as $key => $value) {
            $data['params'][$key] = $value;
        }
        return customerApiCall('create_booking_new', @$data ?: [], 'POST');
    }
    public function name_entry(Request $request)
    {
        $params = $request->all();
        foreach ($params as $key => $value) {
            $data['params'][$key] = $value;
        }
        $responseBody = customerApiCall('name_entry', @$data ?: [], 'POST');
        if ($responseBody['result']['status'] == 'success') {
            $session_data = [
                'customer_id' => $responseBody['result']['UserDetails']['id'],
                'customer_name' => $responseBody['result']['UserDetails']['UserName'],
                'customer_token' => $responseBody['result']['UserDetails']['token'],
                'customer_avatar' => $responseBody['result']['UserDetails']['image'],
                'customer_mobile' => $responseBody['result']['UserDetails']['mobile'],
                'customer_email_address' => $responseBody['result']['UserDetails']['email_address'],
                'customer_default_address_id' => $responseBody['result']['UserDetails']['default_address_id'],
            ];
            session($session_data);
        }
        return $responseBody;
    }
}
