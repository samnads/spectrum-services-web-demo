<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ServiceTypeModelController extends Controller
{
    public function normal_service(Request $request, $data)
    {
        if (!$request->step) {
            return redirect()->route($request->route()->getName(), ['step' => 1]);
        } else if (@$request->step > 1) {
            return redirect()->route($request->route()->getName(), ['step' => 1]);
        }
        $data['frequencies'] = frequencies();
        $data['service_type'] = service_type_data_by_id($data['service_type_id']);
        $data['available_datetime'] = @customerApiCall('available_datetime', [])['result'];
        $params['params']['service_type_id'] = $data['service_type_id'];
        $data_api = @customerApiCall('data', [])['result'];
        $data['payment_types'] = $data_api['payment_types'];
        $data['service_type_addons'] = @customerApiCall('service_addons', $params)['result']['service_addons'];
        $data['data'] = customerApiCall('data', [], 'GET');
        return view('service-model.normal', $data);
    }
    public function package_service(Request $request, $data)
    {
        if (!$request->step) {
            return redirect()->route($request->route()->getName(), ['step' => 1]);
        } else if (@$request->step > 1) {
            return redirect()->route($request->route()->getName(), ['step' => 1]);
        }
        $data['available_datetime'] = @customerApiCall('available_datetime', [])['result'];
        $params['params']['service_type_id'] = $data['service_type_id'];
        $data['packages_grouped'] = @customerApiCall('packages', $params)['result']['packages'];
        $data['frequencies'] = frequencies();
        $data['service_type'] = service_type_data_by_id($data['service_type_id']);
        $data_api = @customerApiCall('data', [])['result'];
        $data['payment_types'] = $data_api['payment_types'];
        $data['data'] = customerApiCall('data', [], 'GET');
        return view('service-model.package', $data);
    }
    public function special_offer(Request $request, $offer_id)
    {
        /*if (!$request->step) {
            return redirect()->route($request->route()->getName(), ['step' => 1]);
        } else if (@$request->step > 1) {
            return redirect()->route($request->route()->getName(), ['step' => 1]);
        }*/
        $params['params']['offer_id'] = $offer_id;
        $data['special_offer'] = customerApiCall('special_offers', $params)['result']['offerbanner'][0];
        //dd($data);
        //$params['params']['service_type_id'] = $data['service_type_id'];
        //$data['packages_grouped'] = @customerApiCall('packages', $params)['result']['packages'];
        //$data['frequencies'] = frequencies();
        //$data['service_type'] = service_type_data_by_id($data['service_type_id']);
        $data_api = @customerApiCall('data', [])['result'];
        $data['payment_types'] = $data_api['payment_types'];
        $data['data'] = customerApiCall('data', [], 'GET');
        return view('service-model.special-offer', $data);
    }
}
