<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class HomeController extends Controller
{
    public function home(Request $request)
    {
        $data['service_types'] = service_types();
        $data['frequencies'] = frequencies();
        $data['data'] = customerApiCall('data', [], 'GET');
        $data['special_offers'] = customerApiCall('special_offers', [], 'GET');
        //dd($data);
        return view('home', $data);
    }
    /*public function test(Request $request)
    {
        $deletes = DB::table('deletes as d')->select('d.some_name');
        foreach ($test as $key => $value) {
            $deletes = $deletes->orWhere(function ($query) use ($value) {
                $query->where('d.booking_id', '=', $value['booking_id'])
                    ->where('d.date', '=', $value['date']);
            });
        }
        $deletes = $deletes->get();
    }*/
}
