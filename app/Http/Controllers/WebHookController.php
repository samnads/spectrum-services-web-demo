<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\Customer;
use App\Models\CustomerAddress;
use App\Models\Frequencies;
use App\Models\CustomerPayments;
use App\Models\OnlinePayment;
use Carbon\Carbon;
use Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Mail;
use Response;
use stdClass;

class WebHookController extends Controller
{
    public function payfort_transaction_feedback(Request $request)
    {
        // The URL where the Merchant's server receives Amazon Payment Services responses after the transaction is processed.
        $client = new \GuzzleHttp\Client([
            'verify' => false,
        ]);;
        $client->request('POST', Config::get('url.api_url') . 'webhook/payfort/transaction-feedback', [
            'headers' => [],
            'form_params' => $request->all()
        ]);
        // response OK
    }
    public function payfort_notification(Request $request)
    {
        // The URL where the Merchant receives offline notifications regarding any status updates for any of his transactions and orders.
    }
}
