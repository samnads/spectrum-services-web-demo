<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Config;

class PaymentController extends Controller
{
    public function payfort_processing(Request $request)
    {
        //dd($request->all());
        return view('payment.payfort_processing', ['post' => $request->post()]);
    }
    public function payfort_verify(Request $request)
    {
        dd($request->all());
    }
    public function payment_form()
    {
        return view('payment.link_payment_form', []);
    }
    public function payment_confirmation(Request $request)
    {
        Session::put('direct_payment_url', url('payment?id=' . $request->customer_id . '&amount=' . $request->amount . '&message=' . $request->description));
        //$reference_id = 'SS-OP-' . mt_rand(100000, 999999);
        $params['params']['description'] = $request->description;
        $params['params']['customer_id'] = $request->customer_id;
        $params['params']['amount'] = $request->amount;
        $params['params']['transaction_charge'] = round($request->amount * .03,2);
        $params['params']['total_payable'] = round($params['params']['amount'] + $params['params']['transaction_charge'],2);
        $params['params']['ip_address'] = $request->ip();
        $params['params']['user_agent'] = $request->header('User-Agent');
        $params['params']['post_data'] = json_encode($request->all());
        $params['params']['return_data'] = NULL;
        //$params['params']['reference_id'] = $reference_id;
        $new_online_payment = customerApiCall('new-online-payment', $params, 'POST');
        //dd($new_online_payment);
        $data = $new_online_payment['result'];
        $data['amount'] = $params['params']['amount'];
        $data['transaction_charge'] = $params['params']['transaction_charge'];
        $data['total_payable'] = $params['params']['total_payable'];
        if (@$data['online_payment']['payment_id']) {
            return view('payment.payment_confirmation', $data);
        } else {
            return 'An error occured !';
        }
    }
    public function payfort_online_payment_process(Request $request)
    {
        //dd((array) $request->all());
        $response = customerApiCall('process-payfort-online-payment', $request->all(), 'POST');
        if ($response['result']['status'] == 'success') {
            return redirect('payment/success/' . $response['result']['online_payment']['payment_id']);
        } else {
            return redirect('payment/failed/' . $response['result']['online_payment']['payment_id']);
        }
    }
    public function online_payment_success(Request $request, $payment_id)
    {
        $data = customerApiCall('online-payment/' . $payment_id, [], 'GET');
        //dd($data);
        if(@$data['online_payment']['payment_status'] != 'success'){
            return redirect('payment/failed/' . $payment_id);
        }
        return view('payment.payment_success', $data);
    }
    public function online_payment_failed(Request $request, $payment_id)
    {
        $data = customerApiCall('online-payment/' . $payment_id, [], 'GET');
        return view('payment.payment_failed', $data);
    }
}
