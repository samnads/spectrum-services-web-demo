<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;

class ApiController extends Controller
{
    public function customer_api_call(Request $request, $endpoint)
    {
        // to handle any ajax requests
        $data = [];
        /*************************************************************** */
        // customize data for real API
        foreach ($request->all() as $key => $value) {
            $data['params'][$key] = $value;
        }
        $response = customerApiCall($endpoint, $data, $request->method());
        /*************************************************************** */
        if (@$response['result']['status'] == 'success') {
            if ($endpoint == 'check_otp') {
                $customer_default_address_id = (isset($response['result']['UserDetails']['default_address_id']) ? $response['result']['UserDetails']['default_address_id'] : Session::get('customer_default_address_id'));
                $session_data = [
                    'customer_id' => $response['result']['UserDetails']['id'],
                    'customer_name' => $response['result']['UserDetails']['UserName'],
                    'customer_token' => $response['result']['UserDetails']['token'],
                    'customer_avatar' => $response['result']['UserDetails']['image'],
                    'customer_mobile' => $response['result']['UserDetails']['mobile'],
                    'customer_email' => $response['result']['UserDetails']['email_address'],
                    'customer_default_address_id' => $customer_default_address_id,
                ];
                session($session_data);
            }
            else if ($endpoint == 'name_entry') {
                $session_data = [
                    'customer_token' => $response['result']['UserDetails']['token'],
                    'customer_name' => $response['result']['UserDetails']['UserName'],
                    'customer_email' => $response['result']['UserDetails']['email_address'],
                ];
                session($session_data);
            } else if ($endpoint == 'update_customer_data') {
                $customer_default_address_id = (isset($response['result']['UserDetails']['default_address_id']) ? $response['result']['UserDetails']['default_address_id'] : Session::get('customer_default_address_id'));
                $session_data = [
                    'customer_name' => $response['result']['UserDetails']['UserName'],
                    'customer_token' => $response['result']['UserDetails']['token'],
                    'customer_avatar' => $response['result']['UserDetails']['image'],
                    'customer_email' => $response['result']['UserDetails']['email_address'],
                    'customer_default_address_id' => $customer_default_address_id,
                ];
                session($session_data);
            } else if ($endpoint == 'customer_logout') {
                Session::flush();
            }
        }
        /*************************************************************** */
        return $response;
    }
}