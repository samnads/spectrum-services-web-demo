<?php
use Cache as Cache;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

function service_type_data_by_id($service_type_id)
{
    /*
     * store service for future usage
     */
    if (Cache::get('service_type_' . $service_type_id)) {
        return Cache::get('service_type_' . $service_type_id);
    } else {
        $client = new \GuzzleHttp\Client([
            'verify' => false,
        ]);
        $response = $client->request('GET', Config::get('url.api_url') . 'web/service_type/' . $service_type_id, [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded',
            ],
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        if ($responseBody['result']['status'] == 'success') {
            Cache::put('service_type_' . $service_type_id, $responseBody['result']['data'], 60);
        }
        return Cache::get('service_type_' . $service_type_id);
    }
}
function frequencies()
{
    /*
     * store frequencies for future usage
     */
    if (Cache::get('frequencies')) {
        return Cache::get('frequencies');
    } else {
        $client = new \GuzzleHttp\Client([
            'verify' => false,
        ]);
        $response = $client->request('GET', Config::get('url.api_url') . 'customer/frequencies', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded',
            ],
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        if ($responseBody['result']['status'] == 'success') {
            Cache::put('frequencies', $responseBody['result']['frequency_list'], 60);
        }
        return Cache::get('frequencies');
    }
}
function service_types()
{
    /*
     * store service types for future usage
     */
    if (Cache::get('service_types')) {
        return Cache::get('service_types');
    } else {
        $client = new \GuzzleHttp\Client([
            'verify' => false,
        ]);
        $response = $client->request('GET', Config::get('url.api_url') . 'customer/service_types', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded',
            ],
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        if ($responseBody['result']['status'] == 'success') {
            Cache::put('service_types', $responseBody['result']['data'], 60);
        }
        return Cache::get('service_types');
    }
}

function customerApiCall($endpoint, $data, $method = 'GET')
{
    $client = new \GuzzleHttp\Client([
        'verify' => false,
    ]);
    // append with existing data
    $data['params']['id'] = @session('customer_id');
    $data['params']['token'] = @session('customer_token');
    $data['params']['platform'] = 'web';
    $response = $client->request($method, Config::get('url.api_url') . 'customer/' . $endpoint, [
        'headers' => [
            'cache-control' => 'no-cache',
            'Content-Type' => 'application/x-www-form-urlencoded',
        ],
        'body' => json_encode($data),
    ]);
    $responseBody = json_decode((string) $response->getBody(), true);
    return $responseBody;
}
function isDemo()
{
    $locals = ['127.0.0.1', 'localhost', '::1', '-demo'];
    foreach ($locals as $local) {
        if (strpos(@$_SERVER['HTTP_HOST'] . @$_SERVER['REQUEST_URI'], $local) !== FALSE) {
            return true;
        }
    }
    return false;
}