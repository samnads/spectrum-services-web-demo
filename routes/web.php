<?php

use App\Http\Controllers\CustomerApiController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ServiceTypeModelController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\BookingPaymentStatusController;
use App\Http\Controllers\PayfortPaymentController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\WebHookController;
use App\Http\Controllers\ApiController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
Route::group(['middleware' => ['validateSessionToken']], function () {
    Route::get('/account', [ProfileController::class, 'account'])->name('account');
    Route::get('/account/personal', [ProfileController::class, 'account_personal'])->name('account_personal');
    Route::get('/account/address', [ProfileController::class, 'account_address'])->name('account_address');
    Route::get('/booking/success/{reference_id}', [BookingPaymentStatusController::class, 'success'])->name('payment-success');
    Route::get('/booking/failed/{reference_id}', [BookingPaymentStatusController::class, 'failed'])->name('payment-failed');
});
Route::group(['middleware' => ['validateSessionTokenIfExist']], function () {
    Route::get('/', [HomeController::class, 'home'])->name('home');
    Route::get('/home-cleaning', function () {
        $data['service_type_id'] = 1;
        return App::call('App\Http\Controllers\ServiceTypeModelController@normal_service', ['data' => $data]);
    })->name('home-cleaning');
    Route::get('/deep-cleaning', function () {
        $data['service_type_id'] = 19;
        return App::call('App\Http\Controllers\ServiceTypeModelController@package_service', ['data' => $data]);
    })->name('deep-cleaning');
    Route::get('/furniture-cleaning', function () {
        $data['service_type_id'] = 18;
        return App::call('App\Http\Controllers\ServiceTypeModelController@package_service', ['data' => $data]);
    })->name('furniture-cleaning');
    Route::get('/special-offer/{offer_id}', [ServiceTypeModelController::class, 'special_offer'])->name('special-offer');
    //
    Route::get('/bookings/upcoming', [ProfileController::class, 'upcoming_bookings'])->name('upcoming-bookings');
    Route::get('/bookings/past', [ProfileController::class, 'past_bookings'])->name('past-bookings');
    Route::get('/bookings/cancelled', [ProfileController::class, 'cancelled_bookings'])->name('cancelled-bookings');
});
//
Route::post('booking/payment/payfort/process/{booking_id}', [PayfortPaymentController::class, 'booking_payment_payfort_process']);
Route::any('/payment/payfort/processing', [PaymentController::class, 'payfort_processing']);
Route::post('/payment/payfort/verify', [PaymentController::class, 'payfort_verify']);
Route::any('/api/customer/{endpoint}', [ApiController::class, 'customer_api_call']);
/***********************************************************************************
 * 
 * 
 * payment link module
 * 
 * 
 */
Route::get('/payment', [PaymentController::class, 'payment_form']);
Route::any('/payment/confirmation', [PaymentController::class, 'payment_confirmation']);
Route::post('/payment/payfort/online-payment/process', [PaymentController::class, 'payfort_online_payment_process']);
Route::get('/payment/success/{payment_id}', [PaymentController::class, 'online_payment_success']);
Route::get('/payment/failed/{payment_id}', [PaymentController::class, 'online_payment_failed']);
/***********************************************************************************
 * 
 * 
 * WebHook urls for payment gateways
 * 
 * 
 */
// WebHook urls for payfort gateway
Route::post('webhook/payfort/transaction-feedback', [WebHookController::class, 'payfort_transaction_feedback']); // The URL where the Merchant's server receives Amazon Payment Services responses after the transaction is processed.
Route::post('webhook/payfort/notification', [WebHookController::class, 'payfort_notification']); // The URL where the Merchant receives offline notifications regarding any status updates for any of his transactions and orders.
/**********************************************************************************/