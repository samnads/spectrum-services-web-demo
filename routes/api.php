<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ApplePayController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::any('payfort_apple_test', [ApplePayController::class, 'payfort_apple_test']); // POST | DONE
Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
